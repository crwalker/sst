
# allow_user_segv_handler necessary for Racket: https://gist.github.com/samth/a24ee8064fe79d56f036
# list all ASAN options with "> ASAN_OPTIONS=help=1 ./bin/sst"
ASAN_OPTIONS="detect_leaks=1,allow_user_segv_handler=true" ./bin/sst \
    -g \
    --node_key=0000000000000000000000000000000100000000000000000000000000000001 \
    --remote_node_key=0000000000000000000000000000000200000000000000000000000000000002 \
    --remote_ip=127.0.0.1 \
    --remote_datagram_port=8101 \
    --remote_stream_port=8100 2>&1
