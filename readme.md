![Logo](assets/banner.png)  

# Universal data store enabling a single source of truth

## Dependencies:
* git
* cmake 3.7+
* llvm
 * OSX: Apple LLVM version 7.3.0 (clang-703.0.31)
* libsodium (cryptography library)

### Installing dependencies on OSX:
* `brew install llvm` (if included version too old)
* `brew install cmake` (if included version too old)
* run $> ./setup.sh once to gather lib dependencies

### Installing dependencies on Ubuntu:
* Install llvm
* Install clang
* Install cmake
* run $> ./setup.sh once to gather lib dependencies

## Development
* Sublime:
 * SublimeLinter-contrib-clang 
 * SublimeLinter-cpplint (requires `pip install cpplint`)

## Compilation / Installation
* run `$> ./compile.sh` after each change to src
* The runtime binary is output to `./bin/`

## Operation
* Quick start: `$ ./run.sh` and `$ ./pair.sh` set up a pair of SST nodes
* in general: `$> ./bin/sst` 
* options:
  * -g: create a genesis block
  * --node_key: set the key of this node
  * --datagram_port (optional, default 8000): the node will listen for UDP messages on this port
  * --stream_port (optional, default 8001): the node will listen for TCP messages on this port
  * --remote_node_key: set the key of the remote bootstrap node
  * --remote_node_ip: set the IP for the remote bootstrap node
  * --remote_datagram_port (optional, default 8000): set the UDP port for the remote bootstrap node
  * --remote_stream_port (optional, default 8001): set the TCP port for the remote bootstrap node

## Architecture
SST is a single-threaded, event driven program written in stupidly simple C11. The long-term goal is to remove all OS dependencies so that SST can optionally be run on bare metal using musl-libc (http://www.musl-libc.org/) or similar. Functionality is split across several mostly independent modules:
* SST: run the entire application
* blockchain: blocks, transactions, tx inputs and outputs
* crypto: hashes, signatures
* kv_store: hash table storing content
* message: RPC between nodes
* route: kademlia routing table to find relevant SST nodes
* serialize: transform data to / from serial form for messaging
* network: TCP/IP and datagram sockets to send messages

If you would like to contribute, your pull requests may not increase:
* obfuscation
* non-essential complexity
* any dependency under 5 years old
* source lines of code, measured by `cloc ./src/`, above 5k SLOC

# Overview

## Definition
SST is both:
* protocol (upcoming whitepaper)
* reference software implementation (this repository)

SST enables participating nodes to create a universal key-value store which reaches consensus by blockchain, verifies storage transactions using commutative hashing, and secures values by public-key cryptography, forming a decentralized storage network with other SST nodes.

## Objective
SST aims to create a simple, low-cost way to store and share data that *always works*. Ethernet always works for transmitting data, but we want a similarly ubiquitous and trustworthy system for storing data.

## Rationale
Computers are used for computation, i.e. applying functions to data. Computers, broadly speaking, can only:
* Receive incoming electronic data
* Store this data as an array of bits
* Apply functions to this data
* Electronically transmit the output of these functions to other systems, generally other other computers, screens, or mechanical actuators

It would be easy for software practitioners to lose sight of the fundamental purpose of computation considering the complexity in current approaches to software, such as frontend javascript transpilation frameworks and containerization libraries. Most of what software developers do today is gluing together large packages of code, and in general these packages:
* change frequently
* are too large to audit
* contain large numbers of bugs and security vulnerabilities
* add layers of abstraction and compexity

The resulting software systems contain far more complexity than demanded by the underlying function they aim to perform. Frequently, many different components in the resulting software artifact perform the same function, add no value, or work at cross purposes to each other. There is obviously a positive correlation between the size of a software artifact and the number of flaws in the software artifact.

Everyone in the industry can see this problem, and yet, faced with an unacceptably high failure rate in our software systems, we continue adding layers of abstraction and complexity.

SST is an experiment attempting to answer the question: how much of our current software infrastructure can be eliminated with a simple, universal, immutable data store?

SST aims to always work, not to solve every problem optimally. There are obviously problems where highly custom software is necessary, e.g. at CERN or Google. But most software problems are somewhat predictable. We want to give developers a general purpose tool that enables them to spend time building useful functionality and a beautiful user experience rather than reinventing another custom tech stack.

SST aims to out-compete the following approaches by providing a barely-good-enough foundation for technical solutions that eliminate:
* SaaS (isn't software-as-an-asset more useful in the long run?)
* Operating systems (when did you last face the challenge of sharing processor time on a $1MM mainframe with 100 college students?)
* Cloud data storage (currently provided free of charge, courtesy of the NSA)
* Disparate versioning solutions for each domain (you didn't like git anyways)
* File systems (if indexing data by './x/../y/z.txt' is the best we can do, let's go home)

## Design Philosophy
* Make solutions as simple as possible, but no simpler
  * do as little as possible
  * lines of code are a liability, not an asset
  * but explicit and stupid is simpler than implicit and clever
* Use proven technical solutions to avoid risk
* Make peace with physics: don't pretend to solve intractable problems
* Effectiveness, not efficiency: optimize across all performance dimensions weighted by importance rather than focusing on a single dimension

## Example Applications
### Inter-company collaboration
If an electronics company, a shipping company, and a contract manufacturer want to track the status of a particular batch of goods as they are manufactured, shipped, and assembled, their current options are:
* Writing specific integrations for the software platforms each uses as a source of truth (expensive, fragile, non-scalable)
* One company convincing the others to use its database as a single source of truth (unacceptable business risk)

A lightweight data validation tool using SST as a backend would provide a protocol and a decentralized storage mechanism for this collaboration, reducing development costs, decreasing business risk, and preserving flexibility as business needs and collaborators change.

### Long-term data storage
People generate a wide variety of important, long-lived data such as photo libraries, tax returns, and contact info. To handle this data, they must either:
* Manually sync the data across many computers and back up the data onto remote storage (only possible for highly technical users)
* Trust the data to third-party cloud providers which generally have access to the unencrypted data and extract rents for the convenience of cloud storage.

SST provides a scalable storage mechanism at commodity prices backed by well defined financial risk and rewards to storage nodes, which can be used as a distributed, redundant

# Data Structures
## Node
A computer implementing SST

## Client
A computer communicating with the SST network which is not a node

## Store
A set of immutable (key, value, type) tuples on a node's persistent storage. The tuples are redundantly distributed across nodes using a Kademlia routing tree based on key location within the key address space.
### Value
An immutable one-dimensional array of bytes
### Key
An immutable hash of a value and type, key = Hash(value, type)
### Type
A key to a value used to describe values
### Id

## Message
A signal sent between a node and another node or a client
### Ping
### Pong
### Node Request
### Node Response
### Value Request
### Value Response
### Value Store
### Transaction
### Block Request
### Block
### Blockchain Request
### Blockchain

## Blockchain
### Block
### Transaction
### Input
### Output
### Contract
Conditions under which a transaction output may be used as an input by another transaction.

## Kademlia Routing Tree
### Node
### Bucket
