#!/usr/local/bin/python
import os
import random
import socket
import time

SERVER_PORT = 8000
UDP_IP = "127.0.0.1"
UDP_PORT = SERVER_PORT + random.randint(1, 8000)

sock = socket.socket(socket.AF_INET, # Internet
             socket.SOCK_DGRAM) # UDP

types = {
    'ping': "01000000",
    'pong': "02000000",
    'find_node': "03000000",
    'found_node': "04000000",
}

def id(fake=True):
    # fake a simple key
    if fake:
        return str(random.randint(0,9)) * 32
    return os.urandom(16).encode('hex')

ID = id()
SEARCH_ID = id()

def node():
    # fake an actual node
    return "22222222222222222222222222222222100231857f00000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004986bd5800000000000000000000000060ee0000f0600000"

def decode(arr):
    return ''.join('{:02x}'.format(x) for x in arr)

def send_message(message_type, contents):
    message = bytearray.fromhex(types[message_type] + contents)
    print 'sending message type: {}, length: {}, contents: {}'.format(message_type, len(message), decode(message))
    sock.sendto(message, (UDP_IP, SERVER_PORT))

def ping():
    send_message('ping', ID)

def pong():
    send_message('pong', ID)

def send_find_node():
    # ask another node to find and return nodes closest to the search node
    send_message('find_node', ID + SEARCH_ID)

def send_found_node():
    # return: message type, our id, id requested by search, serialized node we found
    send_message('found_node', ID + SEARCH_ID + node())

print 'Running python mock client at ip {}, port {}, id {}'.format(UDP_IP, UDP_PORT, ID)

sock.bind((UDP_IP, UDP_PORT))

while True:
    ping()
    send_find_node()

    data, addr = sock.recvfrom(1024);
    data = bytearray(data)
    print 'received message', decode(data), addr

    message_type = data[0:5]
    if message_type == types['ping']:
        print 'received ping'
        pong()

    elif message_type == types['pong']:
        print 'received pong'
        pass

    elif message_type == types['find_node']:
        print 'received find_node, so sending a node'
        send_found_node()

    time.sleep(2)
