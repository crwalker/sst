# compile with fsantize flag enabled
clang -O1 -g -fsanitize=address -fno-omit-frame-pointer bad_address.c
# run the binary: this should break with verbose fsanitize messages.
./a.out

# compile with fsantize flag enabled
clang -fsanitize=undefined bad_undefined.c
./a.out