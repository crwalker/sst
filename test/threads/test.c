#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>

/*
If the counter is not atomic and incremented atomically, it will get out of sync
with the thread number, i.e. thread 999 will report a counter value of ~900.
*/

struct Foo {
    //uintptr_t counter;
    _Atomic uintptr_t counter;
    uintptr_t i;
};

void *child(void *in) {
    struct Foo *foo = in;
    uintptr_t naive = foo->i;
    printf("Hello World! in thread %ld! counter: %ld\n", naive, foo->counter);
    volatile unsigned long long i;
    for (i = 0; i < rand(); ++i);

    foo->counter += 1;
    //atomic_fetch_add(&foo->counter, 1);
    printf("Goodbye World! in thread %ld! counter: %ld\n", naive, foo->counter);
    pthread_exit(NULL);
}

int main() {
    int nThreads = 1000;
    pthread_t threads[nThreads];

    struct Foo fin = {0, 0};
    struct Foo *foo = &fin;

    int error;
    for (foo->i = 0; foo->i < nThreads; foo->i++) {
        printf("in main, foo i: %ld, counter: %ld\n", foo->i, foo->counter);
        error = pthread_create(&threads[foo->i], NULL, child, foo);
        printf("in main, creating thread %ld\n", foo->i);
        if (error) {
            printf("ERROR; return code from pthread_create() is %d\n", error);
            perror("Oh this failed");
            exit(EXIT_FAILURE);
        }
    }
    printf("FINALLY: in main, foo i: %ld, counter: %ld\n", foo->i, foo->counter);
    pthread_exit(NULL);
}