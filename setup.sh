# Brute force script for getting all dependencies
# Only tested on OSX

mkdir lib
cd lib

#install libsodium
git clone https://github.com/jedisct1/libsodium --branch stable
cd libsodium
git checkout v1.0.12
./configure
make && make check
make install
cd ..

# gyp required for libuv
# git clone https://chromium.googlesource.com/external/gyp.git
# cd gyp
# python setup.py install

# install libuv
# git clone https://github.com/libuv/libuv --branch v1.x
# cd libuv
# git checkout v1.11.0
# ./gyp_uv.py -f make
# make -C out
# BUILDTYPE=Release make -C out
# brew install --HEAD libuv
# ./out/Debug/run-tests
# cd ..

# install libev -- we include the ev.h header rather than linking libraries
brew install wget 
wget http://dist.schmorp.de/libev/Attic/libev-4.24.tar.gz
gunzip -c libev-4.24.tar.gz | tar xopf -
cd libev-4.24
./autogen.sh
./configure
make
sudo make install