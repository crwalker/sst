#!/bin/bash

rm -rf bin build
mkdir bin build

cd src
echo "building racket sandbox"
raco ctool --c-mods racket_sandbox.h racket-sandbox.rkt
cd ..


# don't pollute the source code with cmake output
cd build

# Force compiling with clang
cmake \
    -D CMAKE_C_COMPILER=/usr/bin/clang \
    -D CMAKE_BUILD_TYPE=DEBUG \
    ../src

# Fail hard if cmake failed
if [ $? -ne 0 ] ; then
   echo "cmake failed, aborting!" >&2
   exit
fi

make VERBOSE=1

if [ $? -ne 0 ] ; then
   echo "make failed, aborting!" >&2
   exit
fi

cd ..
