// Copyright 2017 Chris Walker

// Hack, required for struct addrinfo, getnameinfo, NI_NUMERICHOST, NI_NUMERICSERV on debian
#define _POSIX_C_SOURCE 201112L
// Hack, somebody failing to define, needed for maximum length of FQDN and server name
#define NI_MAXSERV    32
#define NI_MAXHOST  1025

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <assert.h>
#include <ev.h>
#include <inttypes.h>

#include "config.h"
#include "network.h"
#include "serialize.h"
#include "utils.h"
#include "crypto.h"


/*
Hand crafted networking.
See http://www.beej.us/guide/bgnet/output/html/singlepage/bgnet.html

Most assuredly not thread-safe
*/

static void on_stream_accept(struct ev_loop *loop, ev_io *stream_watcher, int revents);
static void on_stream_client(struct ev_loop *loop, ev_io *stream_watcher, int revents);
static void on_datagram(struct ev_loop *loop, ev_io *stream_watcher, int revents);
// static void print_addrinfo(struct addrinfo *);

// how many pending TCP/IP connections queue will hold for stream server socket
#define STREAM_BACKLOG 10

// how many stream connections are currently active;
static int stream_connections = 0;

// is this scope really a good idea?
static uint8_t datagram_buffer[DATAGRAM_BUFFER_SIZE];
static uint8_t stream_buffer[STREAM_BUFFER_SIZE];

// only network.c needs access to the actual network sockets
// make sure to start with network_init() before use and finish with network_end()
static int datagram_socket, stream_socket;
ev_io *datagram_watcher, *stream_watcher;

// the key of this node (set by codex.c);
// extern struct Key my_node_key;


void default_addresses(
    enum MessageProtocol protocol,
    struct sockaddr_storage *datagram_address,
    struct sockaddr_storage *stream_address) {
    /*
    given pointers to addresses and a protocol indicating which one is valid, guess the other
    assuming the node uses default SST datagram and stream configuration:
    datagram port = stream_port + 1
    */
    puts("guessing at a missing address");
    char host_string[NI_MAXHOST];
    char port_string[NI_MAXSERV];
    long port;
    char guessed_port[33];

    // the address matching the protocol must exist
    assert(datagram_address);
    assert(stream_address);

    int rc = getnameinfo(
        (struct sockaddr *) (protocol == DATAGRAM ? datagram_address : stream_address),
        sizeof(struct sockaddr_storage),
        host_string,
        sizeof host_string,
        port_string,
        sizeof port_string,
        NI_NUMERICHOST | NI_NUMERICSERV);

    if (rc != 0) {
        printf("could not get name info from address!\n");
        exit(EXIT_FAILURE);
    }

    port = strtol(port_string, NULL, 10);

    if (protocol == DATAGRAM) {
        // fill in the missing stream address
        snprintf(guessed_port, sizeof(guessed_port), "%ld", port - 1);
        *stream_address = get_remote_address(host_string, guessed_port, STREAM);
        printf("guessed default stream address: ");
        print_sockaddr_storage(stream_address);
        printf("\n");
    } else {
        // fill in the missing datagram address
        snprintf(guessed_port, sizeof(guessed_port), "%ld", port + 1);
        *datagram_address = get_remote_address(host_string, guessed_port, DATAGRAM);
        printf("guessed default datagram address: ");
        print_sockaddr_storage(datagram_address);
        printf("\n");
    }
}


void sigchild_handler(int s) {
    // wait pid might overwrite errno, so we save and restore it
    int saved_errno = errno;
    while (waitpid(-1, NULL, WNOHANG) > 0) {
        continue;
    }
    errno = saved_errno;
    // prevent warning about unused s
    s = s + 1;
}


void *get_in_addr(struct sockaddr *socket_address) {
    // return socket_address->sa_family == AF_INET ?
    //     (void *) &(((struct sockaddr_in*)socket_address)->sin_addr)
    //     :
    //     (void *) &(((struct sockaddr_in6*)socket_address)->sin6_addr);

    // get socket address: IPv4 or IPv6
    if (socket_address->sa_family == AF_INET) {
        // address formatted as IPv4
        return &(((struct sockaddr_in*)socket_address)->sin_addr);  //NOLINT
    }
    // address formatted as IPv6
    return &(((struct sockaddr_in6*)socket_address)->sin6_addr);  //NOLINT
}


void setup_address(struct addrinfo **address, char* node, char* service, int socket_type) {
    int status;
    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);

    /* 
    AI_NUMERICHOST: host must be a numeric IP, not a DNS lookup
    AI_NUMERICSERV: service must be numeric port string
    AI_PASSIVE: if address is NULL, return wildcard (INADDR_ANY or IN6ADDR_ANY_INIT)
    */
    hints.ai_flags = AI_NUMERICHOST | AI_NUMERICSERV | AI_PASSIVE;

    // IPv4 (AF_INET) or IPv6 (AF_INET6) or either (AF_UNSPEC)
    // NOTE: setting AF_UNSPEC breaks network, so we are not handling IPv6 correctly
    hints.ai_family = AF_INET;

    // TCP or UDP (SOCK_STREAM or SOCK_DGRAM)
    hints.ai_socktype = socket_type;

    if ((status = getaddrinfo(node, service, &hints, address)) == -1) {
        fprintf(stdout, "getaddrinfo error: %s\n", gai_strerror(status));
        panic("Could not get address info matching node, service, hints");
    }
    // print_addrinfo(*address);
}


int setup_server_socket(char* node, char* service, int socket_type) {
    // set up a socket using bind that will listen to incoming messages
    // set socket_type to SOCK_DGRAM for UDP or SOCK_STREAM for TCP
    int socket_fd, yes = 1;
    struct addrinfo *address, *first_address;

    setup_address(&first_address, node, service, socket_type);
    // print_addrinfo(first_address);
    // server_info now points to a linked list of struct addrinfos
    // loop through list and bind to first one we can
    for (address = first_address; address != NULL; address = address->ai_next) {
        socket_fd = socket(address->ai_family, address->ai_socktype, address->ai_protocol);
        if (socket_fd == -1) {
            perror("socket");
            continue;
        }
        if (setsockopt(
            socket_fd,
            SOL_SOCKET,
            SO_REUSEADDR,
            &yes,
            sizeof(int)) == -1) {
            perror("setsockopt");
            panic("Could not set socket options.");
        }

        if (bind(socket_fd, address->ai_addr, address->ai_addrlen) == -1) {
            perror("server: bind");
            close(socket_fd);
            continue;
        }

        break;
    }

    if (address == NULL) {
        // we reached the end of the linked list without binding a valid socket
        printf("server: failed to bind\n");
        exit(EXIT_FAILURE);
    }

    printf("Successfully bound socket to address\n");
    // print_addrinfo(address);
    
    freeaddrinfo(first_address);

    return socket_fd;
}


int setup_client_socket(char* node, char* service) {
    // only necessary for TCP connections
    int socket_fd;
    struct addrinfo *address, *first_address;

    setup_address(&first_address, node, service, SOCK_STREAM);

    for (address = first_address; address != NULL; address = address->ai_next) {
        socket_fd = socket(address->ai_family, address->ai_socktype, address->ai_protocol);
        if (socket_fd == -1) {
            perror("socket");
            continue;
        }

        if (connect(socket_fd, address->ai_addr, address->ai_addrlen) == -1) {
            perror("client: connect");
            close(socket_fd);
            continue;
        }

        break;
    }

    if (address == NULL) {
        // we reached the end of the linked list without binding a valid socket
        printf("client: failed to connect to remote socket");
        // we cannot fail: this may not be our fault
        return -1;
    }

    freeaddrinfo(first_address);
    return socket_fd;
}


struct sockaddr_storage addrinfo2sockaddr_storage(struct addrinfo *address_info) {
    struct sockaddr_storage address_storage;
    // printf("converting from addrinfo to sockaddr_storage\n");
    // printf("from addrinfo: ");
    // print_addrinfo(address_info);
    memcpy(&address_storage, address_info->ai_addr, address_info->ai_addrlen);
    // printf("to sockaddr_storage: ");
    // print_sockaddr_storage(&address_storage);
    return address_storage;
}


struct sockaddr_storage get_remote_address(char *host, char *service, enum MessageProtocol protocol) {
    struct addrinfo *address_info;
    struct sockaddr_storage address_storage;

    setup_address(&address_info, host, service, protocol == DATAGRAM ? SOCK_DGRAM : SOCK_STREAM);

    address_storage = addrinfo2sockaddr_storage(address_info);
    freeaddrinfo(address_info);
    return address_storage;
}


// void print_addrinfo(struct addrinfo *address) {
//     char ip_string[INET6_ADDRSTRLEN];

//     // convert the IP to a string and print it
//     printf(
//         "IP address: %s\n",
//         inet_ntop(
//             address->ai_family,
//             get_in_addr((struct sockaddr *) address),
//             ip_string,
//             sizeof ip_string));
// }

void print_sockaddr_storage(struct sockaddr_storage *address) {
    char host_string[NI_MAXHOST];
    char port_string[NI_MAXSERV];

    int rc = getnameinfo(
        (struct sockaddr *) address,
        sizeof(struct sockaddr_storage),
        host_string,
        sizeof host_string,
        port_string,
        sizeof port_string,
        NI_NUMERICHOST | NI_NUMERICSERV);

    if (rc == 0) {
        printf("address: %s:%s", host_string, port_string);
    } else {
        printf("address: invalid sockaddr_storage address!");
    }
}

void send_stream(
    struct sockaddr_storage *remote_address,
    int *stream_socket,
    uint8_t *message,
    uint32_t message_size) {
    // send a stream socket, re-using the socket if possible

    printf("trying to send a stream with message of size: %"PRIu32"\n", message_size);
    assert(remote_address);
    assert(message);
    assert(message_size <= STREAM_BUFFER_SIZE);

    // TODO(cw): try to reuse existing socket?
    // if (!*stream_socket) {
    // }

    // this is an odd way to create a socket: sockaddr_storage -> addrinfo -> socket
    char host_string[NI_MAXHOST];
    char port_string[NI_MAXSERV];

    int rc = getnameinfo(
        (struct sockaddr *) remote_address,
        sizeof(struct sockaddr_storage),
        host_string,
        sizeof host_string,
        port_string,
        sizeof port_string,
        NI_NUMERICHOST | NI_NUMERICSERV);

    if (rc != 0) {
        printf("could not get name info from address!\n");
        exit(EXIT_FAILURE);
    }

    *stream_socket = setup_client_socket(host_string, port_string);

    if (*stream_socket == -1) {
        puts("could not connect to remote socket");
        return;
    }

    int sent_bytes = send(
        *stream_socket,
        message,
        (int) message_size,
        0);

    // printf("sent stream of %i bytes\n", sent_bytes);

    if (sent_bytes == -1) {
        perror("sendto");
        exit(EXIT_FAILURE);
    } else if (sent_bytes < (int) message_size) {
        // TODO(cw): handle intelligently. But at least we don't fail silently
        puts("could not send complete message");
        exit(EXIT_FAILURE);
    }

    close(*stream_socket);
}


void send_datagram(struct sockaddr_storage *remote_address, uint8_t *message, uint32_t message_size) {
    assert(remote_address);
    assert(message);
    assert(message_size <= DATAGRAM_BUFFER_SIZE);
    int sent_bytes = sendto(
        datagram_socket,
        message,
        (int) message_size,
        0,
        (struct sockaddr *) remote_address,
        sizeof(struct sockaddr));

    if (sent_bytes == -1) {
        perror("sendto");
        exit(EXIT_FAILURE);
    } else if (sent_bytes < (int) message_size) {
        // TODO(cw): handle intelligently. But at least we don't fail silently
        puts("could not send complete message");
        exit(EXIT_FAILURE);
    }
}

// ignore the warning that loop, w, revents may not be used
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
void on_stream_accept(struct ev_loop *loop, struct ev_io *watcher, int revents) {
    // Accept a new TCP/IP connection by creating a new socket, then
    // add a watcher to read incoming data

    // need to save sockaddr_storage for the watcher to use later
    socklen_t address_length = sizeof(struct sockaddr_storage);
    // TODO(cw): somebody needs to free this memory!
    struct sockaddr_storage *remote_address = calloc_or_exit(1, address_length);
    int new_socket_fd;

    if (EV_ERROR & revents) {
        puts("unspecified stream watcher event error");
        exit(EXIT_FAILURE);
    }

    puts("can read tcp/ip socket");

    new_socket_fd = accept(
        stream_socket,
        (struct sockaddr *) remote_address,
        &address_length);

    if (new_socket_fd  == -1) {
        perror("accept");
        return;
    }

    struct ev_io *new_client_watcher = malloc(sizeof(struct ev_io));

    // save the remote_address so on_stream_client can reference it
    new_client_watcher->data = remote_address;

    stream_connections++;
    printf("successfully connected with new client, %i clients connected\n", stream_connections);

    // listen to the new client on the new socket
    ev_io_init(new_client_watcher, on_stream_client, new_socket_fd, EV_READ);
    ev_io_start(loop, new_client_watcher);
}


void on_stream_client(struct ev_loop *loop, struct ev_io *watcher, int revents) {
    if (EV_ERROR & revents) {
        puts("unspecified stream client watcher event error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_storage *remote_address = (struct sockaddr_storage *) watcher->data;
    ssize_t bytes_read = recv(watcher->fd, stream_buffer, STREAM_BUFFER_SIZE, 0);

    if (bytes_read < 0) {
        puts("tcp/ip stream read error");
        exit(EXIT_FAILURE);
    } else if (bytes_read == 0) {
        ev_io_stop(loop, watcher);
        free(watcher);
        stream_connections--;
        printf("a tcp/ip client just closed a connection, now %i clients connected\n", stream_connections);
        return;
    }

    printf("on_stream_client: got a new message\n");
    // print_buffer(stream_buffer, bytes_read);
    // printf("\n");

    on_message(*remote_address, STREAM, stream_buffer, bytes_read);
}

void on_datagram(struct ev_loop *loop, struct ev_io *w, int revents) {
    struct sockaddr_storage remote_address;
    socklen_t address_length = sizeof(struct sockaddr_storage);
    memset(&remote_address, 0, address_length);
    int bytes_read;

    if (EV_ERROR & revents) {
        puts("unspecified datagram_watcher event error");
        exit(EXIT_FAILURE);
    }

    puts("can read datagram socket");

    if ((bytes_read = recvfrom(
            datagram_socket,
            (void *) datagram_buffer,
            DATAGRAM_BUFFER_SIZE - 1,
            0,
            (struct sockaddr *) &remote_address,
            &address_length)) == -1) {
        perror("recvfrom");
        exit(1);
    }

    printf("got %d byte message from ", bytes_read);
    print_sockaddr_storage(&remote_address);
    printf("\n");

    on_message(remote_address, DATAGRAM, datagram_buffer, bytes_read);
}
#pragma clang diagnostic pop


void network_init(struct ev_loop *loop, char *datagram_port, char *stream_port) {
    printf("running datagram server on port %s, stream server on port %s\n", datagram_port, stream_port);

    printf("Setting up datagram socket on port %s\n", datagram_port);
    datagram_socket = setup_server_socket(NULL, datagram_port, SOCK_DGRAM);

    printf("setting up stream socket on port %s\n", stream_port);
    stream_socket = setup_server_socket(NULL, stream_port, SOCK_STREAM);
    if (listen(stream_socket, STREAM_BACKLOG) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    // level triggered
    stream_watcher = malloc_or_exit(sizeof (ev_io));
    ev_io_init(stream_watcher, on_stream_accept, stream_socket, EV_READ);
    ev_io_start(loop, stream_watcher);

    // level triggered
    datagram_watcher = malloc_or_exit(sizeof (ev_io));
    ev_io_init(datagram_watcher, on_datagram, datagram_socket, EV_READ);
    ev_io_start(loop, datagram_watcher);
}


void network_end() {
    close(datagram_socket);
    close(stream_socket);

    free(stream_watcher);
    free(datagram_watcher);
}

