// Copyright 2017 Chris Walker

#define _POSIX_C_SOURCE 201112L // rand_r is a POSIX function, not in ANSI C
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sodium.h>

#include "crypto.h"
#include "types.h"
#include "blockchain.h"
#include "utils.h"
#include "mpack.h"
#include "serialize.h"

extern uint32_t random_seed;
extern struct Key *my_node_key;
// static struct IndexHash murmur_hash3_x64_128(const void *input, const int input_length);


void crypto_init() {
    // TODO(cw): unify these constants
    assert(HASH_BYTES == crypto_generichash_BYTES);
    assert(KEY_BYTES == HASH_BYTES);
    puts("initializing libsodium");
    if (sodium_init() == -1) {
        perror("libsodium init failed:");
        exit(EXIT_FAILURE);
    }

    crypto_sign_keypair(my_public_key.v, my_secret_key.v);
    puts("public key for this node:");
    print_public_key(&my_public_key);
    puts("");
}


struct Key random_key() {
    // NOTE: that this is a **terrible** random key generator
    // TODO(cw): find better solution
    puts("using an insecure random key generator");
    struct Key key;
    for (int i = 0; i < KEY_BYTES; i++) {
        key.v[i] = rand_r(&random_seed) % 256;
    }
    return key;
}


struct Key identity_key(struct Key *value, struct Hash *previous, struct PublicKey *public_key, struct Signature *signature) {
    struct Key key;
    crypto_generichash_state state;

    crypto_generichash_init(&state, NULL, 0, sizeof key.v);

    crypto_generichash_update(&state, value->v, sizeof value->v);
    crypto_generichash_update(&state, previous->v, sizeof previous->v);
    crypto_generichash_update(&state, public_key->v, sizeof public_key->v);
    crypto_generichash_update(&state, signature->v, sizeof signature->v);

    crypto_generichash_final(&state, key.v, sizeof key.v);

    return key;
}


struct Signature identity_signature(struct Key *value_key, struct Hash *previous, struct PublicKey *public_key, struct SecretKey *secret_key) {
    struct Signature signature;

    crypto_sign_state state;

    crypto_sign_init(&state);

    crypto_sign_update(&state, value_key->v, sizeof value_key->v);
    crypto_sign_update(&state, previous->v, sizeof previous->v);
    crypto_sign_update(&state, public_key->v, sizeof public_key->v);

    crypto_sign_final_create(&state, signature.v, NULL, secret_key->v);

    return signature;
}


bool identity_signature_valid(struct Entry *entry) {
    assert(entry && entry->type == ENTRY_ID);

    size_t signed_size = KEY_BYTES + HASH_BYTES + crypto_sign_PUBLICKEYBYTES;
    uint8_t signed_data[signed_size];

    memcpy(signed_data, entry->id.value.v, KEY_BYTES);
    memcpy(signed_data + KEY_BYTES, entry->id.previous.v, HASH_BYTES);
    memcpy(signed_data + KEY_BYTES + HASH_BYTES, entry->id.public_key.v, crypto_sign_PUBLICKEYBYTES);

    return signature_valid(&entry->id.signature, signed_data, signed_size, &entry->id.public_key);
}



struct Key value_key_hash(void *value, uint32_t size) {
    // create a key based on a value for purely content-based addressing
    struct Key key;
    assert(value);
    // TODO(cw): check min secure size?
    assert(size > 0);
    crypto_generichash(key.v, sizeof key.v, value, size, NULL, 0);
    return key;
}


struct Hash identity_hash(struct Entry *entry) {
    // create a hash for an identity
    assert(entry && entry->type == ENTRY_ID);

    struct Hash hash;
    uint8_t *data;
    size_t size;
    mpack_writer_t writer;

    // find the hash of the previous id
    mpack_writer_init_growable(&writer, (char **) &data, &size);

    write_entry(&writer, entry, true, false);

    if (mpack_writer_destroy(&writer) != mpack_ok) {
        printf("block_valid: an error occurred writing the identity!\n");
        free(data);
        exit(EXIT_FAILURE);
    }

    crypto_generichash(hash.v, sizeof hash.v, data, size, NULL, 0);
    free(data);

    return hash;
}


struct Key string_to_key(char *key_string) {
    struct Key key;
    assert(strlen(key_string) == 2 * KEY_BYTES);
    for (int i = 0; i < KEY_BYTES; i++) {
        if (!sscanf(key_string + (2 * i), "%2hhx", &key.v[i])) {
            puts("error scanning key string");
            exit(EXIT_FAILURE);
        }
    }
    return key;
}


struct Hash hash(void *value, uint32_t size) {
    // generic cryptographically secure hash
    struct Hash h;
    assert(value);
    // TODO(cw): check min secure size?
    assert(size > 0);
    crypto_generichash(h.v, sizeof h.v, value, size, NULL, 0);
    return h;
}


uint64_t index_hash(struct Key *key) {
    // non cryptographically secure quick hash
    // used to find index within store hash table of a key

    // this is not really a hash at all - we still need to ensure
    uint64_t input, hash = 0;
    size_t key_length = sizeof key->v;

    // key must fold into index hash
    assert(key_length > 8);
    // (must be power of 2)
    assert((key_length & (key_length - 1)) == 0);

    for (size_t i = 0; i < key_length / 8; i++) {
        memcpy(&input, &key->v[8*i], 8);
        hash = hash ^ input;
    }

    return hash;
    // return murmur_hash3_x64_128(id->v, sizeof id->v);
}


struct StorageProofHash storage_proof_hash(void *input, int input_length, struct StorageProofHash *seed) {
    /*
    This hash must commute such that, for input value V, and seeds a and b: 
    h(h(V, a), b) == h(h(V, b), a)

    The implementation below meets this definition but is NOT cryptographically secure!
    Also, it only considers the first part of the input
    TODO(cw): find a better hash function
    */

    struct StorageProofHash out = *seed;

    for (int i = 0; i < (input_length > KEY_BYTES ? input_length : KEY_BYTES); i++) {
        out.v[i] = ((uint8_t *) input)[i] ^ seed->v[i];
    }

    return out;
}


int compare_keys(struct Key key1, struct Key key2) {
    // interpret ids as big-endian unsigned integers and compare:
    // key1 < key2: return -1
    // key1 = key2: return 0
    // key1 > key2: return 1
    for (int i = 0; i < KEY_BYTES; i++) {
        if ((uint8_t) key1.v[i] < (uint8_t) key2.v[i]) {
            return -1;
        } else if ((uint8_t) key1.v[i] > (uint8_t) key2.v[i]) {
            return 1;
        }
    }
    return 0;
}


int identical_prefix_bits(struct Key key1, struct Key key2) {
    // Find the number of consecutive bits, starting at the most significant bit and
    // treating the ids as big-endian integers, that are identical between the two ids
    int i;
    for (i = 0; i < KEY_BYTES; i++) {
        if (key1.v[i] != key2.v[i]) {
            break;
        }
    }

    if (i == KEY_BYTES) {
        return KEY_BYTES * 8;
    }

    for (int j = 0; j < 8; j++) {
        if ((key1.v[i] ^ key2.v[i]) & (0x80 >> j)) {
            return (i * 8 + j);
        }
    }

    return KEY_BYTES * 8;
}


bool hashes_equal(struct Hash h1, struct Hash h2) {
    for (uint8_t i = 0; i < crypto_generichash_BYTES; i++) {
        if (h1.v[i] != h2.v[i]) {
            return false;
        }
    }
    return true;
}


bool keys_equal(struct Key *k1, struct Key *k2) {
    for (uint8_t i = 0; i < KEY_BYTES; i++) {
        if (k1->v[i] != k2->v[i]) {
            return false;
        }
    }
    return true;
}


bool public_keys_equal(struct PublicKey *pk1, struct PublicKey *pk2) {
    for (uint8_t i = 0; i < crypto_sign_PUBLICKEYBYTES; i++) {
        if (pk1->v[i] != pk2->v[i]) {
            return false;
        }
    }
    return true;
}


bool storage_proof_hashes_equal(struct StorageProofHash h1, struct StorageProofHash h2) {
    for (uint8_t i = 0; i < STORAGE_PROOF_HASH_BYTES; i++) {
        if (h1.v[i] != h2.v[i]) {
            return false;
        }
    }
    return true;
}


bool signature_valid(struct Signature *signature, uint8_t *message, int message_length, struct PublicKey *public_key) {
    int result = crypto_sign_verify_detached(signature->v, message, message_length, public_key->v);
    if (result != 0) {
        puts("invalid signature");
    }
    return result == 0;
}



void print_key(struct Key *key) {
    assert(key);
    size_t s = sizeof key->v;
    assert(s == KEY_BYTES);
    assert(s >= 16);
    printf("0x");
    for (size_t i = 0; i < 7; i++) {
        printf("%02x", (key->v)[i]);
    }
    printf("....");
    for (size_t i = s - 7; i < s; i++) {
        printf("%02x", (key->v)[i]);
    }
}


void print_hash(struct Hash *h) {
    assert(h);
    assert(sizeof h->v == HASH_BYTES);
    printf("0x");
    for (size_t i = 0; i < sizeof h->v; i++) {
        printf("%02x", (h->v)[i]);
    }
}


void print_storage_proof_hash(struct StorageProofHash *h) {
    assert(h);
    assert(sizeof h->v == STORAGE_PROOF_HASH_BYTES);
    printf("0x");
    for (size_t i = 0; i < sizeof h->v; i++) {
        printf("%02x", (h->v)[i]);
    }
}


void print_public_key(struct PublicKey *public_key) {
    assert(sizeof public_key->v == crypto_sign_PUBLICKEYBYTES);
    printf("0x");
    for (size_t i = 0; i < sizeof public_key->v; i++) {
        printf("%02x", (public_key->v)[i]);
    }
}

void print_secret_key(struct SecretKey *secret_key) {
    assert(sizeof secret_key->v == crypto_sign_SECRETKEYBYTES);
    printf("SECRET KEY: DO NOT PRINT THIS IN PRODUCTION: 0x");
    for (size_t i = 0; i < sizeof secret_key->v; i++) {
        printf("%02x", (secret_key->v)[i]);
    }
}

void print_signature(struct Signature *s) {
    assert(sizeof s->v == crypto_sign_BYTES);
    printf("0x");
    for (size_t i = 0; i < sizeof s->v; i++) {
        printf("%02x", (s->v)[i]);
    }
}


// // Murmur_hash3: see https://github.com/PeterScott/murmur3/blob/master/murmur3.c
// // ---------------------------------------------------------------

// static const uint32_t MURMUR_HASH_SEED = 0;

// #define BIG_CONSTANT(x) (x##LLU)

// // static FORCE_INLINE uint32_t rotl32(uint32_t x, int8_t r) {
// //     return (x << r) | (x >> (32 - r));
// // }

// static FORCE_INLINE uint64_t rotl64(uint64_t x, int8_t r) {
//     return (x << r) | (x >> (64 - r));
// }

// // Finalization mix - force all bits of a hash block to avalanche
// // static FORCE_INLINE uint32_t fmix32(uint32_t h) {
// //     h ^= h >> 16;
// //     h *= 0x85ebca6b;
// //     h ^= h >> 13;
// //     h *= 0xc2b2ae35;
// //     h ^= h >> 16;

// //     return h;
// // }

// static FORCE_INLINE uint64_t fmix64(uint64_t k) {
//     k ^= k >> 33;
//     k *= BIG_CONSTANT(0xff51afd7ed558ccd);
//     k ^= k >> 33;
//     k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53);
//     k ^= k >> 33;

//     return k;
// }

// /*
// Block read - if your platform needs to do endian-swapping or can only
// handle aligned reads, do the conversion here
// */
// #define getblock(p, i) (p[i])

// // #define ROTL32(x, y) rotl32(x, y)
// #define ROTL64(x, y) rotl64(x, y)

// #define BIG_CONSTANT(x) (x##LLU)


// struct IndexHash murmur_hash3_x64_128(const void *input, const int input_length) {
//     /*
//     Non-cryptographic hash function
//     - Quickly hashes input to 128 bit value for hash table
//     - Optimized for x64 architecture
//     */

//     struct IndexHash out;
//     assert(sizeof(struct IndexHash) == 16);

//     const uint8_t * data = (const uint8_t*) input;
//     const int nblocks = input_length / 16;
//     int i;

//     uint64_t h1 = MURMUR_HASH_SEED;
//     uint64_t h2 = MURMUR_HASH_SEED;

//     uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
//     uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);

//   //----------
//   // body

//     const uint64_t * blocks = (const uint64_t *)(data);

//     for (i = 0; i < nblocks; i++) {
//         uint64_t k1 = getblock(blocks, i*2+0);
//         uint64_t k2 = getblock(blocks, i*2+1);

//         k1 *= c1; k1  = ROTL64(k1, 31); k1 *= c2; h1 ^= k1;

//         h1 = ROTL64(h1, 27); h1 += h2; h1 = h1*5+0x52dce729;

//         k2 *= c2; k2  = ROTL64(k2, 33); k2 *= c1; h2 ^= k2;

//         h2 = ROTL64(h2, 31); h2 += h1; h2 = h2*5+0x38495ab5;
//     }

//     //----------
//     // tail

//     const uint8_t * tail = (const uint8_t*)(data + nblocks*16);

//     uint64_t k1 = 0;
//     uint64_t k2 = 0;

//     switch (input_length & 15) {
//     case 15: k2 ^= (uint64_t)(tail[14]) << 48;
//     case 14: k2 ^= (uint64_t)(tail[13]) << 40;
//     case 13: k2 ^= (uint64_t)(tail[12]) << 32;
//     case 12: k2 ^= (uint64_t)(tail[11]) << 24;
//     case 11: k2 ^= (uint64_t)(tail[10]) << 16;
//     case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
//     case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
//         k2 *= c2; k2  = ROTL64(k2, 33); k2 *= c1; h2 ^= k2;

//     case  8: k1 ^= (uint64_t)(tail[ 7]) << 56;
//     case  7: k1 ^= (uint64_t)(tail[ 6]) << 48;
//     case  6: k1 ^= (uint64_t)(tail[ 5]) << 40;
//     case  5: k1 ^= (uint64_t)(tail[ 4]) << 32;
//     case  4: k1 ^= (uint64_t)(tail[ 3]) << 24;
//     case  3: k1 ^= (uint64_t)(tail[ 2]) << 16;
//     case  2: k1 ^= (uint64_t)(tail[ 1]) << 8;
//     case  1: k1 ^= (uint64_t)(tail[ 0]) << 0;
//         k1 *= c1; k1  = ROTL64(k1, 31); k1 *= c2; h1 ^= k1;
//     }

//     //----------
//     // finalization

//     h1 ^= input_length; h2 ^= input_length;

//     h1 += h2;
//     h2 += h1;

//     h1 = fmix64(h1);
//     h2 = fmix64(h2);

//     h1 += h2;
//     h2 += h1;

//     ((uint64_t *) out.v)[0] = h1;
//     ((uint64_t *) out.v)[1] = h2;

//     return out;
// }
// // ---------------------------------------------------------------
