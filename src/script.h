// Copyright 2017 Chris Walker

#ifndef SCRIPT_H_
#define SCRIPT_H_

#include "types.h"
#include "utils.h"
#include "crypto.h"

int run_scheme(int argc, char *argv[]);

struct Script *script_init();

// does the script require a prior output to use as an input or does it generate new assets?
bool input_script_requires_prior_output(struct Script *);
// check if the script is formatted correctly
bool script_valid(struct Script *, bool is_input);
// actually run a script
// TODO(cw): clean this up
bool input_script_unlocks_output(
    void *block,
    uint16_t this_tx_index,
    struct Key *asset,
    currency amount,
    struct Script *script,
    uint8_t *content,
    uint32_t content_size);

void print_script(struct Script *);

#endif  // SCRIPT_H_
