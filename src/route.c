// Copyright 2017 Chris Walker

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <inttypes.h>

#include "route.h"
#include "utils.h"
#include "time.h"
#include "string.h"
#include "network.h"
#include "message.h"
#include "serialize.h"
#include "crypto.h"

/* 
if κ nodes should have the value in theory, we will recursively
search k_search nodes for redundancy
*/
// #define k_search 8

/*
how many searches may be run in parallel? (We need to record
searches in progress while other nodes are returning
information about the keys we requested)
*/
#define MAX_SEARCHES 8

// Minimum delay between steps of a search in seconds
#define SEARCH_STEP_DELAY 10

// Time to wait before considering a message sent to a node
// without a response to be lost, in seconds
#define MESSAGE_TIMEOUT 10

// How many times will we attempt to message a node before
// marking it as unresponsive and giving up
#define MESSAGE_TRIES 2

static struct Bucket *buckets;

static struct Search **searches;
static uint32_t n_searches = 0;

static struct Key distance(struct Key key1, struct Key key2);
static int lowbit(struct Key key);

static bool bucket_covers_key(struct Bucket *bucket, struct Key key);
static struct Key bucket_middle_key(struct Bucket *bucket);
static void split_bucket(struct Bucket *bucket);
static struct Bucket *bucket_find(struct Key);
static struct Node *bucket_replace_worst_node(struct Bucket *bucket, struct Node *node);
static struct Node *bucket_find_node(struct Bucket *bucket, struct Key key);

static struct Search *search_init(struct Key target_key);
struct Search *search_find(struct Key search_key);
static void search_add_node(struct Search *search, struct Node *node);
static struct Bucket *next_closest_bucket(struct Key target_key, int identical_prefix_bits);
static void search_step(struct Search *search);
static void search_free(struct Search *search);


struct Key distance(struct Key key1, struct Key key2) {
    /*
    bitwise XOR keys and return result, treating the result
    as an unsigned integer distance metric. When more significant
    bits differ, the distance is larger.
    
    Note: some counterintuitive things happen with distances, e.g.
    by this metric 0 is closer to 2 than to 1, which does not match 
    the normal (Euclidean) distance metric.

    For example, compare the distance metric d() for the following
    unsigned integers on a big-endian computer:

    0 = 0b000000
    1 = 0b000001
    2 = 0b000010

    d(0,1) = d(1,0) = 0b000001
    d(1,2) = d(2,1) = 0b000011
    d(0,2) = d(2,0) = 0b000010

    ∴ d(1,2) > d(0,2) > d(0,1)

    */
    struct Key key3;

    for (int i = 0; i < KEY_BYTES; i++) {
        key3.v[i] = key1.v[i] ^ key2.v[i];
    }
    return key3;
}


int lowbit(struct Key key) {
    // Find the least significant bit in an Key that is 1, treating
    // an Key as a big-endian unsigned integer.
    int i, j;
    for (i = KEY_BYTES - 1; i >= 0; i--)
        if (key.v[i] != 0)
            break;

    if (i < 0)
        // every bit is 0
        return -1;

    for (j = 7; j >= 0; j--)
        if ((key.v[i] & (0x80 >> j)) != 0)
            break;

    return 8 * i + j;
}


struct Node *node_init(
    struct Key key,
    enum NodeType type,
    struct sockaddr_storage datagram_address,
    struct sockaddr_storage stream_address) {
    // Create or find a node on the heap: ensure there is at most one node with this Key in the routing table.
    struct Node *node;
    if (type == CLIENT_NODE || !(node = node_find(key))) {
        node = calloc_or_exit(1, sizeof(struct Node));
    }
    node->key = key;
    node->type = type;
    node->datagram_address = datagram_address;
    node->stream_address = stream_address;

    // try to add this node to the routing table
    if (node->type == SST_NODE) {
        node_add(node);
    }

    return node;
}


bool bucket_covers_key(struct Bucket *bucket, struct Key key) {
    if (compare_keys(key, bucket->first_key) < 0)
        return false;
    return !bucket->next || compare_keys(*my_node_key, bucket->next->first_key) < 0;
}

struct Key bucket_middle_key(struct Bucket *bucket) {
    // return the middle key of a bucket
    struct Key key;
    int bit1 = lowbit(bucket->first_key);
    int bit2 = bucket->next ? lowbit(bucket->next->first_key) : -1;
    int bit = 1 + ((bit1 > bit2) ? bit1 : bit2);

    // print_bucket(bucket);
    if (bucket->next) {
        printf("first key of next bucket: ");
        print_key(&bucket->next->first_key);
        puts("");
    }
    // this bit must be in the range of our keys
    assert(bit <= KEY_BYTES * 8);

    memcpy(key.v, &bucket->first_key.v, KEY_BYTES);
    key.v[bit / 8] |= (0x80 >> (bit % 8));
    printf("new middle key: ");
    print_key(&key);
    puts("");
    return key;
}


void split_bucket(struct Bucket *bucket) {
    // If a bucket containing our own node is already full and
    // a new node arrives, split the bucket into two buckets
    // the new bucket will contain the upper half of the current
    // key range
    struct Node *old_nodes;
    struct Bucket *new_bucket = calloc_or_exit(1, sizeof(struct Bucket));

    // copy over data from the old bucket
    new_bucket->next = bucket->next;
    new_bucket->reply_time = bucket->reply_time;
    new_bucket->first_key = bucket_middle_key(bucket);

    // hold onto existing nodes for now
    old_nodes = bucket->nodes;

    // reset the old bucket
    bucket->next = new_bucket;
    bucket->nodes = NULL;
    bucket->count = 0;

    while (old_nodes) {
        node_add(old_nodes);
        old_nodes = old_nodes->next;
    }
}


struct Bucket *bucket_find(struct Key key) {
    struct Bucket *b;

    for (b = buckets; b != NULL; b = b->next) {
        // bucket key ranges monotonically increase and span all
        // possible keyS.
        // if we reach the end of the linked list,
        // the last bucket is the correct choice.
        assert(compare_keys(key, b->first_key) >= 0);
        if (b->next == NULL
            || compare_keys(key, b->next->first_key) < 0) {
            // there are no more buckets to check or
            // the next bucket's first key is too large
            // so this bucket is ours.
            break;
        }
    }
    // we should always find a bucket
    assert(b != NULL);
    return b;
}


struct Node *bucket_replace_worst_node(struct Bucket *bucket, struct Node *node) {
    printf("trying to replace the worst node in this bucket\n");
    // requires the bucket is already full.
    assert(bucket->count == κ);
    // TODO(cw): find the node least responsive node and ping it.
    // if the ping fails, replace that node with this one.
    // otherwise, do nothing.
    assert(node);
    return NULL;
}


struct Node *node_find(struct Key key) {
    puts("finding a bucket");
    struct Bucket *bucket = bucket_find(key);
    puts("finding a node");
    return bucket_find_node(bucket, key);
}


struct Bucket *node_add(struct Node *node) {
    // add a node to the right bucket if possible.
    // This may require splitting buckets.
    struct Bucket *bucket = bucket_find(node->key);

    for (struct Node *n = bucket->nodes; n != NULL; n = n->next) {
        if (compare_keys(n->key, node->key) == 0) {
            puts("this node is already in this bucket! Not re-adding");
            return NULL;
        }
    }

    if (bucket->count < κ) {
        // there is room to simply insert the node
        puts("there is room in this bucket");
        node->next = bucket->nodes;
        bucket->count += 1;
        bucket->nodes = node;
        node->bucket = bucket;
        return bucket;
    }

    // there is no room: can we split the bucket?
    if (bucket_covers_key(bucket, *my_node_key)) {
        puts("our key is also in this bucket: split the bucket");
        // we are also in this bucket, so we can split it
        // Once split, just try to add the node again.
        split_bucket(bucket);
        return node_add(node);
    }

    // no room, cannot split, but maybe some of the nodes are outdated
    if (bucket_replace_worst_node(bucket, node)) {
        puts("replaced worst node in bucket with this node");
        node->bucket = bucket;
        return bucket;
    }

    puts("not adding this node!");
    return NULL;
}


struct Node *bucket_find_node(struct Bucket *bucket, struct Key key) {
    // search for a node in a bucket.
    // either return the node or NULL.
    struct Node *n = NULL;

    for (n = bucket->nodes; n != NULL; n = n->next) {
        puts("comparing another node");
        if (compare_keys(key, n->key) == 0) {
            break;
        }
    }
    return n;
}


void search_start(struct Key target_key) {
    // start searching for a key
    int oldest = 0;
    struct Search *search;
    puts("search start");

    assert(n_searches <= MAX_SEARCHES);
    for (size_t i = 0; i < n_searches; i++) {
        assert(searches[i]);
        if (compare_keys(searches[i]->target_key, target_key) == 0) {
            puts("found existing search for this key: not doing anything");
            return;
        }
        if (searches[oldest]->step_time > searches[i]->step_time) {
            puts("assigning oldest search index");
            oldest = i;
        }
    }

    search = search_init(target_key);

    if (n_searches < MAX_SEARCHES) {
        vector_add((void ***) &searches, &n_searches, search, 1);
    } else {
        free(searches[oldest]);
        searches[oldest] = search;
    }

    search_step(search);
    puts("done starting search");
}


struct Search *search_init(struct Key target_key) {
    /*
    Initialize a search by adding at least min(κ, n_nodes) nodes from the buckets closest to the target key.
    */
    struct NodeSet node_set = {
        .nodes = NULL,
        .n_nodes = 0,
    };

    struct Search *search;
    // struct Bucket *bucket;
    // struct Node *node;
    printf("initializing new search, already have %"PRIu32" searches\n", n_searches);
    puts("searching for key:");
    print_key(&target_key);
    // create a new search
    search = calloc_or_exit(1, sizeof(struct Search));
    search->search_key = random_key();
    search->target_key = target_key;

    puts("finding up to κ queryable nodes");
    node_set = closest_nodes(&search->target_key, κ);
    puts("found these nodes in node set:");
    print_node_set(&node_set);

    for (size_t i = 0; i < node_set.n_nodes; i++) {
        search_add_node(search, node_set.nodes[i]);
    }
    free_node_set(&node_set);
    printf("done initializing new search key ");
    print_key(&search->search_key);
    printf("\n");
    return search;
}


struct Search *search_find(struct Key search_key) {
    struct Search *search = NULL;

    for (size_t i = 0; i < n_searches; i++) {
        if (compare_keys(searches[i]->search_key, search_key) == 0) {
            puts("found a search for this id");
            search = searches[i];
            break;
        }
    }

    if (!search) {
        puts("could not find an existing search for this key");
    }

    return search;
}


void search_add_node(struct Search *search, struct Node *node) {
    // if missing, add a node to the the search's linked list of nodes
    // with position determined by distance from the search_key
    // if there is a node in the routing table, pass that in as well.

    struct SearchNode *new_search_node, *search_node, **prev;

    assert(search);
    assert(node);

    // puts("search_add_node: Search:");
    // print_search(search);
    // printf("possibly new node: %p", (void *) node);
    // print_node(node);

    // copy data from node to search node: only save pointer to node
    // if node is in routing table
    new_search_node = calloc_or_exit(1, sizeof(struct SearchNode));
    new_search_node->key = node->key;
    new_search_node->datagram_address = node->datagram_address;
    new_search_node->stream_address = node->stream_address;
    new_search_node->distance = distance(search->target_key, node->key);

    if (node->bucket) {
        // if this node in the routing table, link this search with the node
        // TODO(cw): is this ever even useful?
        new_search_node->node = node;
    }

    search_node = search->search_nodes;
    prev = &search->search_nodes;

    while (search_node) {
        puts("while search node");
        if (compare_keys(search_node->node->key, node->key) == 0) {
            puts("search_update: node already in search: doing nothing");
            free(new_search_node);
            return;
        }
        if (compare_keys(new_search_node->distance, search_node->distance) < 0) {
            puts("search_update: placing new search node in front of this one");
            new_search_node->next = search_node;
            break;
        }
        prev = &search_node->next;
        search_node = search_node->next;
    }

    puts("linking previous node to this new node");
    *prev = new_search_node;
    puts("search_update done");
}


struct Bucket *next_closest_bucket(struct Key target_key, int max_same_bits) {
    /*
    Find the closest bucket according to the following distance function:
    - more identical prefix bits in the bucket and target keys == a closer bucket
    - the search will only return buckets with [0, max_same_bits) same bits
    - no two buckets will have the same distance from a given target_key

    see http://stackoverflow.com/a/30655403
    */
    int n, m = -1;
    struct Bucket *bucket = buckets, *next_closest_bucket = NULL;

    while (bucket) {
        n = identical_prefix_bits(target_key, bucket->first_key);
        if (n > m && n < max_same_bits) {
            m = n;
            next_closest_bucket = bucket;
        }
        bucket = bucket->next;
    }

    return next_closest_bucket;
}


void search_step(struct Search *search) {
    /*
    Search goal: find the closest κ nodes to the key,
    or the value at the key if an exact match is found
    (distance is measured by bitwise XOR of node key)

    Search procedure:
    - while any of the min(κ, n_nodes) closest known nodes are queryable:
      - ensure min(α, n_queryable_nodes) nodes are being queried
      - mark a node as unqueryable when either:
        - it responds, possibly with new nodes that should be searched
        - it fails to respond after MESSAGE_TRIES attempts.
      - wait for nodes to reply for a bit

    Search nodes are also updated by on_node_response as
    we receive responses from earlier requests
    */

    int all = 0, waiting = 0, done = 0;
    time_t now = time(NULL);

    assert(search != NULL);
    assert(!search->done);

    search->step_time = now;

    // end the search if the closest κ nodes are done
    for (struct SearchNode *sn = search->search_nodes; sn; sn = sn->next) {
        if (sn->done && done == all) {
                // all closer searches must have succeeded as well
                puts("this search node is done");
                done++;
        }
        all++;
    }

    printf("done: %i, all: %i\n", done, all);

    if (done >= κ || (done == all && all > 0)) {
        // if n all known nodes < κ, still need to end searches
        // but if the routing table is completely empty, do not end search
        puts("the closest up to κ nodes have replied or timed out: this search is done!");
        search->done = true;
        return;
    }

    // make sure we are waiting for responses to up to α messages
    for (struct SearchNode *sn = search->search_nodes; sn; sn = sn->next) {
        if (sn->replied && !sn->done) {
            puts("node replied: good");
            sn->done = true;
        } else if (sn->message_time + MESSAGE_TIMEOUT > now) {
            puts("waiting for node to reply or time out");
        } else if (sn->tries >= MESSAGE_TRIES && !sn->done) {
            puts("node timed out");
            sn->done = true;
        } else if (waiting < α) {
            puts("sending another search message to this node");
            send_key_search(sn->node, &search->search_key, &search->target_key);
            sn->message_time = now;
            sn->tries++;
            waiting++;
        }
    }
}


void search_free(struct Search *search) {
    struct SearchNode *sn, *search_node = search->search_nodes;

    while (search_node) {
        sn = search_node;
        search_node = search_node->next;
        free(sn);
    }
    free(search);
}


void route_init(char *key_string) {
    printf("initializing kademlia routing table\n");
    // either use the provided node key string or create a random node key for this node
    printf("The %s key of this node is ", key_string ? "user-supplied" : "randomly chosen");
    my_node_key = malloc_or_exit(sizeof(struct Key));
    *my_node_key = key_string ? string_to_key(key_string) : random_key();
    print_key(my_node_key);
    printf("\n");

    // κ needs to be 4 or larger for robustness
    // (κ = 1 results in creating a lot of nested buckets for
    // very similar keys)
    assert(κ > 3);

    // we need the ability to run at least one search at a time
    assert(α > 0);

    // no overflow on searches
    assert(MAX_SEARCHES < INT16_MAX);

    // create a single bucket to hold all nodes.
    buckets = calloc_or_exit(1, sizeof(struct Bucket));
}


void on_key_search(struct Node *remote_node, struct Key *search_key, struct Key *target_key) {
    // another node has requested that we help them in their search for this target_key
    // return the min(κ, n_nodes) closest known nodes to this target key, and
    // the key's entry set if it exists
    assert(remote_node);
    assert(search_key);
    assert(target_key);

    printf("on key search for key: ");
    print_key(target_key);
    printf("\n");

    puts("we should send the results of this search back to:");
    print_node(remote_node);

    struct NodeSet node_set;
    struct EntrySet entry_set = store_get_entry(my_store, target_key);

    puts("the search matched these entries in the store:");
    print_entry_set(&entry_set);

    // send over values if exact match found
    if (entry_set.n_entries > 0) {
        puts("sending exactly matched entries back");
        send_value_search_response(remote_node, search_key, target_key, &entry_set);
    }

    // always send over closest nodes
    node_set = closest_nodes(target_key, κ);
    puts("sending closest nodes back:");
    print_node_set(&node_set);
    for (size_t i = 0; i < node_set.n_nodes; i++) {
        send_node_search_response(remote_node, search_key, node_set.nodes[i]);
    }
    free_entry_set(&entry_set);
    free_node_set(&node_set);
}


void on_node_search_response(struct Key *remote_key, struct Key *search_key, struct Node *target_node) {
    printf("another node told use about a node close to the key we requested!\n");
    struct Node *node;
    struct Search *search = search_find(*search_key);
    if (!search) {
        puts("Could not find search specified in node response message! Ignoring message!");
        return;
    }

    if (target_node->type != SST_NODE) {
        puts("Search node response returned a node that was not a full SST node! Ignoring message!");
        return;
    }

    // update the status of the responsible search node
    for (struct SearchNode *sn = search->search_nodes; sn; sn=sn->next) {
        if (compare_keys(sn->key, *remote_key) == 0) {
            sn->replied = true;
            break;
        }
    }

    // try to look up or add the target node in the routing table
    node = node_init(target_node->key, target_node->type, target_node->datagram_address, target_node->stream_address);

    // update the search with this node
    search_add_node(search, node);

    // if the node was not added to the routing table, we no longer need it
    if (!node->bucket) {
        free(node);
    }
}


void on_value_search_response(struct Key *remote_key, struct Key *search_key, struct EntrySet *target_values) {
    // if we were searching for this value, the search is now done
    assert(remote_key);
    assert(search_key);
    assert(target_values);

    for (size_t i = 0; i < target_values->n_entries; i++) {
        for (size_t j = 0; j < n_searches; j++) {
            if (compare_keys(target_values->entries[i]->key, searches[j]->target_key) == 0) {
                // this search is done!
                searches[j]->done = true;
                // printf("this search is done: got a value matching the target key from remote key: ");
                // print_key(remote_key);
                // printf("\nsearch key: ");
                // print_key(search_key);
                // printf("\nfinal search:");
                // print_search(searches[j]);
            }
        }
    }
}


struct Node *node_update(
    struct Node *node,
    enum Direction direction) {
    // update a node, whether or not the node is in the routing table
    assert(node);
    time_t now = time(NULL);

    puts("node update");

    if (direction == INBOUND) {
        node->reply_time = now;
        if (node->bucket) {
            node->bucket->reply_time = now;
        }
    } else {
        node->message_time = now;
    }

    return node;
}


void searches_update() {
    // continue working on each ongoing search
    puts("continuing searches");
    time_t now = time(NULL);
    for (size_t i = 0; i < n_searches; i++) {
        if (!searches[i]->done) {
            // no point in calling this on searches that are complete
            if (now < searches[i]->step_time + SEARCH_STEP_DELAY) {
                // printf("skipping search %zu (too soon)\n", i);
                continue;
            }
            // printf("continuing search %zu\n", i);
            search_step(searches[i]);
        }
    }
    // puts("search update complete");
    // print_searches_summary();
    // print_route_tree();
}


struct NodeSet closest_nodes(struct Key *key, uint32_t n_nodes) {
    puts("finding closest nodes");
    assert(key);
    assert(n_nodes > 0);
    assert(n_nodes <= κ);

    struct NodeSet node_set = {
        .nodes = NULL,
        .n_nodes = 0,
    };
    struct Node *node;
    struct Bucket *bucket = buckets;
    // Because next_closest_bucket is not inclusive of max_same_bits, start with a number greater than
    // the actual maximum number of same bits, which is reached when every bit in the key matches.
    int max_same_bits = KEY_BYTES * 8 + 1;

    // printf("node set starts with %zu nodes\n", node_set.n_nodes);
    print_key(key);
    bucket = next_closest_bucket(*key, max_same_bits);
    // printf("n same bits between that and this: %i\n", identical_prefix_bits(*key, *my_node_key));
    // print_bucket(bucket);

    while (
        (bucket = next_closest_bucket(*key, max_same_bits))
        && node_set.n_nodes < n_nodes) {
        puts("found next closest bucket");
        max_same_bits = identical_prefix_bits(*key, bucket->first_key);
        node = bucket->nodes;
        while (node && node_set.n_nodes < n_nodes) {
            puts("adding a node to closest nodes");
            vector_add((void ***) &node_set.nodes, &node_set.n_nodes, node, 1);
            node = node->next;
        }
    }
    printf("found %"PRIu32" closest nodes\n", node_set.n_nodes);
    return node_set;
}


void free_node_set(struct NodeSet *node_set) {
    if (node_set->n_nodes > 0) {
        // these nodes are on the heap
        free(node_set->nodes);
    }
}


void route_end() {
    printf("freeing Kademlia route table memory\n");
    // free all of the memory we have allocated
    free(my_node_key);
    while (buckets) {
        struct Bucket *bucket = buckets;
        buckets = bucket->next;
        struct Node *node = bucket->nodes;
        while (bucket->nodes) {
            bucket->nodes = node->next;
            free(node);
        }
        free(bucket);
    }

    for (size_t i = 0; i < n_searches; i++) {
        search_free(searches[i]);
    }
    n_searches = 0;
}


void print_node(struct Node *node) {
    if (!node) {
        printf("\nNode:\n (null)\n\n");
        return;
    }

    assert(node);
    assert(&node->key);
    printf("\nNode:\n- key: ");
    print_key(&node->key);
    printf("\n- type: %s", node->type == SST_NODE ? "node" : "client");
    printf("\n- datagram ");
    print_sockaddr_storage(&node->datagram_address);
    printf("\n- stream ");
    print_sockaddr_storage(&node->stream_address);
    printf("\n- last sent time: ");
    print_time(node->message_time);
    printf("\n- last reply time: ");
    print_time(node->reply_time);
    printf("\n- unanswered messages: %i\n", node->messages);
    printf("- next: %p\n", (void *) node->next);
}


void print_node_set(struct NodeSet *node_set) {
    assert(node_set);
    printf("Node set (%"PRIu32" nodes)\n", node_set->n_nodes);
    for (size_t i = 0; i < node_set->n_nodes; i++) {
        printf("node %zu at %p\n", i, (void *) &node_set->nodes[i]);
        print_node(node_set->nodes[i]);
    }
}


void print_bucket(struct Bucket *bucket) {
    struct Node *n;
    if (!bucket) {
        printf("\nBucket:\n (null)\n\n");
        return;
    }
    printf("\nBucket:\n");
    printf("- first key: ");
    print_key(&bucket->first_key);
    printf("\n- last reply time: ");
    print_time(bucket->reply_time);
    printf("\n- next bucket: %p\n", (void *) bucket->next);
    printf("- contains %i nodes:\n----\n", bucket->count);
    for (n = bucket->nodes; n != NULL; n = n->next)
        print_node(n);
    printf("----\n");
}


void print_search_node(struct SearchNode *search_node) {
    printf("Search Node:\n- distance: ");
    print_key(&search_node->distance);
    printf(
        "\n- %i tries\n- %i replies\n- last sent time: ",
        search_node->tries, search_node->replied);
    print_time(search_node->message_time);
    printf("\n");
    print_node(search_node->node);
    printf("\n");
}


void print_search(struct Search *search) {
    int n_search_nodes = 0;
    struct SearchNode *sn = search->search_nodes;
    while (sn) {
        n_search_nodes++;
        sn = sn->next;
    }

    printf(
        "Search:\n- status: %s\n- search id: ",
        search->done ? "done" : "in progress");
    print_key(&search->search_key);

    printf("\n- target key: ");
    print_key(&search->target_key);

    printf("\n- last step time: ");
    print_time(search->step_time);

    printf("\n- %i search nodes:\n----\n", n_search_nodes);

    sn = search->search_nodes;
    while (sn) {
        print_search_node(sn);
        sn = sn->next;
    }
    printf("----\n");
}


void print_searches_summary() {
    int nodes = 0, done = 0;

    printf(
        "Searches:\n"
        "done │ nodes │ done │ search key                         | target key\n"
        "─────┴───────┴──────┴────────────────────────────────────┴───────────────────────────────────\n");

    for (size_t i = 0; i < n_searches; i++) {
        nodes = 0;
        done = 0;
        for (struct SearchNode *n = searches[i]->search_nodes; n; n = n->next) {
            nodes++;
            if (n->done) {
                done++;
            }
        }
        printf("%s      %5i   %4i   ", searches[i]->done ? "✔" : "−", nodes, done);
        print_key(&searches[i]->search_key);
        printf("   ");
        print_key(&searches[i]->target_key);
        printf("\n");
    }

    printf("─────────────────────────────────────────────────────────────────────────────────────────────\n");
}


void print_routes() {
    struct Bucket *b;
    printf("\nEntire routing table:\n--------");
    for (b = buckets; b != NULL; b = b->next)
        print_bucket(b);
    printf("--------\n");
}


void print_route_tree() {
    struct Bucket *bucket = buckets;
    int prefix_bits;

    printf(
        "Routing tree:\n"
        "nodes │ last reply          │ first key prefix\n"
        "──────┴─────────────────────┴─────────────────────────────────\n");

    while (bucket) {
        prefix_bits = identical_prefix_bits(bucket->first_key, *my_node_key);

        printf("%5i   ", bucket->count);
        print_time(bucket->reply_time);
        printf("   0x");
        for (int i = 0; i < prefix_bits / 8; i++) {
            printf("%02x", bucket->first_key.v[i]);
        }
        printf("\n");
        bucket = bucket->next;
    }
    printf("──────────────────────────────────────────────────────────────\n");
}
