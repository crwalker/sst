// Copyright 2017 Chris Walker
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <stdatomic.h>
#include <assert.h>
#include <libpq-fe.h>

#include "types.h"
#include "utils.h"
#include "store.h"
#include "crypto.h"
#include "mpack.h"
#include "serialize.h"

/*
Helpful psql hints
\l - list databases
\c - change database
\d+ table_name - describe table
\dt - list tables
\dn - list schemas
\du - list users
\dt+ table_name - list table details
\dT - list data types
\dD - list data domains
'select * from pg_prepared_statements;' - show prepared statements
'delete from sst.value' - delete all sst values
'alter table sst.value alter column type set not null;' - change column "type" to disable null values
'alter table sst.value alter column foo type bar' - change column "foo" to have type or domain "bar"
*/

static bool entry_open(struct Key key);

static PGconn *pg_conn = NULL;

static void pg_connect() {
    assert(USING_POSTGRES);
    puts("connecting to postgres");
    char *conninfo = "host=postgres port=5432 dbname=sst user=sst password=testpassword connect_timeout=10";
    pg_conn = PQconnectdb(conninfo);

    if (PQstatus(pg_conn) != CONNECTION_OK) {
        printf("Connection to database failed: %s", PQerrorMessage(pg_conn));
        exit(EXIT_FAILURE);
    }
    puts("connection succeeded");
}

static void pg_prepare_schema() {
    assert(USING_POSTGRES);
    PGresult *pg_result;

    puts("preparing postgres schema");

    char *commands[] = {
        // "create database skyforge";
        // TODO(cw): solve domains: SELECT typname FROM pg_catalog.pg_type JOIN pg_catalog.pg_namespace ON pg_namespace.oid = pg_type.typnamespace WHERE typtype = 'd' AND nspname = 'sst';
        "create domain key_d bytea check (length(value) = 32);",
        "create domain hash_d bytea check (length(value) = 32);",
        "create domain signature_d bytea check (length(value) = 64);",
        "create domain public_key_d bytea check (length(value) = 32);",
        "create schema if not exists sst;",
        "create table if not exists sst.value(key key_d primary key, live bool not null, type key_d not null, data bytea not null);",
        // note that we cannot use the identity key as the primary key because multiple identity entries will share the same primary key
        "create table if not exists sst.identity(hash key_d primary key, key key_d not null, live bool not null, value key_d not null, previous hash_d, public_key public_key_d not null, signature signature_d not null);",
    };

    bool error_ok[] = {
        true,
        true,
        true,
        true,
        false,
        false,
        false
    };

    printf("size: %zu \n", sizeof commands / sizeof commands[0]);
    for (size_t i = 0; i < sizeof commands / sizeof commands[0]; i++) {
        pg_result = PQexec(pg_conn, commands[i]);
        if (PQresultStatus(pg_result) != PGRES_COMMAND_OK) {
            printf("postgres transaction failed: %s %s", commands[i], PQerrorMessage(pg_conn));
            if (!error_ok[i]) {
                PQclear(pg_result);
                exit(EXIT_FAILURE);
            }
        }
    }

    PQclear(pg_result);

    //char *drop = "drop table if exists sst.value, sst.identity; drop domain if exists type_d, signature_d, public_key_d; ";
    // need to track keys

    // // set up prepared statements because they are robust to sql injection
    // char *avps = "prepare insert_value (key_d, bool, key_d, bytea) as insert into sst.value values($1, $2, $3, $4);";
    // char *aips = "prepare insert_identity (key_d, bool, key_d, key_d, public_key_d, signature_d) as insert into sst.identity values($1, $2, $3, $4, $5, $6);";

    puts("done preparing postgres schema");
}


static void pg_load_entries(struct Store *store) {
    puts("loading entries from postgres");
    PGresult *pg_result;
    struct Entry *entry;

    int key_col, live_col, type_col, data_col;
    int key_len, live_len, type_len, data_len;
    char *key_p, *live_p, *type_p, *data_p;

    // using PQexecParams to set result format to binary
    pg_result = PQexecParams(pg_conn, "select * from sst.value;", 0, NULL, NULL, NULL, NULL, 1);

    if (PQresultStatus(pg_result) != PGRES_TUPLES_OK) { // note! not PGRES_COMMAND_OK
        printf("postgres transaction failed: %s", PQerrorMessage(pg_conn));
        PQclear(pg_result);
        exit(EXIT_FAILURE);
    }

    key_col = PQfnumber(pg_result, "key");
    live_col = PQfnumber(pg_result, "live");
    type_col = PQfnumber(pg_result, "type");
    data_col = PQfnumber(pg_result, "data");

    for (int i = 0; i < PQntuples(pg_result); i++) {
        // get row
        key_p = PQgetvalue(pg_result, i, key_col);
        live_p = PQgetvalue(pg_result, i, live_col);
        type_p = PQgetvalue(pg_result, i, type_col);
        data_p = PQgetvalue(pg_result, i, data_col);

        key_len = PQgetlength(pg_result, i, key_col);
        live_len = PQgetlength(pg_result, i, live_col);
        type_len = PQgetlength(pg_result, i, type_col);
        data_len = PQgetlength(pg_result, i, data_col);

        entry = calloc_or_exit(1, sizeof(struct Entry));

        *entry = (struct Entry) {
            .live = (bool) *live_p,
            .type = ENTRY_VALUE,
            .value = {
                .v = malloc_or_exit(data_len),
                .size = data_len,
            },
        };

        memcpy(entry->value.v, data_p, data_len);
        memcpy(entry->value.type_key.v, type_p, type_len);
        entry->key = value_key_hash(entry->value.v, entry->value.size);

        store_add_entry(store, entry);
    }

    puts("done loading entries from postgres");
}


static void pg_save_entry(struct Entry *entry) {
    assert(USING_POSTGRES);
    assert(entry);
    puts("saving entry to postgres");

    PGresult *pg_result;
    struct Key key = atomic_load(&entry->key);
    const char *live_str = atomic_load(&entry->live) ? u8"true"  : u8"false";


    if (entry->type == ENTRY_VALUE) {
        const char *param_values[4] = {
            (char *) key.v,
            live_str,
            (char *) entry->value.type_key.v,
            entry->value.v};

        const int param_formats[4] = {1, 0, 1, 1};  // 0 for text, 1 for binary
        const int param_lengths[4] = {sizeof key.v, 0, sizeof entry->value.type_key.v, entry->value.size};  // inferred from c string length for text

        pg_result = PQexecParams(
            pg_conn,
            "insert into sst.value values($1::key_d, $2::bool, $3::key_d, $4::bytea) on conflict (key) do nothing;",
            4,  // number of params
            NULL,  // param types are explicitly cast above,
            param_values,
            param_lengths,
            param_formats,
            0);  // result format: text string

        if (PQresultStatus(pg_result) != PGRES_COMMAND_OK) {
            printf("postgres transaction failed: %s", PQerrorMessage(pg_conn));
            PQclear(pg_result);
            exit(EXIT_FAILURE);
        }
        PQclear(pg_result);

        puts("done saving entry to postgres");
        return;
    } else if (entry->type == ENTRY_ID) {
        struct Hash hash = identity_hash(entry);

        const char *param_values[7] = {
            (char *) hash.v,
            (char *) key.v,
            live_str,
            (char *) entry->id.value.v,
            zeros(entry->id.previous.v, sizeof entry->id.previous.v) ? NULL : (char *) entry->id.previous.v,
            (char *) entry->id.public_key.v,
            (char *) entry->id.signature.v};

        const int param_formats[7] = {1, 1, 0, 1, 1, 1, 1};  // 0 for text, 1 for binary

        const int param_lengths[7] = {
            sizeof hash.v,
            sizeof key.v,
            0,
            sizeof entry->id.value.v,
            zeros(entry->id.previous.v, sizeof entry->id.previous.v) ? 0 : sizeof entry->id.previous.v,
            sizeof entry->id.public_key.v,
            sizeof entry->id.signature.v};  // inferred from c string length for text

        pg_result = PQexecParams(
            pg_conn,
            // columns: identity hash, identity key, live bool, value key, previous identity hash, public key, signature
            "insert into sst.identity values($1::hash_d, $2::key_d, $3::bool, $4::key_d, $5::hash_d, $6::public_key_d, $7::signature_d) on conflict (hash) do nothing;",
            7,  // number of params
            NULL,  // param types are explicitly cast above,
            param_values,
            param_lengths,
            param_formats,
            0);  // result format: text string

        if (PQresultStatus(pg_result) != PGRES_COMMAND_OK) {
            printf("postgres transaction failed: %s", PQerrorMessage(pg_conn));
            PQclear(pg_result);
            exit(EXIT_FAILURE);
        }
        PQclear(pg_result);

        puts("done saving entry to postgres");
        return;
    }

    puts("unknown entry type");
    exit(EXIT_FAILURE);
}


static void pg_disconnect() {
    PGresult *pg_result = PQexec(pg_conn, "END");
    if (PQresultStatus(pg_result) != PGRES_COMMAND_OK) {
        printf("postgres transaction failed");
        exit(EXIT_FAILURE);
    }
    PQclear(pg_result);
}


struct Store *store_init(struct Key *key, uint64_t store_size) {
    assert(key);
    // Ensure store_size is a power of 2
    assert((store_size & (store_size - 1)) == 0);

    struct Store *store;

    // preallocate the *entire* store!
    store = calloc_or_exit(1, sizeof(struct Store));
    *store = (struct Store) {
        .entries = calloc_or_exit(store_size, sizeof(struct Entry)),
        .key = *key,
        .size = store_size,
    };
    puts("initialized new store");
    print_store(store);


    if (USING_POSTGRES) {
        pg_connect();
        pg_prepare_schema();
        pg_load_entries(store);
    }

    return store;
}


struct Entry entry_value_init(struct Key *type_key, uint8_t *value, uint32_t value_size) {
    // create a new entry holding a value
    return (struct Entry) {
        .key = value_key_hash(value, value_size),
        .type = ENTRY_VALUE,
        .live = true,
        .value = {
            .type_key = *type_key,
            .size = value_size,
            .v = value,
        },
    };
}


struct Entry entry_identity_init(
    struct Key *value,
    struct Hash *previous,
    struct PublicKey *public_key,
    struct SecretKey *secret_key) {
    // create a new entry holding an id

    struct Signature signature = identity_signature(value, previous, public_key, secret_key);

    struct Entry entry = (struct Entry) {
        .key = identity_key(value, previous, public_key, &signature),
        .type = ENTRY_ID,
        .live = true,
        .id = {
            .value = *value,
            .previous = *previous,
            .public_key = *public_key,
            .signature = signature,
        },
    };

    assert(identity_signature_valid(&entry));
    return entry;
}


bool entry_open(struct Key key) {
    // this entry is not storing anything: we are free to use it
    for (int i = 0; i < KEY_BYTES; i++) {
        if (key.v[i] != 0) {
            return false;
        }
    }
    return true;
}


void store_add_entry(struct Store *store, struct Entry *entry) {
    puts("adding entry to store");
    assert(store);
    assert(entry);

    // TODO(cw): assert that entry is valid!

    // there must be room to store the new entry, plus an open entry to
    // break the loop in store_get_entry
    assert(atomic_load(&store->used) < store->size - 1);

    struct Key key = atomic_load(&entry->key), probe;

    if (zeros(key.v, sizeof key.v) || ones(key.v, sizeof key.v)) {
        puts("invalid entry key: valid hash cannot be all zeros or ones");
        exit(EXIT_FAILURE);
    }

    for (uint64_t i = index_hash(&key);; i++) {
        // use the index modulo store size
        // (index must be in store)
        i &= store->size - 1;

        // find the first hash of the entry at this index
        probe = atomic_load(&store->entries[i].key);

        if (compare_keys(probe, entry->key) == 0) {
            // this value has already been stored
            // (using content based addressing: identical key implies identical content)
            break;
        }

        // the probed key does not match this entry
        if (!entry_open(probe)) {
            // the probed entry is already used
            continue;
        }

        // the entry is free: try to claim
        if (!atomic_compare_exchange_weak(&store->entries[i].key, &probe, entry->key)) {
            // something else just claimed it
            continue;
        }

        // We have set the key: safe to use this entry
        // atomic increment of store entry counter
        atomic_fetch_add(&store->used, 1);
        atomic_store(&store->entries[i].key, entry->key);
        atomic_store(&store->entries[i].live, entry->live);

        // nothing else will write to the following entry fields: they do not need to be atomic
        store->entries[i].type = entry->type;
        if (entry->type == ENTRY_ID) {
            store->entries[i].id.value = entry->id.value;
            store->entries[i].id.previous = entry->id.previous;
            store->entries[i].id.public_key = entry->id.public_key;
            store->entries[i].id.signature = entry->id.signature;
        } else if (entry->type == ENTRY_VALUE) {
            store->entries[i].value.type_key = entry->value.type_key;
            store->entries[i].value.size = entry->value.size;
            store->entries[i].value.v = entry->value.v;
        } else {
            puts("unknown entry type");
            exit(EXIT_FAILURE);
        }
        break;
    }
    // puts("store after value added");
    print_store(store);

    if (USING_POSTGRES) {
        pg_save_entry(entry);
    }
}


void entry_set_add_entry(struct EntrySet *entry_set, struct Entry *entry) {
    assert(entry_set);
    assert(entry);
    vector_add((void ***) &entry_set->entries, &entry_set->n_entries, entry, 1);
}


struct EntrySet store_get_entry(struct Store *store, struct Key *key) {
    // puts("attempting to get entry from store");
    // print_store(store);
    // puts("key we are attempting to find");
    // print_key(key);
    struct Key probe;
    struct EntrySet entry_set = {
        .entries = NULL,
        .n_entries = 0,
    };

    for (uint64_t i = index_hash(key);; i++) {
        // use the index modulo store size
        // (index must be in store)
        i &= store->size - 1;

        // find the first hash of the entry at this index
        probe = atomic_load(&store->entries[i].key);

        if (entry_open(probe)) {
            // No more entries in the store
            break;
        }

        // We haven't found the key yet. See if it is in this entry.
        if (compare_keys(probe, *key) == 0) {
            // we must include this entry
            entry_set_add_entry(&entry_set, &store->entries[i]);
        }
    }
    puts("found these matching entries in the store:");
    print_entry_set(&entry_set);
    // puts("complete store:");
    // print_store(store);
    return entry_set;
}


struct Entry *store_get_entry_matching_hash(struct Store *store, struct Hash *hash, int min_matching_bits) {
    // we are going to treat the beginning of the hash as a key
    assert(HASH_BYTES >= KEY_BYTES);
    assert(min_matching_bits >= 0);
    assert(min_matching_bits <= KEY_BYTES * 8);

    struct Key probe;
    struct Entry *entry = NULL;

    // puts("attempting to get entry from store matching hash");
    // print_store(store);
    // printf("must match first %i bits of hash ", min_matching_bits);
    // print_hash(hash);

    for (uint64_t i = index_hash((struct Key *) hash);; i++) {
        // use the index modulo store size
        // (index must be in store)
        i &= store->size - 1;

        // find the first hash of the entry at this index
        probe = atomic_load(&store->entries[i].key);

        if (entry_open(probe)) {
            // No more entries in the store
            break;
        }

        // See if this entry is close enough
        int same = identical_prefix_bits(*(struct Key *) hash, probe);
        printf("n same bits: %i\n", same);
        if (identical_prefix_bits(*(struct Key *) hash, probe) >= min_matching_bits) {
            // we must include this entry
            printf("this entry is close enough (index %"PRIu64")", i);
            print_entry(&store->entries[i]);
            entry = &store->entries[i];
        }
    }
    puts("found this matching entry in the store:");
    print_entry(entry);
    return entry;
}


void store_free(struct Store *store) {
    if (USING_POSTGRES) {
        pg_disconnect();
        PQfinish(pg_conn);
    }
    free((void *) store->entries);
    free(store);
}


void free_entry_set(struct EntrySet *entry_set) {
    if (entry_set->n_entries > 0) {
        // these nodes are on the heap
        free(entry_set->entries);
    }
}


void print_entry(struct Entry *entry) {
    if (!entry) {
        printf("Entry:\n(null)\n");
        return;
    }
    struct Key key = atomic_load(&entry->key);

    printf(
        "Entry:\n- status: %s\n- type: %s\n- index: %"PRIu64"\n- key: ",
        atomic_load(&entry->live) ? "live" : "dead",
        entry->type == ENTRY_ID ? "Id" : "Value",
        index_hash(&key));

    print_key(&key);
    printf("\n");

    if (entry->type == ENTRY_ID) {
        printf("- value key: ");
        print_key(&entry->id.value);
        printf("\n- previous id key: ");
        print_hash(&entry->id.previous);
        printf("\n- public key: ");
        print_public_key(&entry->id.public_key);
        printf("\n- signature: ");
        print_signature(&entry->id.signature);
        printf("\n");
    } else if (entry->type == ENTRY_VALUE) {
        printf("- type key: ");
        print_key(&entry->value.type_key);
        printf("\n- size: %u\n", entry->value.size);
        printf("- value: ");
        print_buffer(entry->value.v, entry->value.size);
        printf("\n");
    }
}


void print_entry_set(struct EntrySet *entry_set) {
    assert(entry_set);
    printf("Value set (%"PRIu32" entries):\n", entry_set->n_entries);
    for (size_t i = 0; i < entry_set->n_entries; i++) {
        print_entry(entry_set->entries[i]);
    }
}


void print_store(struct Store *store) {
    assert(store);
    struct Key key;
    uint8_t *v;

    printf("Store: %"PRIu64" entries, (%"PRIu64" used), key ", store->size, atomic_load(&store->used));
    print_key((struct Key *) &store->key);
    printf("\n hash            │ key                                │ type │ details\n");
    puts("─────────────────┼────────────────────────────────────┼──────┼────────");
    for (size_t i = 0; i < store->size; i++) {
        key = atomic_load(&store->entries[i].key);
        if (entry_open(key)) {
            if (store->size > 25) {
                // don't try to print huge stores
                continue;
            }
            printf("%16s │ %34s | %4s |\n", "-", "-", "-");
            continue;
        }

        printf("%16"PRIu64" │ ", index_hash(&key));
        print_key(&key);
        printf(" | %4s | ", store->entries[i].type == ENTRY_ID ? "id" : "val");
        if (store->entries[i].type == ENTRY_ID) {
            printf("to value: ");
            print_key(&store->entries[i].id.value);
            printf(", previous id: ");
            print_hash(&store->entries[i].id.previous);
            printf("\n");
        } else if (store->entries[i].type == ENTRY_VALUE) {
            assert(store->entries[i].value.v);
            printf("size: %"PRIu32", 0x", store->entries[i].value.size);
            v = (uint8_t *) store->entries[i].value.v;
            for (size_t j = 0; j < store->entries[i].value.size; j++) {
                printf("%02x", v[j]);
            }
            printf("\n");
        }
    }
    puts("─────────────────┴────────────────────────────────────┴──────┴────────");
}
