// Copyright 2017 Chris Walker

#ifndef CONFIG_H_
#define CONFIG_H_

/*
Metadata
*/
#define SST_VERSION 1

/*
Cryptography
*/
/*
number of bytes in an Key.
Minimum size:
- randomly drawn Keys must be universally unique (>= ~16 bytes)
- Keys used for content-based addressing: size must prevent collision attack (>= ~32 bytes)
*/
#define KEY_BYTES 32

// number of bytes in a generic cryptographically secure hash
#define HASH_BYTES 32

// number of bytes in the commutative hash used to prove storage of a value
#define STORAGE_PROOF_HASH_BYTES 32

/*
Blockchain
*/

// how long does the blockchain timer wait before trying to mine again, in seconds
#define TRY_MINING_DELAY 50

// mininum time between blocks within a valid blockchain, in seconds
#define MIN_BLOCK_TIME 60

/*
DHT data storage
fixed maximum number of key-value pair entries in store: must be a power of 2!
*/
#define STORE_SIZE 32

/*
Networking
*/
#define STREAM_PORT "8000"
#define DATAGRAM_PORT "8001"

// maximum number of bytes in datagram socket buffer 
// TODO(cw): intelligently handle large messages
#define DATAGRAM_BUFFER_SIZE 10000
#define STREAM_BUFFER_SIZE 10000

#endif  // CONFIG_H_
