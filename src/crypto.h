// Copyright 2017 Chris Walker

#ifndef CRYPTO_H_
#define CRYPTO_H_

#include <stdbool.h>
#include <assert.h>
#include <sodium.h>

#include "types.h"

// #include "uthash.h"
// TODO(cw): speed up dumb array lookups using hashtables
// hashtables using UTHash
// void keytable_add(struct KeyTable *table, struct Key *item);
// void *keytable_find(struct KeyTable *table, struct Key *key, struct Key *item);
// void keytable_delete(struct KeyTable *table, struct Key *item);

void crypto_init();

struct Key random_key();
struct Key string_to_key(char *);

struct Hash identity_hash(struct Entry *entry);
struct Key identity_key(struct Key *value, struct Hash *previous, struct PublicKey *public_key, struct Signature *signature);
struct Signature identity_signature(struct Key *value, struct Hash *previous, struct PublicKey *public_key, struct SecretKey *);
bool identity_signature_valid(struct Entry *);

struct Key value_key_hash(void *value, uint32_t value_size);

struct Hash hash(void *value, uint32_t value_size);
uint64_t index_hash(struct Key *);
struct StorageProofHash storage_proof_hash(
    void *input,
    int input_length,
    struct StorageProofHash *seed);

int compare_keys(struct Key, struct Key);
bool keys_equal(struct Key *, struct Key *);
int identical_prefix_bits(struct Key, struct Key);
bool hashes_equal(struct Hash, struct Hash);
bool public_keys_equal(struct PublicKey *, struct PublicKey *);
bool storage_proof_hashes_equal(struct StorageProofHash, struct StorageProofHash);
bool signature_valid(struct Signature *, uint8_t *, int, struct PublicKey *);

void print_key(struct Key *);
void print_hash(struct Hash *);
void print_storage_proof_hash(struct StorageProofHash *);
void print_public_key(struct PublicKey *);
void print_secret_key(struct SecretKey *);
void print_signature(struct Signature *);

#endif  // CRYPTO_H_
