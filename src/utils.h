
// Copyright 2017 Chris Walker

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <stdbool.h>
#include <time.h>

// extremely simple unit testing
#define mu_assert(test, message) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                                if (message) {return message;} } while (0)
extern int tests_run;

// shared seed for random number generator
uint32_t random_seed;

#define FORCE_INLINE inline

void panic(char[]);

void print_time(time_t);

// TODO(cw): these are pretty similar, eh?
bool all_zeros(uint8_t *v, uint32_t size);
bool zeros(uint8_t *v, size_t size);
bool ones(uint8_t *v, size_t size);

uint32_t to_buffer(uint8_t **, void *, uint32_t);
uint32_t from_buffer(void *, uint8_t **, uint32_t);
void print_buffer(uint8_t *, uint32_t);

// ultimate minimal vectors
void vector_add(void ***, uint32_t *, void *, uint32_t);

void *calloc_or_exit(int, uint32_t);
void *malloc_or_exit(uint32_t);
void *realloc_or_exit(void *a, uint32_t b);

bool bin_to_pghex(char *, uint8_t *, size_t);

#endif  // UTILS_H_
