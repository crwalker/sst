// Copyright 2017 Chris Walker

#define _POSIX_C_SOURCE 201112L // localtime_r is a POSIX function, not in ANSI C
#include <string.h>
#include <sodium.h>
#include <assert.h>
#include <inttypes.h>

#include "config.h"
#include "blockchain.h"
#include "serialize.h"
#include "time.h"
#include "crypto.h"
#include "utils.h"
#include "network.h"
#include "script.h"


static struct Hash GENESIS_HASH = {{3, 1, 4, 1, 5, 9, 2, 6}};

// Array of pointers to all known txs, which may or may not be valid or unspent
static struct Tx **unbound_txs;
static uint32_t n_unbound_txs = 0;

// event timer that triggers block mining
static struct ev_timer *time_watcher;

static bool output_spent(
    struct Block *this_block,
    uint16_t this_tx_index,
    uint16_t this_input_index,
    struct Hash *output_tx,
    uint16_t output_index);

static bool asset_known(
    struct Block *this_block,
    uint16_t this_tx_index,
    uint16_t this_input_index,
    struct Key *asset);

static currency tx_input_sstcoin(struct Tx *);
static currency tx_output_sstcoin(struct Tx *);
static currency tx_fee_sstcoin(struct Tx *);
static currency tx_reward_sstcoin(struct Tx *);
// TODO(cw): free txs
// static void free_tx(struct Tx *);

// static bool tx_has_fee_or_reward(struct Tx *tx);

static struct Block *block_init(struct Hash previous_hash);
static bool block_add(struct Block *);
static bool block_link(struct Block *);
static bool block_linked(struct Block *block);
static struct Hash block_hash(struct Block *);
static void free_block(struct Block *);
static void block_proof(struct Block *);
static struct Hash block_merkle_root(struct Block *);
static bool block_proof_valid(struct Block *);
static bool block_valid(struct Block *);

static void create_asset(struct Block *, struct Key *asset, uint64_t quantity);

// static void block_tx_select(struct Block *new_block);
static void block_pad_txs(struct Block *);
static void block_add_reward(struct Block *block);
static void block_finish(struct Block *block);
static currency block_output_value(struct Block *block);
static void free_block(struct Block *);

static currency mining_reward();
static currency mining_fee(struct Block *block);
static void on_mine(struct ev_loop *loop, ev_timer *time_watcher, int revents);

static void add_blockchain_to_block_levels(
    struct Block *block,
    size_t level_width,
    struct Block **block_levels);
static void print_block_prefix(int line,
    uint64_t height,
    size_t level_index,
    bool print_index_connector);
static bool print_block_line(struct Block *block_levels[],
    size_t height,
    size_t level_width,
    size_t level_index,
    int line);


// SST's internal currency is represented as an asset with all 1's in the signature.
struct Key sstcoin = {
    .v = {1},
};


void unspent_outputs(struct Block *block, struct UTXO ***utxo, uint32_t *n_utxo) {
    /*
    get all tx outputs that were not spent by any tx in or prior to this block
    */
    assert(block);
    assert(n_utxo);

    struct Hash h;
    struct UTXO *u;
    struct Block *final_block = block;
    uint32_t n_outputs = 0, final_tx_index = block->n_txs - 1;
    uint16_t final_input_index = block->txs[final_tx_index]->n_inputs - 1;

    while (block) {
        for (size_t i = 0; i < block->n_txs; i++) {
            n_outputs += block->txs[i]->n_outputs;
            h = tx_hash(block->txs[i]);
            for (size_t j = 0; j < block->txs[i]->n_outputs; j++) {
                if (!output_spent(final_block, final_tx_index, final_input_index, &h, j)) {
                    puts("found an unspent output!");
                    print_output(block->txs[i]->outputs[j]);
                    u = calloc_or_exit(1, sizeof(struct UTXO));
                    *u = (struct UTXO) {
                        .tx_hash = tx_hash(block->txs[i]),
                        .tx_output_index = j,
                        .output = *block->txs[i]->outputs[j],
                    };
                    vector_add((void ***) utxo, n_utxo, u, 1);
                }
            }
        }

        block = block->previous;
    }

    printf("found %"PRIu32" unspent outputs out of %"PRIu32" known outputs\n", *n_utxo, n_outputs);
    // TODO(cw): who deallocates UTXOs?
}


bool output_spent(
    struct Block *this_block,
    uint16_t this_tx_index,
    uint16_t this_input_index,
    struct Hash *output_tx_hash,
    uint16_t output_index) {
    /*
    Find out whether the output defined by output_tx and output_index is spent,
    according to the blockchain known to the input defined by this_block, this_tx, and this_input_index
    The output is spent iff the output is referenced by any input prior to this_input
    (whether in this block or a previous block)
    */
    assert(this_block);


    struct Block *block = this_block;
    struct Input *input;
    size_t tx_range, input_range;
    printf("checking if output already spent: should not find any other input referencing tx hash ");
    print_hash(output_tx_hash);
    printf(" output index %lu\n", (unsigned long) output_index);

    while (block) {
        tx_range = block == this_block ? this_tx_index + 1 : block->n_txs;
        for (size_t i = 0; i < tx_range; i++) {
            input_range = block == this_block && i == this_tx_index ? this_input_index : block->txs[i]->n_inputs;
            for (size_t j = 0; j < input_range; j++) {
                printf("checking against tx %zu input %zu\n", i, j);
                input = block->txs[i]->inputs[j];
                assert(script_valid(&input->script, true));

                if (input_script_requires_prior_output(&input->script)
                    && hashes_equal(*output_tx_hash, input->script.tx_hash)
                    && output_index == input->script.tx_output_index) {
                    printf(
                        "Tx %u input %u double spends output %u in tx ",
                        this_tx_index,
                        this_input_index,
                        output_index);

                    print_hash(output_tx_hash);
                    printf("in block %" PRIu32 "\n", block->height);

                    return true;
                }
            }
        }
        block = block->previous;
    }

    return false;
}


bool asset_known(
    struct Block *this_block,
    uint16_t this_tx_index,
    uint16_t this_input_index,
    struct Key *asset) {
    /*
    Find out whether the asset referenced by an input transaction existed at the time of the input:
    the asset existed iff an input prior to this input created the asset using an ASSET_CREATION script
    */
    assert(this_block);
    assert(asset);

    struct Block *block = this_block;
    struct Input *input;
    size_t tx_range, input_range;

    printf("checking if asset ");
    print_key(asset);
    printf(" known to block %u, tx %u, input %u\n", block->height, this_tx_index, this_input_index);

    while (block) {
        tx_range = block == this_block ? this_tx_index + 1 : block->n_txs;
        for (size_t i = 0; i < tx_range; i++) {
            input_range = block == this_block && i == this_tx_index ? this_input_index : block->txs[i]->n_inputs;
            for (size_t j = 0; j < input_range; j++) {
                printf("checking against tx %zu input %zu\n", i, j);
                input = block->txs[i]->inputs[j];

                assert(script_valid(&input->script, true));

                if (input->script.type == ASSET_CREATION && keys_equal(&input->asset, asset)) {
                    printf("asset known\n");
                    return true;
                }
            }
        }
        block = block->previous;
    }

    puts("asset not known");
    return false;
}


void blockchain_init(struct ev_loop *loop, bool create_genesis_block) {
    n_all_blocks = 0;
    bool generated;

    if (create_genesis_block) {
        puts("creating the genesis block (you shall not pass)!");

        struct Block *block = block_init(GENESIS_HASH);
        create_asset(block, &sstcoin, 0);
        block_finish(block);
        generated = block_add(block);

        if (!generated) {
            puts("Failed to generate genesis block");
            exit(EXIT_FAILURE);
        }

        printf("done creating genesis block, n_all_blocks = %" PRIu32, n_all_blocks);
        print_block(block, true);
        print_blockchain_graph(0);
    }


    // repeatedly try to mine new blocks
    time_watcher = malloc_or_exit(sizeof (ev_timer));
    ev_init(time_watcher, on_mine);
    time_watcher->repeat = TRY_MINING_DELAY;
    ev_timer_again(loop, time_watcher);
}


void blockchain_end() {
    free(time_watcher);

    for (uint32_t i = 0; i < n_all_blocks; i++) {
        free_block(all_blocks[i]);
    }
}


struct Block *block_init(struct Hash hash) {
    // Create a new block and add it to our block set
    struct Block *block = calloc_or_exit(1, sizeof(struct Block));
    block->time = time(NULL);
    block->previous_hash = hash;
    return block;
}


void unbound_tx_add(struct Tx *tx) {
    // TODO(cw): what constitutes a transaction *identity*?
    // e.g. why can't a node repeat transactions with large mining fees in perpetuity?
    assert(tx);
    assert(tx_valid(tx));
    // either there are no existing txs or we have an array of tx pointers
    assert(n_unbound_txs == 0 || *unbound_txs);
    printf("adding tx to %"PRIu32" valid unbound txs\n", n_unbound_txs);

    printf("tx to add:\n");
    print_tx(tx);

    vector_add((void ***) &unbound_txs, &n_unbound_txs, tx, 1);
    puts("done adding txs");
}


bool block_add(struct Block *block) {
    // add a block into our known block set
    // deduplicate blocks with identity determined by hashing
    // returns true if block added, false if block a duplicate and not added
    assert(block);

    if (!block_valid(block)) {
        puts("not adding this block: it is invalid");
        free_block(block);
        return false;
    }

    // printf("maybe adding block to %zu known blocks\n", n_all_blocks);
    for (size_t i = 0; i < n_all_blocks; i++) {
        if (hashes_equal(block->hash, all_blocks[i]->hash)) {
            // let's do a bit of sanity checking to ensure these are the same block
            assert(hashes_equal(block->previous_hash, all_blocks[i]->previous_hash));
            assert(all_blocks[i]->n_txs == block->n_txs);
            for (size_t j = 0; j < all_blocks[i]->n_txs; j++) {
                assert(hashes_equal(
                    tx_hash(all_blocks[i]->txs[j]),
                    tx_hash(block->txs[j])));
            }
            // this block is no longer needed: it's a duplicate
            puts("already have this block: not adding");
            free_block(block);
            return false;
        }
    }

    puts("this is a new block: adding");

    // // add txs to known txs first
    // for (size_t i = 0; i < block->n_txs; i++) {

    // }

    // this is a new block
    vector_add((void ***) &all_blocks, &n_all_blocks, block, 1);

    puts("done adding block");
    return true;
}


struct Hash *missing_block_hash(struct Block *block) {
    // follow a block's linked list, returning either NULL for a valid
    // blockchain or the hash of the first missing block in the blockchain
    assert(block);
    while (block) {
        if (!block->previous) {
            return hashes_equal(block->previous_hash, GENESIS_HASH) ?
                NULL
                :
                &block->previous_hash;
        }

        block = block->previous;
    }
    return NULL;
}


bool block_linked(struct Block *block) {
    // follow a block's links, ensuring that they are valid
    if (hashes_equal(block->previous_hash, GENESIS_HASH)) {
        // this is a genesis block: stop here, we cannot have a previous block
        assert(block->previous == NULL);
        return true;
    }

    if (!block->previous) {
        // this block has not yet been linked to blocks within all_blocks
        return false;
    }

    // this block has been linked: ensure it has been done correctly.
    assert(hashes_equal(block->previous_hash, block->previous->hash));

    return block_linked(block->previous);
}


bool block_link(struct Block *block) {
    // try to link the block and its parents until we reach a genesis block.
    assert(block);

    if (hashes_equal(block->previous_hash, GENESIS_HASH)) {
        // puts("this block claims to be the genesis block, ok.");
        return true;
    }

    // puts("linking block with previous block");
    // link a new block to our existing blocks if possible
    block->previous = block_find(&block->previous_hash);
    if (!block->previous) {
        puts("new block references unkown previous block!");
        return false;
    }

    return block_link(block->previous);
}


void tx_add_inputs(struct Tx *tx, struct Input *input, uint16_t n_inputs) {
    // add an array of inputs to a tx
    vector_add((void ***) &tx->inputs, (uint32_t *) &tx->n_inputs, input, n_inputs);
}


void tx_add_outputs(struct Tx *tx, struct Output *output, uint16_t n_outputs) {
    // add an array of outputs to a tx
    vector_add((void ***) &tx->outputs, (uint32_t *) &tx->n_outputs, output, n_outputs);
}


void block_add_txs(struct Block *block, struct Tx *txs, uint16_t n_txs) {
    vector_add((void ***) &block->txs, (uint32_t *) &block->n_txs, txs, n_txs);
}


void create_asset(struct Block *block, struct Key *asset, uint64_t quantity) {
    struct Tx *tx = tx_init();
    struct Input *input;
    struct Output *output;

    input = input_init((struct Input) {
        .asset = *asset,
        .amount = quantity,
        .script = {
            .type = ASSET_CREATION,
        },
    });

    output = output_init((struct Output) {
        .asset = *asset,
        .amount = quantity,
        .script = {
            .type = PUBLIC_KEY,
            .public_key = my_public_key,
        }
    });

    tx_add_inputs(tx, input, 1);
    tx_add_outputs(tx, output, 1);
    block_add_txs(block, tx, 1);
}


void block_pad_txs(struct Block *block) {
    // Want valid merkle trees, but also don't want tons of dummy content
    // block n_txs must be 2^n for n >= 0
    struct Tx *dummy_tx;
    struct Input *dummy_input;
    struct Output *dummy_output;

    dummy_input = input_init((struct Input) {
        .asset = sstcoin,
        .amount = 0,
        .script = {
            .type = FEE,
        },
    });

    dummy_output = output_init((struct Output) {
        .asset = sstcoin,
        .amount = 0,
        .script = {
            .type = NEVER,
        }
    });

    while ((block->n_txs & block->n_txs - 1) != 0 || block->n_txs == 0) {
        puts("padding block with a dummy tx");
        dummy_tx = tx_init();
        tx_add_inputs(dummy_tx, dummy_input, 1);
        tx_add_outputs(dummy_tx, dummy_output, 1);
        block_add_txs(block, dummy_tx, 1);
    }
}


void free_block(struct Block *block) {
    for (size_t i = 0; i < block->n_txs; i++) {
        free(block->txs[i]);
    }
    free(block->txs);
    free(block);
}


uint32_t block_height(struct Block *block) {
    uint32_t height = 0;

    while (block && block->previous) {
        block = block->previous;
        height++;
    }
    return height;
}


struct Hash block_hash(struct Block *block) {
    // TODO(cw): can we hash w/o serialization in separate buffer?
    struct Hash hash;

    uint8_t *data;
    size_t data_size;

    mpack_writer_t writer;
    mpack_writer_init_growable(
        &writer,
        (char **) &data,
        (size_t *) &data_size);

    write_block(&writer, block, false);

    if (mpack_writer_destroy(&writer) != mpack_ok) {
        printf("block_hash: an error occurred encoding the block!\n");
        exit(EXIT_FAILURE);
    }

    crypto_generichash(hash.v, sizeof hash.v, data, data_size, NULL, 0);


    free(data);
    return hash;
}


struct Input *input_init(struct Input in) {
    struct Input *input = calloc_or_exit(1, sizeof(struct Input));
    memcpy(input, &in, sizeof(struct Input));
    return input;
}


struct Output *output_init(struct Output in) {
    struct Output *output = malloc_or_exit(sizeof(struct Output));
    memcpy(output, &in, sizeof(struct Output));
    return output;
}


struct Tx *tx_init() {
    // create a new transaction
    struct Tx *tx = calloc_or_exit(1, sizeof(struct Tx));
    tx->time = time(NULL);
    return tx;
}


struct Output *output_find(
    struct Block *this_block,
    uint16_t this_tx_index,
    struct Hash *tx_h,
    uint16_t output_index) {
    /*
    Some input scripts reference a prior output. The referenced output may be in the same
    new block as the input spending it, but in a prior tx, or it may be in a prior block.
    This function only attempts to locate the referenced output. it doesn't check whether
    the referenced output is valid, unspent, or otherwise matches the input.
    */
    printf("looking for output that matches input\n");
    print_hash(tx_h);
    size_t n_txs;
    struct Hash h;

    struct Block *block = this_block;

    while (block) {
        // if the output is in this block, it must be in a prior tx
        n_txs = block == this_block ? this_tx_index : block->n_txs;
        for (size_t i = 0; i < n_txs; i++) {
            h = tx_hash(block->txs[i]);
            printf("checking tx %zu\n", i);
            if (hashes_equal(h, *tx_h)) {
                assert(output_index < block->txs[i]->n_outputs);
                puts("found a matching output!");
                return block->txs[i]->outputs[output_index];
            }
        }
        block = block->previous;
    }
    puts("could not find a matching output!");
    return NULL;
}


void print_input(struct Input *input) {
    if (!input) {
        printf("\nInput:\n (null)\n");
        return;
    }

    // TODO(cw): print validity w/o checking sig each time
    printf(
        "\nInput:\n"
        "- amount: %"PRIi64"\n"
        "- ",
        input->amount);

    print_script(&input->script);
    printf("\n");
}


void print_output(struct Output *output) {
    if (!output) {
        printf("\nOutput:\n (null)\n");
        return;
    }

    printf("\nOutput:\n- amount: %"PRIi64"\n- ", output->amount);
    print_script(&output->script);
    printf("\n");
}


void print_tx(struct Tx *tx) {
    if (!tx) {
        printf("\nTx:\n (null)\n");
        return;
    }

    printf("\nTx:\n"
        "(%s)\n"
        "(fee: %"PRIi64")\n"
        "(reward: %"PRIi64")\n"
        "- %u inputs of value %"PRIi64"\n"
        "- %u outputs of value %"PRIi64"\n",
        tx_valid(tx) ? "valid" : "invalid",
        tx_fee_sstcoin(tx),
        tx_reward_sstcoin(tx),
        tx->n_inputs,
        tx_input_sstcoin(tx),
        tx->n_outputs,
        tx_output_sstcoin(tx));

    printf("----\n");

    for (size_t i = 0; i < tx->n_inputs; i++) {
        print_input(tx->inputs[i]);
    }

    for (size_t i = 0; i < tx->n_outputs; i++) {
        print_output(tx->outputs[i]);
    }
    printf("----\n");
}


void print_block(struct Block *block, bool include_txs) {
    if (!block) {
        printf("\nBlock:\n (null)\n");
        return;
    }

    printf("\nBlock:\n"
        "%s"
        "%s"
        "(height: %"PRIu32")\n"
        "-nonce: %u\n"
        "-output value: %"PRIi64"\n"
        "-time: ",
        hashes_equal(block->previous_hash, GENESIS_HASH) ? "(genesis block)\n" : "",
        block_valid(block) ? "(valid)\n" : "(invalid)\n",
        block->height,
        block->nonce,
        block_output_value(block));

    print_time(block->time);

    printf("\n-merkle root: ");
    print_buffer(block->merkle_root.v, crypto_generichash_BYTES);

    printf("\n-storage proof: ");
    print_buffer(block->storage_proof.v, sizeof block->storage_proof.v);

    printf("\n- hash: ");
    print_buffer(block->hash.v, crypto_generichash_BYTES);

    printf("\n-n txs: %u\n"
        "-previous block pointer: %p\n"
        "-previous block hash: ",
        block->n_txs,
        (void *) block->previous);

    print_buffer(block->previous_hash.v, crypto_generichash_BYTES);
    printf("\n");

    if (include_txs) {
        for (size_t i = 0; i < block->n_txs; i++) {
            print_tx(block->txs[i]);
        }

        printf("--------\n");
    }
}

void print_blockchain(struct Block *block, bool include_txs) {
    printf("\nBlockchain:\n");
    if (!block) {
        puts("(null)");
    }
    while (block) {
        print_block(block, include_txs);
        block = block->previous;
    }
}


currency tx_input_sstcoin(struct Tx *tx) {
    assert(tx);
    currency amount = 0;
    for (size_t i = 0; i < tx->n_inputs; i++) {
        amount += tx->inputs[i]->amount;
    }
    return amount;
}


currency tx_output_sstcoin(struct Tx *tx) {
    assert(tx);
    currency amount = 0;
    for (size_t i = 0; i < tx->n_outputs; i++) {
        if (keys_equal(&tx->outputs[i]->asset, &sstcoin)) {
            amount += tx->outputs[i]->amount;
        }
    }
    return amount;
}


currency tx_reward_sstcoin(struct Tx *tx) {
    assert(tx);
    currency amount = 0;
    for (size_t i = 0; i < tx->n_inputs; i++) {
        if (keys_equal(&tx->inputs[i]->asset, &sstcoin)
            && (tx->inputs[i]->script.type == STORAGE_REWARD || tx->inputs[i]->script.type == WORK_REWARD)) {
            amount += tx->inputs[i]->amount;
        }
    }
    return amount;
}


currency tx_fee_sstcoin(struct Tx *tx) {
    assert(tx);
    currency amount = 0;
    for (size_t i = 0; i < tx->n_inputs; i++) {
        if (keys_equal(&tx->inputs[i]->asset, &sstcoin) && tx->inputs[i]->script.type == FEE) {
            amount += tx->inputs[i]->amount;
        }
    }
    return amount;
}


currency block_output_value(struct Block *block) {
    assert(block);
    currency amount = 0;
    for (size_t i = 0; i < block->n_txs; i++) {
        amount += tx_output_sstcoin(block->txs[i]);
    }
    return amount;
}


void tx_sign(struct Tx *tx) {
    /*
    Each input includes a signature of the entire transaction.
    - The tx must be signed rather than the input alone, as otherwise a malicious user could
      change the tx output addresses before adding the tx into a block.
    - the signatures cannot sign themselves, so they are removed from 
    */

    struct Signature signature;

    uint8_t *data;
    size_t data_size;

    mpack_writer_t writer;
    mpack_writer_init_growable(
        &writer,
        (char **) &data,
        (size_t *) &data_size);

    write_tx(&writer, tx, false);

    if (mpack_writer_destroy(&writer) != mpack_ok) {
        printf("tx_sign: an error occurred writing the tx!\n");
        exit(EXIT_FAILURE);
    }

    crypto_sign_detached(
        signature.v,
        NULL,
        data,
        data_size,
        my_secret_key.v);

    // tack the signature on to each input that needs it
    for (size_t i = 0; i < tx->n_inputs; i++) {
        if (tx->inputs[i]->script.type == SIGNATURE) {
            tx->inputs[i]->script.signature = signature;
        }
    }

    free(data);
}


bool tx_valid(struct Tx *tx) {
    // Check that this transaction is internally consistent
    // I used a goto and I liked it
    assert(tx);

    struct BalanceSet balance_set = {
        .balances = NULL,
        .n_balances = 0,
    };

    struct Balance *balance;

    bool valid = false;
    //currency input_value = 0, output_value = 0;

    if (tx->n_inputs == 0 || tx->n_outputs == 0) {
        puts("tx invalid: must have one or more inputs and one or more outputs");
        goto done;
    }

    for (int i = 0; i < tx->n_inputs; i++) {
        // TODO(cw): disable dummy txs
        // if (zeros(tx->inputs[i]->asset.v, sizeof tx->inputs[i]->asset.v)) {
        //     printf("tx invalid: input %i has invalid zero-valued asset\n", i);
        //     goto done;
        // }
        if (!script_valid(&tx->inputs[i]->script, true)) {
            printf("tx invalid: input %i script is invalid\n", i);
            goto done;
        }

        balance = NULL;
        for (uint32_t j = 0; j < balance_set.n_balances; j++) {
            if (keys_equal(&tx->inputs[i]->asset, &balance_set.balances[j]->asset)) {
                balance = balance_set.balances[j];
                break;
            }
        }
        if (!balance) {
            balance = malloc_or_exit(sizeof(struct Balance));
            balance->asset = tx->inputs[i]->asset;
            balance->value = 0;

            vector_add((void ***) &balance_set.balances, &balance_set.n_balances, balance, 1);
        }

        balance->value += tx->inputs[i]->amount;
    }

    for (int i = 0; i < tx->n_outputs; i++) {
        // TODO(cw): disable dummy txs
        // if (zeros(tx->outputs[i]->asset.v, sizeof tx->outputs[i]->asset.v)) {
        //     printf("tx invalid: output %i has invalid zero-valued asset\n", i);
        //     goto done;
        // }
        if (!script_valid(&tx->outputs[i]->script, false)) {
            printf("tx invalid: output %i script is invalid\n", i);
            goto done;
        }

        // for assets, input value must equal output value, except:
        // for SST native asset, cannot output more value than input
        // however, if input > output, the remainder is a block fee that satisfies
        // Σ(normal_inputs) + reward + fee == Σ(outputs)

        balance = NULL;
        for (uint32_t j = 0; j < balance_set.n_balances; j++) {
            if (keys_equal(&tx->inputs[i]->asset, &balance_set.balances[j]->asset)) {
                balance = balance_set.balances[j];
                break;
            }
        }
        if (!balance) {
            printf("tx invalid: output %i spends asset ", i);
            print_key(&balance->asset);
            printf(" that was not present in tx inputs\n");
            goto done;
        }


        if (balance->value < tx->outputs[i]->amount) {
            printf("tx invalid: output %i with amount %" PRIu64 "drops balance of asset ", i, tx->outputs[i]->amount);
            print_key(&balance->asset);
            printf(" below 0\n");
            goto done;
        }

        balance->value -= tx->outputs[i]->amount;
    }

    for (uint32_t i = 0; i < balance_set.n_balances; i++) {
        if (!keys_equal(&sstcoin, &balance->asset) && balance_set.balances[i]->value > 0) {
            puts("transaction must bring all non-SST asset balances to zero");
            goto done;
        }
    }

    // all checks passed
    valid = true;

    done:
    for (uint32_t i = 0; i < balance_set.n_balances; i++) {
        free(balance_set.balances[i]);
    }
    return valid;
}


struct Hash block_merkle_root(struct Block *block) {
    puts("calculating merkle root of block");

    assert(block);
    // We can only construct a valid merkle tree if the number of transactions is a power of 2
    assert((block->n_txs > 0) && (block->n_txs & block->n_txs - 1) == 0);

    struct Hash hashes[block->n_txs];

    // generate initial hashes of actual txs
    for (size_t i = 0; i < block->n_txs; i++) {
        hashes[i] = tx_hash(block->txs[i]);
    }

    // hash adjacent hashes together until a single hash remains
    size_t n_hashes = block->n_txs;

    // Method assumes no padding around hash value in struct. Verify.
    assert(n_hashes > 0);
    assert(sizeof(struct Hash) == sizeof hashes[0].v);

    while (n_hashes > 1) {
        for (size_t i = 0; i < n_hashes / 2; i++) {
            crypto_generichash(hashes[i].v, sizeof hashes[i].v, hashes[2*i].v, 2 * sizeof hashes[i].v, NULL, 0);
        }
        n_hashes /= 2;
    }

    return hashes[0];
}


struct Block *block_find(struct Hash *previous_hash) {
    assert(previous_hash);
    for (size_t i = 0; i < n_all_blocks; i++) {
        if (hashes_equal(*previous_hash, all_blocks[i]->hash)) {
            return all_blocks[i];
        }
    }
    return NULL;
}


struct Block *block_find_highest() {
    // Note: this returns the longest chain, but the longest chain is not
    // necessarily a valid blockchain starting at a valid genesis block
    uint64_t highest_height = 0;
    struct Block *highest_block = NULL;

    for (size_t i = 0; i < n_all_blocks; i++) {
        // >= to select at least one block when max height = 0
        if (all_blocks[i]->height >= highest_height) {
            highest_block = all_blocks[i];
            highest_height = all_blocks[i]->height;
        }
    }
    return highest_block;
}


struct Block *block_find_highest_valid() {
    // Find the highest valid chain
    uint64_t highest_height = 0;
    struct Block *highest_block = NULL;

    for (size_t i = 0; i < n_all_blocks; i++) {
        // >= to select at least one block when max height = 0
        if (all_blocks[i]->height >= highest_height && block_valid(all_blocks[i])) {
            highest_block = all_blocks[i];
            highest_height = all_blocks[i]->height;
        }
    }
    return highest_block;
}


void block_proof(struct Block *block) {
    printf("generating block proof ");
    while (!block_proof_valid(block)) {
        printf(".");
        block->nonce++;
    }
    puts(" done");
}


bool block_proof_valid(struct Block *block) {
    // note: must recalculate the block hash each time: the hash field is only
    // saved to the block struct after the proof is complete
    return block_hash(block).v[0] == 0;
}

bool block_valid(struct Block *block) {
    /*
    We check the following fields on the block:
    - each tx is internally consistent
    - no tx double spends an output
    - all tx inputs reference known outputs or create outputs
    - all scripts execute correctly
    - the block reward is correct (based on reward schedule)
    - the block fee is correct (fee = Σ(normal inputs) - Σ(normal outputs))
    - the block has 2^n blocks to generate a merkle tree
    - the block has a valid proof of work
    */
    assert(block);

    uint8_t *data;
    size_t data_size;
    mpack_writer_t writer;

    currency input_sstcoin = 0, output_sstcoin = 0, fee_sstcoin = 0, reward_sstcoin = 0;

    bool this_asset_known, creating_asset;

    if (block_height(block) != block->height) {
        puts("calculated block height does not match block's claimed height");
        return false;
    }

    if (!hashes_equal(block_hash(block), block->hash)) {
        puts("calculated block hash does not match block's hash record");
        return false;
    }

    if (!((block->n_txs > 0) && ((block->n_txs & block->n_txs - 1) == 0))) {
        puts("block invalid: does not have 2^n transactions for n >= 0, cannot create a merkle root hash");
        return false;
    }

    for (size_t i = 0; i < block->n_txs; i++) {
        if (!tx_valid(block->txs[i])) {
            printf("block invalid: transaction %zu invalid\n", i);
            return false;
        }

        mpack_writer_init_growable(
            &writer,
            (char **) &data,
            (size_t *) &data_size);

        write_tx(&writer, block->txs[i], false);

        if (mpack_writer_destroy(&writer) != mpack_ok) {
            printf("block_valid: an error occurred writing the tx!\n");
            free(data);
            exit(EXIT_FAILURE);
        }

        for (size_t j = 0; j < block->txs[i]->n_inputs; j++) {
            this_asset_known = asset_known(block, i, j, &block->txs[i]->inputs[j]->asset);
            creating_asset = block->txs[i]->inputs[j]->script.type == ASSET_CREATION;

            if (this_asset_known && creating_asset) {
                puts("block invalid: found an input attempting to create a known asset");
                free(data);
                return false;
            }

            if (!this_asset_known && !creating_asset) {
                puts("block invalid: found an input attempting to use an unkown asset");
                free(data);
                return false;
            }

            if (input_script_requires_prior_output(&block->txs[i]->inputs[j]->script)
                && output_spent(
                    block,
                    i,
                    j,
                    &block->txs[i]->inputs[j]->script.tx_hash,
                    block->txs[i]->inputs[j]->script.tx_output_index)) {
                puts("block invalid: found a double spent transaction");
                free(data);
                return false;
            }

            if (!input_script_unlocks_output(
                    block,
                    i,
                    &block->txs[i]->inputs[j]->asset,
                    block->txs[i]->inputs[j]->amount,
                    &block->txs[i]->inputs[j]->script,
                    data,
                    data_size)) {
                printf(
                    "block invalid: tx %zu input %zu script does not have rights to unlock output\n",
                    i, j);
                free(data);
                return false;
            }
        }

        free(data);

        input_sstcoin += tx_input_sstcoin(block->txs[i]);
        output_sstcoin += tx_output_sstcoin(block->txs[i]);
        fee_sstcoin += tx_fee_sstcoin(block->txs[i]);
        reward_sstcoin += tx_reward_sstcoin(block->txs[i]);
    }

    if (reward_sstcoin != mining_reward()) {
        printf("block invalid: reward %"PRIi64" does not equal correct mining reward %"PRIi64"\n",
            reward_sstcoin,
            mining_reward());
    }

    if (input_sstcoin - output_sstcoin != fee_sstcoin) {
        printf("block invalid: fee %"PRIi64" does not equal input value - output value %"PRIi64"\n",
            fee_sstcoin,
            input_sstcoin - output_sstcoin);
    }

    // validate reported merkle root against transactions
    struct Hash computed_merkle_hash = block_merkle_root(block);
    if (!hashes_equal(computed_merkle_hash, block->merkle_root)) {
        puts("block invalid: bad merkle root hash");
        return false;
    }

    // validate reported block proof
    if (!block_proof_valid(block)) {
        puts("block invalid: bad proof");
        return false;
    }

    return true;
}


bool blockchain_valid(struct Block *block) {
    /* 
    Ensure that a block, and all of its predecessors, are valid
    - either the block is a valid genesis block or it is linked to a valid genesis block
    - sufficient time has elapsed between the previous block and this block
    - every linked block is also valid
    - the block has a valid proof of storage referencing an earlier storage transaction
    */
    assert(block);

    if (!block_valid(block)) {
        return false;
    }

    // either the block needs to be the genesis block or it needs to reference another valid block
    if (hashes_equal(block->previous_hash, GENESIS_HASH)) {
        // This block is a genesis block
        return true;
    }

    // the checks below only necessary for blocks that are *not* the genesis block, and assume
    // that the block is not the genesis block

    if (!block_linked(block)) {
        puts("blockchain invalid: this block is not linked to a prior block. Must use block_link() to link");
        return false;
    }

    if (block->time < block->previous->time + MIN_BLOCK_TIME) {
        puts("block invalid: insufficient time elapsed between previous block and this block!");
        return false;
    }

    return blockchain_valid(block->previous);
}


void on_receive_tx(struct Node *node, struct Tx *tx) {
    assert(node);
    assert(tx);

    print_tx(tx);

    if (!tx_valid(tx)) {
        puts("tx not valid!");
        // TODO(cw): handle gracefully
        exit(EXIT_FAILURE);
        return;
    }

    puts("adding a new tx from another node to our unbound tx set");
    // TODO(cw): connect this tx to the relevant contract and the relevant value stored
    unbound_tx_add(tx);

    puts("proving unbound txs are OK");
    for (size_t i = 0; i < n_unbound_txs; i++) {
        assert(unbound_txs[i]);
        print_tx(unbound_txs[i]);
    }
    puts("done proving unbound tx are OK");
}


void on_receive_block(struct Node *node, struct Block *block) {
    puts("on_receive_block");
    assert(node);
    assert(block);
    if (block_add(block)) {
        // block_deduplicate_txs(block);
        if (!block_link(block)) {
            puts("new block is missing a previous block: requesting missing block");
            send_block_request(node, missing_block_hash(block));
        }
    }
    print_blockchain_graph(3);
    puts("done receiving block");
}


struct Hash tx_hash(struct Tx *tx) {
    assert(tx);

    struct Hash hash;

    uint8_t *data;
    size_t data_size;

    mpack_writer_t writer;
    mpack_writer_init_growable(
        &writer,
        (char **) &data,
        (size_t *) &data_size);

    write_tx(&writer, tx, true);

    if (mpack_writer_destroy(&writer) != mpack_ok) {
        printf("tx_hash: an error occurred writing the tx\n");
        exit(EXIT_FAILURE);
    }

    crypto_generichash(hash.v, sizeof hash.v, data, data_size, NULL, 0);

    free(data);
    return hash;
}


currency mining_reward() {
    // 2^32 ... ?
    return (currency) 1 << 32;
}


currency mining_fee(struct Block *block) {
    currency fee = 0;
    for (size_t i = 0; i < block->n_txs; i++) {
        fee += tx_fee_sstcoin(block->txs[i]);
    }
    return fee;
}


void add_blockchain_to_block_levels(
    struct Block *block,
    size_t level_width,
    struct Block **block_levels) {
    // take care of all block ancestors as well
    while (block) {
        for (size_t i = 0; i < level_width; i++) {
            if (block_levels[block->height * level_width + i] == block) {
                // this block, and its ancestors, have been added already
                return;
            }
            if (!block_levels[block->height * level_width + i]) {
                // the element is empty: put the block here
                block_levels[block->height * level_width + i] = block;
                break;
            }
        }
        block = block->previous;
    }
}


void print_block_prefix(int line, uint64_t height, size_t level_index, bool print_index_connector) {
    if (level_index == 0) {
        // far left of terminal
        if (line == 1 && level_index == 0) {
            printf("h %6"PRIi64"  ", height);
            return;
        } else {
            printf("          ");
            return;
        }
    }

    printf("%s", print_index_connector && line == 6 ? "─────" : "    ");
}


bool print_block_line(struct Block *block_levels[], size_t height, size_t level_width, size_t level_index, int line) {
    struct Block *adjacent_block, *block;
    bool found_parent, print_index_connector, valid_genesis_block;
    size_t previous_level_index, adjacent_block_previous_level_index;

    block = block_levels[height * level_width + level_index];
    adjacent_block = level_width > level_index + 1 ? block_levels[height * level_width + level_index + 1] : NULL;

    valid_genesis_block = height == 0 && hashes_equal(block->previous_hash, GENESIS_HASH);

    previous_level_index = level_index;
    adjacent_block_previous_level_index = level_index + 1;

    if (height > 0) {
        found_parent = false;
        for (size_t i = 0; i < level_width; i++) {
            if (block->previous == block_levels[(height - 1) * level_width + i]) {
                previous_level_index = i;
                found_parent = true;
                break;
            }
        }
        if (!found_parent) {
            puts("no previous block found!");
            exit(EXIT_FAILURE);
        }
    }

    if (height > 0 && adjacent_block) {
        found_parent = false;
        // print_block(adjacent_block, false);
        for (size_t i = 0; i < level_width; i++) {
            // printf("comparing with %p\n", (void *) block_levels[(height - 1) * level_width + i]);
            if (adjacent_block->previous == block_levels[(height - 1) * level_width + i]) {
                adjacent_block_previous_level_index = i;
                // puts("found a parent block");
                found_parent = true;
                break;
            }
        }
        if (!found_parent) {
            puts("no previous block for adjacent block found!");
            exit(EXIT_FAILURE);
        }
    }
    // print a connector for the adjacent block only when it
    // descends from a block at an earlier level index
    print_index_connector = adjacent_block_previous_level_index <= level_index;

    // print the block
    if (line == 0) {
        printf("%s", "┌─────────────────────┐");
    } else if (line == 1) {
        printf("│ hash ");
        for (int i = 0; i < 7; i++) {
            printf("%02x", block->hash.v[i]);
        }
        printf(" │");
    } else if (line == 2) {
        char buffer[20];
        struct tm time_info;
        localtime_r(&block->time, &time_info);
        strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", &time_info);
        printf("│ %s │", buffer);
    } else if (line == 3) {
        printf("│ %15u txs │", block->n_txs);
    } else if (line == 4) {
        printf("│ %15"PRIi64" out │", block_output_value(block));
    } else if (line == 5) {
        printf("%s", "└──────────┬──────────┘");
    } else if (line == 6) {
        // print links between block and predecessor
        if (height == 0) {
            printf("           %s           ", valid_genesis_block ? "█" : "╳");
        } else if (level_index == previous_level_index) {
            printf("           %s", print_index_connector ? "├──────────" : "│           ");
        } else {
            printf("───────────%s", print_index_connector ? "┴──────────" : "╯           ");
        }
    }

    return print_index_connector;
}


void print_blockchain_graph(int depth) {
    // print out the most recent blocks in the blockchain (set depth = 0 to print all blocks)
    struct Block *block, *highest_block = block_find_highest();

    size_t n_levels = highest_block->height + 1;
    if (depth == 0) {
        depth = n_levels;
    }

    puts("Blockchain graph");
    // count the number of blocks that are at height i
    size_t *block_counts = calloc_or_exit(sizeof (size_t), n_levels);

    for (size_t i = 0; i < n_all_blocks; i++) {
        block_counts[all_blocks[i]->height]++;
    }

    size_t level_width = 0;
    for (size_t i = 0; i < n_levels; i++) {
        level_width = block_counts[i] > level_width ? block_counts[i] : level_width;
    }

    struct Block **block_levels = calloc_or_exit(sizeof (struct Block *), n_levels * level_width);

    // fill up the block levels such that the highest blockchains fill up the lowest indices
    // in the [level_width] dimension -- the highest blockchains are the most important
    size_t height = n_levels;

    while (height > 0) {
        height--;
        for (size_t i = 0; i < n_all_blocks; i++) {
            if (all_blocks[i]->height == height) {
                // put this block and its decendents in the block_levels
                add_blockchain_to_block_levels(all_blocks[i], level_width, block_levels);
            }
        }
    }

    height = n_levels;
    while (height > 0 && n_levels - height < (uint64_t) depth) {
        height--;
        // this row is complete: let's print
        for (int line = 0; line < 7; line++) {
            // for each block
            bool print_index_connector = false;
            for (size_t level_index = 0; level_index < level_width; level_index++) {
                // sanity check that we are only showing valid blocks
                block = block_levels[height * level_width + level_index];
                if (block) {
                    print_block_prefix(line, height, level_index, print_index_connector);
                    print_index_connector = print_block_line(block_levels, height, level_width, level_index, line);
                }
            }
            printf("\n");
        }
    }
    printf("\n");
    free(block_counts);
    free(block_levels);
}


void block_add_reward(struct Block *block) {
    // given a block with a set of transactions, add new transactions including:
    // - mining fee
    // - mining reward

    // ensure that nobody has already done this
    for (size_t i = 0; i < block->n_txs; i++) {
        for (size_t j = 0; j < block->txs[i]->n_inputs; j++) {
            // never accept fee or reward transactions from other nodes
            assert(block->txs[i]->inputs[j]->script.type != WORK_REWARD);
            assert(block->txs[i]->inputs[j]->script.type != STORAGE_REWARD);
            assert(block->txs[i]->inputs[j]->script.type != FEE);
        }
    }
    struct Input *reward_input, *fee_input;
    struct Output *output;

    reward_input = input_init((struct Input) {
        .asset = sstcoin,
        .amount = mining_reward(),
        .script = {
            .type = WORK_REWARD,
        },
    });

    fee_input = input_init((struct Input) {
        .asset = sstcoin,
        .amount = mining_fee(block),
        .script = {
            .type = FEE,
        }
    });

    output = output_init((struct Output) {
        .asset = sstcoin,
        .amount = mining_reward() + mining_fee(block),
        .script = {
            .type = PUBLIC_KEY,
            .public_key = my_public_key,
        },
    });

    struct Tx *tx = tx_init();
    tx_add_inputs(tx, reward_input, 1);
    tx_add_inputs(tx, fee_input, 1);
    tx_add_outputs(tx, output, 1);
    tx_sign(tx);

    print_input(reward_input);
    print_input(fee_input);
    print_output(output);

    puts("checking if tx is valid");
    assert(tx_valid(tx));
    puts("done checking if tx is valid");

    block_add_txs(block, tx, 1);
    puts("done adding txs");
}

void block_finish(struct Block *block) {
    // perform all actions needed to create a valid block from a block
    // containing 0 or more normal transactions (no rewards or fees)
    // that was started with block_init()
    assert(block);
    bool linked = block_link(block);
    assert(linked);

    block_add_reward(block);
    // pad transactions to create Merkle tree with 2^n txs
    block_pad_txs(block);
    block->merkle_root = block_merkle_root(block);
    block->height = block_height(block);
    block_proof(block);
    block->hash = block_hash(block);
}

// TODO(cw): fix SEGV on unknown address
// void block_tx_select(struct Block *new_block) {
//     // given a set of transactions, select those with the optimum payout for inclusion into a new block.
//     // the current approach is very simple: remove rewards and fee transactions so that this node
//     // can claim the rewards and fees itself
//     // printf("looking for unbound txs, have %zu known txs\n", n_all_txs);
//     printf("selecting new txs from %zu known unbound txs\n", n_unbound_txs);

//     puts("proving unbound txs are OK");
//     for (size_t i = 0; i < n_unbound_txs; i++) {
//         assert(unbound_txs[i]);
//         print_tx(unbound_txs[i]);
//     }
//     puts("done proving unbound tx are OK");

//     for (size_t i = 0; i < n_unbound_txs; i++) {
//         assert(unbound_txs[i]);
//         print_tx(unbound_txs[i]);
//         if (tx_valid(unbound_txs[i])
//             && !tx_has_fee_or_reward(unbound_txs[i])) {
//             puts("adding the following unbound tx to this new block");
//             print_tx(unbound_txs[i]);
//             block_add_txs(new_block, unbound_txs[i], 1);
//         }
//     }
// }


int min_matching_bits(struct Block *highest_block) {
    // the number of bits that must match decreases as the highest block grows older
    time_t open_time, now = time(NULL);
    assert(now > highest_block->time);
    // time elapsed since the first possible block mining time
    open_time = now - highest_block->time - MIN_BLOCK_TIME;

    // no number of bits is appropriate if insufficient time has elapsed
    assert(open_time >= 0);

    // the matching problem loses 16 bits of difficulty each second (chosen arbitrarily)
    int n_bits = 8 * KEY_BYTES - 16 * open_time;
    return n_bits > 0 ? n_bits : 0;
}


void on_mine(struct ev_loop *loop, ev_timer *time_watcher, int revents) {
    struct Block *highest_block;
    struct NodeSet node_set = {
        .nodes = NULL,
        .n_nodes = 0,
    };

    // struct Entry *entry;
    puts("on mine");

    if (EV_ERROR & revents) {
        puts("unspecified time_watcher event error");
        exit(EXIT_FAILURE);
    }

    // attempt to mine a new block
    highest_block = block_find_highest_valid();

    // cannot mine without a genesis block
    if (!highest_block) {
        puts("cannot mine until we have a genesis block");
        return;
    }

    if (highest_block->time + MIN_BLOCK_TIME > time(NULL)) {
        // puts("cannot mine until min block time has elapsed");
        return;
    }

    // entry = store_get_entry_matching_hash(my_store, &highest_block_hash, min_matching_bits(highest_block));

    // if (!entry) {
    //     puts("no stored values were close enough to mine this block");
    //     return;
    // }


    printf("mining a new block\n");
    struct Block *block = block_init(highest_block->hash);
    // block_tx_select(block);
    // printf("added %zu tx to this new block\n", block->n_txs);
    block_finish(block);
    assert(block_add(block));
    puts("mined a new block!");
    print_block(block, false);
    print_blockchain_graph(6);

    // better share the news
    node_set = closest_nodes(my_node_key, κ);

    for (size_t i = 0; i < node_set.n_nodes; i++) {
        send_block(node_set.nodes[i], block);
    }

    free_node_set(&node_set);
    ev_timer_again(loop, time_watcher);
}
