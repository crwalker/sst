// Copyright 2017 Chris Walker

// Hack, required for struct addrinfo, getnameinfo, NI_NUMERICHOST, NI_NUMERICSERV on debian
#define _POSIX_C_SOURCE 201112L
// Hack, somebody failing to define, needed for maximum length of FQDN and server name
#define NI_MAXSERV    32
#define NI_MAXHOST  1025

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <netdb.h>
#include <inttypes.h>
#include <stdatomic.h>
#include <string.h>

#include "types.h"
#include "serialize.h"
#include "network.h"
#include "route.h"
#include "crypto.h"
// #include "store.h"
#include "mpack.h"

const char *SCRIPT_TYPE_KEYS[] = {
    "publicKey",
    "signature",
    "dataHashChallenge",
    "dataHashProof",
    "fee",
    "storageReward",
    "workReward",
    "never",
    "htl",
    "value",
    "assetCreation",
};


const char *ENTRY_TYPE_KEYS[] = {
    u8"unknown",
    u8"value",
    u8"identity",
};


// TODO(cw): put key strings in intelligent location
const char*NODE_TYPE_KEYS2[] = {
    u8"unkown",
    u8"sst",
    u8"client",
};

void write_input(mpack_writer_t *writer, struct Input *input, bool include_signatures);
void write_output(mpack_writer_t *writer, struct Output *output, bool include_signatures);


struct Key extract_key(mpack_tree_t *tree, mpack_node_t node) {
    struct Key k = {
        .v = {0},
    };
    if (mpack_node_data_len(node) != KEY_BYTES) {
        printf("Cannot extract key: data of length %"PRIu32" is wrong (mpack possibly in error state)\n", mpack_node_data_len(node));
        mpack_tree_flag_error(tree, mpack_error_data);
        return k;
    }

    uint8_t *arr;
    arr = (uint8_t *) mpack_node_data(node);
    for (int i = 0; i < KEY_BYTES; i++) {
        k.v[i] = arr[i];
    }

    return k;
}

// struct Key extract_optional_key(mpack_tree_t *tree, mpack_node_t node) {
//     struct Key k = {
//         .v = {0},
//     };

//     if (mpack_node_nil(node)) {
//         return k;
//     }

//     if (mpack_node_data_len(node) != KEY_BYTES) {
//         printf("Cannot extract key: data of length %"PRIu32" is wrong (mpack possibly in error state)\n", mpack_node_data_len(node));
//         mpack_tree_flag_error(tree, mpack_error_data);
//         return k;
//     }

//     uint8_t *arr;
//     arr = (uint8_t *) mpack_node_data(node);
//     for (int i = 0; i < KEY_BYTES; i++) {
//         k.v[i] = arr[i];
//     }

//     return k;
// }


struct Hash extract_hash(mpack_tree_t *tree, mpack_node_t node) {
    struct Hash h = {
        .v = {0},
    };
    if (mpack_node_data_len(node) != HASH_BYTES) {
        printf("Cannot extract hash: data of length %"PRIu32" is wrong (mpack possibly in error state)\n", mpack_node_data_len(node));
        mpack_tree_flag_error(tree, mpack_error_data);
        return h;
    }

    uint8_t *arr;
    arr = (uint8_t *) mpack_node_data(node);
    for (int i = 0; i < HASH_BYTES; i++) {
        h.v[i] = arr[i];
    }

    return h;
}


struct PublicKey extract_public_key(mpack_tree_t *tree, mpack_node_t node) {
    struct PublicKey pk = {
        .v = {0},
    };
    if (mpack_node_data_len(node) != crypto_sign_PUBLICKEYBYTES) {
        printf("Cannot extract public key: data of length %"PRIu32" is wrong (mpack possibly in error state)\n", mpack_node_data_len(node));
        mpack_tree_flag_error(tree, mpack_error_data);
        return pk;
    }

    uint8_t *arr;
    arr = (uint8_t *) mpack_node_data(node);
    for (uint8_t i = 0; i < crypto_sign_PUBLICKEYBYTES; i++) {
        pk.v[i] = arr[i];
    }

    return pk;
}


struct Signature extract_signature(mpack_tree_t *tree, mpack_node_t node) {
    struct Signature sig = {
        .v = {0},
    };

    if (mpack_node_data_len(node) != crypto_sign_BYTES) {
        printf("Cannot extract signature: data of length %"PRIu32" is wrong (mpack possibly in error state)\n", mpack_node_data_len(node));
        mpack_tree_flag_error(tree, mpack_error_data);
        return sig;
    }

    uint8_t *arr;
    arr = (uint8_t *) mpack_node_data(node);
    for (uint8_t i = 0; i < crypto_sign_BYTES; i++) {
        sig.v[i] = arr[i];
    }

    return sig;
}


struct EntrySet extract_entry_set(mpack_tree_t *tree, mpack_node_t entries) {
    assert(tree);
    struct Key key;
    struct Entry *entry;
    mpack_node_t node;
    const char* data;
    enum ENTRY_TYPE type;

    struct EntrySet entry_set = {
        .entries = NULL,
        .n_entries = 0,
    };

    struct EntrySet existing_entries;

    if (mpack_tree_error(tree) != mpack_ok) {
        printf("extract_entry_set: error occurred parsing this message, not saving entries\n");
        return entry_set;
    }

    size_t n_entries = mpack_node_array_length(entries);
    printf("We think there are %zu entries\n", n_entries);

    for (size_t i = 0; i < n_entries; i++) {
        entry = calloc_or_exit(1, sizeof(struct Entry));
        node = mpack_node_array_at(entries, i);

        type = mpack_node_enum(mpack_node_map_cstr(node, u8"type"), ENTRY_TYPE_KEYS, ENTRY_TYPE_COUNT);

        if (type == ENTRY_VALUE) {
            entry->type = ENTRY_VALUE;
            key = extract_key(tree, mpack_node_map_cstr(node, u8"key"));
            entry->key = key;
            entry->live = mpack_node_bool(mpack_node_map_cstr(node, u8"live"));
            entry->value.type_key = extract_key(tree, mpack_node_map_cstr(node, u8"dataType"));
            entry->value.size = mpack_node_data_len(mpack_node_map_cstr(node, u8"data"));
            entry->value.v = malloc_or_exit(entry->value.size);

            data = mpack_node_data(mpack_node_map_cstr(node, u8"data"));
            if (!data || mpack_tree_error(tree) != mpack_ok) {
                puts("could not parse value data: node is not string or binary");
                return entry_set;
            }

            memcpy(entry->value.v, data, entry->value.size);
            if (!compare_keys(entry->key, value_key_hash(entry->value.v, entry->value.size))) {
                puts("entry key does not match hash");
                mpack_tree_flag_error(tree, mpack_error_data);
                return entry_set;
            }
            entry_set_add_entry(&entry_set, entry);
        } else if (type == ENTRY_ID) {
            entry->type = ENTRY_ID;


            printf("key len: %"PRIu32"\nvalue len: %"PRIu32"\npublic key len: %"PRIu32"\nsig len: %"PRIu32"\n",
                mpack_node_data_len(mpack_node_map_cstr(node, u8"key")),
                mpack_node_data_len(mpack_node_map_cstr(node, u8"value")),
                // mpack_node_data_len(mpack_node_map_cstr(node, u8"previous")),
                mpack_node_data_len(mpack_node_map_cstr(node, u8"publicKey")),
                mpack_node_data_len(mpack_node_map_cstr(node, u8"signature")));

            entry->key = extract_key(tree, mpack_node_map_cstr(node, u8"key"));
            entry->live = mpack_node_bool(mpack_node_map_cstr(node, u8"live"));
            entry->id.value = extract_key(tree, mpack_node_map_cstr(node, u8"value"));

            if (mpack_node_map_contains_str(node, u8"previous", 8)) {
                puts("PREVIOUS ******");
                entry->id.previous = extract_hash(tree, mpack_node_map_cstr(node, u8"previous"));
            } else {
                puts("NO PREVIOUS ****");
                // the previous will be an array of zeros
            }

            entry->id.public_key = extract_public_key(tree, mpack_node_map_cstr(node, u8"publicKey"));
            entry->id.signature = extract_signature(tree, mpack_node_map_cstr(node, u8"signature"));

            puts("the entry *************************");
            print_entry(entry);

            if (!identity_signature_valid(entry)) {
                puts("identity signature not valid!");
                mpack_tree_flag_error(tree, mpack_error_data);
                return entry_set;
            }

            // verify that previous identity exists iff it should and key matches
            existing_entries = store_get_entry(my_store, &key);

            if (zeros(entry->id.previous.v, sizeof entry->id.previous.v)) {
                if (existing_entries.n_entries != 0) {
                    puts("a previous identity entry already exists but is not referenced");
                    mpack_tree_flag_error(tree, mpack_error_data);
                    return entry_set;
                }
            } else {
                bool found_previous = false;
                for (uint32_t i = 0; i < existing_entries.n_entries; i++) {
                    if (existing_entries.entries[i]->type != ENTRY_ID) {
                        // must point to a previous identity, not a previous value
                        continue;
                    }

                    if (hashes_equal(entry->id.previous, identity_hash(existing_entries.entries[i]))) {
                        found_previous = true;
                        break;
                    }
                }

                if (!found_previous) {
                    puts("could not find referenced previous identity entry");
                    mpack_tree_flag_error(tree, mpack_error_data);
                    return entry_set;
                }
            }
            // this identity has passed the gauntlet: add it to the entry set that
            // will be added to the store
            entry_set_add_entry(&entry_set, entry);
        } else {
            puts("unknown entry type");
            exit(EXIT_FAILURE);
        }
    }

    return entry_set;
}


void write_node(mpack_writer_t *writer, struct Node *node) {
    char tcp_host_string[NI_MAXHOST],
        udp_host_string[NI_MAXHOST],
        tcp_port_string[NI_MAXSERV],
        udp_port_string[NI_MAXSERV];

    int rc;
    rc = getnameinfo(
        (struct sockaddr *) &node->stream_address,
        sizeof(struct sockaddr_storage),
        tcp_host_string,
        sizeof tcp_host_string,
        tcp_port_string,
        sizeof tcp_port_string,
        NI_NUMERICHOST | NI_NUMERICSERV);

    if (rc != 0) {
        printf("could not get name info from address!\n");
        exit(EXIT_FAILURE);
    }

    rc = getnameinfo(
        (struct sockaddr *) &node->datagram_address,
        sizeof(struct sockaddr_storage),
        udp_host_string,
        sizeof udp_host_string,
        udp_port_string,
        sizeof udp_port_string,
        NI_NUMERICHOST | NI_NUMERICSERV);

    if (rc != 0) {
        printf("could not get name info from address!\n");
        exit(EXIT_FAILURE);
    }

    mpack_start_map(writer, 9);

        mpack_write_utf8_cstr(writer, u8"type");
        mpack_write_utf8_cstr(writer, NODE_TYPE_KEYS2[node->type]);

        mpack_write_utf8_cstr(writer, u8"tcpHost");
        mpack_write_utf8_cstr(writer, tcp_host_string);

        mpack_write_utf8_cstr(writer, u8"tcpPort");
        mpack_write_utf8_cstr(writer, tcp_port_string);

        mpack_write_utf8_cstr(writer, u8"udpHost");
        mpack_write_utf8_cstr(writer, udp_host_string);

        mpack_write_utf8_cstr(writer, u8"udpPort");
        mpack_write_utf8_cstr(writer, udp_port_string);

        mpack_write_utf8_cstr(writer, u8"key");
        mpack_write_bin(writer, (char *) node->key.v, sizeof node->key.v);

        mpack_write_utf8_cstr(writer, u8"lastMessage");
        mpack_write_u64(writer, node->message_time);

        mpack_write_utf8_cstr(writer, u8"lastReply");
        mpack_write_u64(writer, node->reply_time);

        mpack_write_utf8_cstr(writer, u8"unansweredMessages");
        mpack_write_int(writer, node->messages);

    mpack_finish_map(writer);
}


void write_entry(mpack_writer_t *writer, struct Entry *entry, bool include_signature, bool include_live) {
    struct Key k = atomic_load(&entry->key);
    bool live = atomic_load(&entry->live);
    // bool live = atomic_load(&entry->live);

    if (entry->type == ENTRY_ID) {
        mpack_start_map(writer, 5 + (include_signature ? 1 : 0) + (include_live ? 1 : 0));

        mpack_write_utf8_cstr(writer, u8"key");
        mpack_write_bin(writer, (char *) k.v, sizeof k.v);

        if (include_live) {
            mpack_write_utf8_cstr(writer, u8"live");
            mpack_write_bool(writer, live);
        }

        mpack_write_utf8_cstr(writer, u8"type");
        mpack_write_utf8_cstr(writer, ENTRY_TYPE_KEYS[entry->type]);

        mpack_write_utf8_cstr(writer, u8"value");
        mpack_write_bin(writer, (char *) entry->id.value.v, sizeof entry->id.value.v);

        mpack_write_utf8_cstr(writer, u8"previous");
        mpack_write_bin(writer, (char *) entry->id.previous.v, sizeof entry->id.previous.v);

        mpack_write_utf8_cstr(writer, u8"publicKey");
        mpack_write_bin(writer, (char *) entry->id.public_key.v, sizeof entry->id.public_key.v);

        if (include_signature) {
            mpack_write_utf8_cstr(writer, u8"signature");
            mpack_write_bin(writer, (char *) entry->id.signature.v, sizeof entry->id.signature.v);
        }
    } else if (entry->type == ENTRY_VALUE) {
        mpack_start_map(writer, 4 + (include_live ? 1 : 0));

        mpack_write_utf8_cstr(writer, u8"key");
        mpack_write_bin(writer, (char *) k.v, sizeof k.v);

        if (include_live) {
            mpack_write_utf8_cstr(writer, u8"live");
            mpack_write_bool(writer, live);
        }

        mpack_write_utf8_cstr(writer, u8"type");
        mpack_write_utf8_cstr(writer, ENTRY_TYPE_KEYS[entry->type]);
        mpack_write_utf8_cstr(writer, u8"data");
        mpack_write_bin(writer, (char *) entry->value.v, entry->value.size);

        mpack_write_utf8_cstr(writer, u8"dataType");
        mpack_write_bin(writer, (char *) entry->value.type_key.v, sizeof entry->value.type_key.v);
    } else {
        printf("bad entry type");
        exit(EXIT_FAILURE);
    }

    mpack_finish_map(writer);
}


void write_entry_set(mpack_writer_t *writer, struct EntrySet *entry_set) {
    assert(writer);
    assert(entry_set);

    mpack_start_array(writer, entry_set->n_entries);

    for (uint32_t i = 0; i < entry_set->n_entries; i++) {
        write_entry(writer, entry_set->entries[i], true, true);
    }

    mpack_finish_array(writer);
}


void write_script(mpack_writer_t *writer, struct Script *script, bool include_signatures) {
    mpack_start_map(writer, 2);

        mpack_write_utf8_cstr(writer, u8"type");
        mpack_write_utf8_cstr(writer, u8"asset");

        mpack_write_utf8_cstr(writer, u8"data");

        switch (script->type) {
            case FEE:
            case WORK_REWARD:
            case NEVER:
            case ASSET_CREATION:
            mpack_write_nil(writer);
            break;
            // nothing else to serialize

            // pretend this is correct for now
            case HTL:
            case VALUE:
            mpack_write_nil(writer);
            break;

            case PUBLIC_KEY:
            mpack_start_map(writer, 1);
                mpack_write_utf8_cstr(writer, u8"publicKey");
                mpack_write_bin(writer, (char *) script->public_key.v, sizeof script->public_key.v);
            mpack_finish_map(writer);
            break;

            case SIGNATURE:
            if (include_signatures) {
                mpack_start_map(writer, 1);
                    mpack_write_utf8_cstr(writer, u8"signature");
                    mpack_write_bin(writer, (char *) script->signature.v, sizeof script->signature.v);
                mpack_finish_map(writer);
            } else {
                mpack_write_nil(writer);
            }
            break;

            case DATA_HASH_CHALLENGE:
            case DATA_HASH_PROOF:
            case STORAGE_REWARD:
            mpack_start_map(writer, 2);
                mpack_write_utf8_cstr(writer, u8"proofSeed");
                mpack_write_bin(writer, (char *) script->storage_proof.seed.v, sizeof script->storage_proof.seed.v);
                mpack_write_utf8_cstr(writer, u8"proofHash");
                mpack_write_bin(writer, (char *) script->storage_proof.hash.v, sizeof script->storage_proof.hash.v);
            mpack_finish_map(writer);
            break;

            default:
            puts("serialize_script: unknown script type");
            exit(EXIT_FAILURE);
        }


    mpack_finish_map(writer);
}


void write_input(mpack_writer_t *writer, struct Input *input, bool include_signatures) {
    mpack_start_map(writer, 3);

        mpack_write_utf8_cstr(writer, u8"asset");
        mpack_write_bin(writer, (char *) input->asset.v, sizeof input->asset.v);

        mpack_write_utf8_cstr(writer, u8"amount");
        mpack_write_i64(writer, input->amount);

        mpack_write_utf8_cstr(writer, u8"script");
        write_script(writer, &input->script, include_signatures);

    mpack_finish_map(writer);
}


void write_output(mpack_writer_t *writer, struct Output *output, bool include_signatures) {
    mpack_start_map(writer, 3);

        mpack_write_utf8_cstr(writer, u8"asset");
        mpack_write_bin(writer, (char *) output->asset.v, sizeof output->asset.v);

        mpack_write_utf8_cstr(writer, u8"amount");
        mpack_write_i64(writer, output->amount);

        mpack_write_utf8_cstr(writer, u8"script");
        write_script(writer, &output->script, include_signatures);

    mpack_finish_map(writer);
}


void write_tx(mpack_writer_t *writer, struct Tx *tx, bool include_signatures) {
    mpack_start_map(writer, 2);

        mpack_write_utf8_cstr(writer, u8"inputs");
        mpack_start_array(writer, tx->n_inputs);
        for (int i = 0; i < tx->n_inputs; i++) {
            write_input(writer, tx->inputs[i], include_signatures);
        }
        mpack_finish_array(writer);

        mpack_write_utf8_cstr(writer, u8"outputs");
        mpack_start_array(writer, tx->n_outputs);
        for (int i = 0; i < tx->n_outputs; i++) {
            write_output(writer, tx->outputs[i], include_signatures);
        }

        mpack_finish_array(writer);

    mpack_finish_map(writer);
}


void write_block(mpack_writer_t *writer, struct Block *block, bool include_hash) {
    mpack_start_map(writer, 7 + (include_hash ? 1 : 0));

    if (include_hash) {
        mpack_write_utf8_cstr(writer, u8"hash");
        mpack_write_bin(writer, (char *) block->hash.v, sizeof block->hash.v);
    }

    mpack_write_utf8_cstr(writer, u8"previousHash");
    mpack_write_bin(writer, (char *) block->previous_hash.v, sizeof block->previous_hash.v);

    mpack_write_utf8_cstr(writer, u8"height");
    mpack_write_u32(writer, block->height);

    mpack_write_utf8_cstr(writer, u8"time");
    mpack_write_u64(writer, (uint64_t) block->time);

    mpack_write_utf8_cstr(writer, u8"merkleRoot");
    mpack_write_bin(writer, (char *) block->merkle_root.v, sizeof block->merkle_root.v);

    mpack_write_utf8_cstr(writer, u8"storageProof");
    mpack_write_bin(writer, (char *) block->storage_proof.v, sizeof block->storage_proof.v);

    mpack_write_utf8_cstr(writer, u8"nonce");
    mpack_write_u32(writer, block->nonce);

    mpack_write_utf8_cstr(writer, u8"txs");
    mpack_start_array(writer, block->n_txs);
    for (int i = 0; i < block->n_txs; i++) {
        write_tx(writer, block->txs[i], true);
    }
    mpack_finish_array(writer);

    mpack_finish_map(writer);
}
