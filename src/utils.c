// Copyright 2017 Chris Walker

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#include "utils.h"


bool zeros(uint8_t *v, size_t size) {
    // check if the value is all zeros
    for (size_t i = 0; i < size; i++) {
        if (v[i] != 0) {
            return false;
        }
    }
    return true;
}


bool ones(uint8_t *v, size_t size) {
    // check if the value is all zeros
    for (size_t i = 0; i < size; i++) {
        if (v[i] != 1) {
            return false;
        }
    }
    return true;
}


void panic(char error_message[]) {
    printf("ERROR: %s! Stopping!\n", error_message);
    exit(1);
}


void vector_add(void ***old, uint32_t *n_old, void *new, uint32_t n_new) {
    // Add new pointers to old 'array' of pointers or create the array if it doesn't exist
    assert(old);
    assert(n_old);
    assert(new);
    assert(&new);
    assert(n_new > 0);

    // printf(
    //     "vector add: %zu old elements, adding %zu new pointers starting at address %p\n",
    //     *n_old,
    //     n_new,
    //     new);

    if (*n_old == 0) {
        *old = malloc_or_exit(sizeof(void *) * (n_new));
    } else {
        assert(*old);
        // printf("old element array starts at %p\n", (void *) *old);
        // print_buffer(**old, sizeof(void *) * *n_old);

        for (uint32_t i = 0; i < *n_old; i++) {
            // the existing pointers must not be null
            // printf("old element %zu at address %p\n", i, (*old)[i]);
            assert((*old)[i]);
        }
        *old = realloc_or_exit(*old, sizeof(void *) * (*n_old + n_new));
        // printf("new element array starts at %p\n", (void *) *old);
    }
    memcpy(*old + *n_old, &new, sizeof(void *) * n_new);
    *n_old += n_new;
}


void print_time(time_t t) {
    if (t == 0) {
        printf(" never (0)         ");
        return;
    }
    char buffer[26];
    struct tm *time_info;

    time_info = localtime(&t);

    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", time_info);
    printf("%s", buffer);
}


bool all_zeros(uint8_t *v, uint32_t size) {
    // check if the value is all zeros
    for (uint32_t i = 0; i < size; i++) {
        if (v[i] != 0) {
            return false;
        }
    }
    return true;
}


uint32_t to_buffer(uint8_t **buffer, void *obj, uint32_t obj_size) {
    /*
    Add content to end of buffer.
    Copy the thing pointed to by *obj into the buffer
    Unsafe: must verify that buffer has room for content beforehand!!!
    */
    assert(buffer);
    assert(*buffer);
    assert(obj);
    assert(obj_size > 0);
    memcpy(*buffer, obj, obj_size);
    *buffer += obj_size;
    return obj_size;
}


uint32_t from_buffer(void *obj, uint8_t **buffer, uint32_t obj_size) {
    assert(obj && buffer && obj_size > 0);
    memcpy(obj, *buffer, obj_size);
    *buffer += obj_size;
    return obj_size;
}


void *calloc_or_exit(int a, uint32_t b) {
    void *c = calloc(a, b);
    if (!c) {
        puts("could not calloc memory, exiting!");
        exit(EXIT_FAILURE);
    }
    return c;
}

void *malloc_or_exit(uint32_t a) {
    void *b = malloc(a);
    if (!b) {
        puts("could not malloc memory, exiting!");
        exit(EXIT_FAILURE);
    }
    return b;
}

void *realloc_or_exit(void *a, uint32_t b) {
    void *c = realloc(a, b);
    if (!c) {
        puts("could not reaalloc memory, exiting!");
        exit(EXIT_FAILURE);
    }
    return c;
}


void print_buffer(uint8_t *buffer, uint32_t n_bytes) {
    printf("0x");
    for (size_t i = 0; i < n_bytes; i++) {
        printf("%02x", buffer[i]);
    }
}

bool bin_to_pghex(char *buffer, uint8_t *data, size_t size) {
    char *p = buffer;

    p += sprintf(p, "E'\\\\x");

    for (size_t i = 0; i < size; i++) {
        p += sprintf(p, "%02x", data[i]);
    }

    p += sprintf(p, "'");

    assert(p - buffer < 1000);
    return true;
}
