// Copyright 2017 Chris Walker

#ifndef ROUTE_H_
#define ROUTE_H_

#include <sys/socket.h>

#include "config.h"
#include "types.h"
#include "utils.h"
#include "crypto.h"
#include "store.h"

/*
Routing based on paper
Kademlia: A Peer-to-peer information system based on the XOR metric
Petar Maymounkov, David Mazières
See https://github.com/jech/dht for implementation / inspiration
See also http://www.bittorrent.org/beps/bep_0005.html
*/


struct Bucket {
    /*
    Leaves of the binary tree routing table.
    Each bucket contains up to κ nodes all sharing a common key prefix,
    As this prefix is also the bucket's position in the routing tree,
    each bucket covers some range of the key space, and together the
    buckets span the entire key space with no overlap.
    Sections 2.2-2.4

    The bucket stores up to one cached address in case one of the nodes fails.
    */

    // This is an key's bucket iff:
    // bucket.first_key <= key.v <=next_bucket->first_key
    struct Key first_key;
    // number of nodes currently in the bucket
    int count;
    // time of most recent correct reply
    time_t reply_time;
    // first node in the bucket, sorted by increasing reply time
    struct Node *nodes;
    // // a cached address in case a node fails
    // struct sockaddr_storage cached;
    // pointer to the next Bucket
    struct Bucket *next;
};


struct SearchNode {
    // pointer to node in routing table: not always present, as the search may return
    // nodes in a bucket that is already full!
    struct Node *node;
    // Key of the node
    struct Key key;
    // distance between the node and the searched for Key
    struct Key distance;
    // the IPv4 or IPv6 address of this node for TCP messages
    struct sockaddr_storage stream_address;
    // the IPv4 or IPv6 address of this node for UDP messages
    struct sockaddr_storage datagram_address;
    // pointer to next search node in linked list, sorted
    // by increasing distance from the searched for Key
    struct SearchNode *next;
    // number of unanswered requests for this search
    int tries;
    // time of most recent outgoing message
    time_t message_time;
    bool replied;
    bool done;
};


struct Search {
    // the key of the search itself
    struct Key search_key;
    // the key we are searching for
    struct Key target_key;
    // time of the last search step
    time_t step_time;
    bool done;
    // pointer to linked list of nodes
    struct SearchNode *search_nodes;
    // TODO(cw): when / why use identical_prefix_bits?
    // smallest return value of identical_prefix_bits() for any bucket in search
    // int miidentical_prefix_bits;
};

// the key of this node;
struct Key *my_node_key;


/*
number of nodes queried in parallel during a node lookup
*/
static const int α = 4;

/*
number of adjacent bits in a node key used to place the
node key into a bucket (accelerated lookup factor of
section 4.2)
*/
// static const int b = 1;

/*
maximum number of buckets containing nodes.
For node m, node n belongs to bucket j iff
2^j <= distance(node_m, node_n) < 2^(j+1)
(this means that bucket for j=0 contains the closest nodes)
(it also means node m never puts itself in any buckets FALSE?)
*/
// static const int β = KEY_BYTES * 8 / b;

/*
replication factor
for a value given value key, the κ closest nodes
must store the value.

Also: maximum number of nodes in each bucket
*/
#define κ 4

int identical_prefix_bits(struct Key, struct Key);

void route_init(char *key_string);
struct Node *node_init(
    struct Key key,
    enum NodeType type,
    struct sockaddr_storage datagram_address,
    struct sockaddr_storage stream_address);

// update ongoing searches -- call periodically
void searches_update();

// add a node to the routing table
struct Bucket *node_add(struct Node *);
struct Node *node_find(struct Key key);
// update a single node when sending a message to it
struct Node *node_update(struct Node *node, enum Direction direction);

void route_end();

void search_start(struct Key target_key);
void on_key_search(struct Node *remote_node, struct Key *search_key, struct Key *target_key);
void on_node_search_response(struct Key *remote_key, struct Key *search_key, struct Node *target_node);
void on_value_search_response(struct Key *remote_key, struct Key *search_key, struct EntrySet *target_values);
// void on_value_request(struct Node *remote_node, struct Key *search_key, struct Key *target_key);
// void on_entry_set(struct Node *remote_node, struct EntrySet *entry_set);
// void on_entry_store(struct Node *remote_node, struct EntrySet *entry_set);

struct NodeSet closest_nodes(struct Key *, uint32_t max_n_nodes);
void free_node_set(struct NodeSet *node_set);

void print_node(struct Node *);
void print_node_set(struct NodeSet *);
void print_bucket(struct Bucket *);
void print_search(struct Search *);
void print_searches_summary();
void print_routes();
void print_route_tree();

#endif  // ROUTE_H_
