// Copyright 2017 Chris Walker
#include <string.h>
#include <stdatomic.h>
#include <inttypes.h>

#include "types.h"
#include "utils.h"
#include "message.h"
#include "blockchain.h"
#include "network.h"
#include "serialize.h"
#include "crypto.h"
#include "store.h"
#include "route.h"
#include "mpack.h"

// the key of this node;
extern struct Key *my_node_key;

// the key-value store of this node
extern struct Store *my_store;

const char *MESSAGE_TYPE_KEYS[] = {
    "unknown",
    "ping",
    "ack",
    "search",
    "nodeSearchResponse",
    "valueSearchResponse",
    "entryStore",
    "transaction",
    "blockRequest",
    "block",
    "blockchainRequest",
};

const char *NODE_TYPE_KEYS[] = {
    u8"unkown",
    u8"sst",
    u8"client",
};

const char *MESSAGE_NAMES[] = {
    "Unknown",
    "Ping",
    "Acknowledgement",
    "Search for key",
    "Node search response",
    "Value search response",
    "Value store",
    "Transaction",
    "Block request",
    "Block",
    "Blockchain request",
};


struct Message *message_init_outgoing(
    mpack_writer_t *writer,
    struct Node *to,
    enum MessageType type,
    struct Key *message_key,
    int n_content_nodes);

static void message_finish_outgoing(mpack_writer_t *writer, struct Node *to, struct Message *);

static struct Message *message_init_incoming(
    mpack_tree_t *tree,
    struct sockaddr_storage,
    enum MessageProtocol,
    uint8_t *,
    size_t);

static void message_finish_incoming(
    mpack_tree_t *tree,
    struct Message *message);

static void print_message_type(enum MessageType);
static void print_message(struct Message *);
static void message_set_protocol(struct Message *message, struct Node *node);
static void message_send(struct Node *, struct Message *);
static bool node_type_handles_message(enum NodeType, enum MessageType);


bool node_type_handles_message(enum NodeType node_type, enum MessageType message_type) {
    // if we send a message of this type to a node of this type, can the node handle it?
    switch (message_type) {
        case PING:
        case ACK:
        case NODE_SEARCH_RESPONSE:
        case VALUE_SEARCH_RESPONSE:
        case TRANSACTION:
        case BLOCK:
        return true;

        case KEY_SEARCH:
        case ENTRY_STORE:
        case BLOCK_REQUEST:
        case BLOCKCHAIN_REQUEST:
        return node_type == SST_NODE;

        default:
        printf(
            "unhandled message type in node_type_handles_message: node type %s, message type: ",
            node_type == SST_NODE ? "node" : "client");
        print_message_type(message_type);
        exit(EXIT_FAILURE);
    }
}


struct Message *message_init_outgoing(
    mpack_writer_t *writer,
    struct Node *node,
    enum MessageType type,
    struct Key *message_key,
    int n_content_nodes) {
    // create a new message with the specified address, type, and contents,
    // serialize it, and save the message and serialized form to the heap.
    assert(node);

    struct Message *message = calloc_or_exit(1, sizeof(struct Message));

    // every message has a key which is generated if not provided
    message->key = message_key ? *message_key : random_key();
    message->node_type = SST_NODE;
    message->direction = OUTBOUND;
    message->type = type;
    message->remote_key = *my_node_key;
    message->valid = true;
    message_set_protocol(message, node);

    mpack_writer_init_growable(
        writer,
        (char **) &message->data,
        (size_t *) &message->data_size);

    mpack_start_map(writer, 4 + n_content_nodes);

        // SST protocol version
        mpack_write_utf8_cstr(writer, u8"sstVersion");
        mpack_write_u16(writer, SST_VERSION);

        // message type
        mpack_write_utf8_cstr(writer, u8"type");
        mpack_write_utf8_cstr(writer, MESSAGE_TYPE_KEYS[message->type]);

        // message key
        mpack_write_utf8_cstr(writer, u8"key");
        mpack_write_bin(writer, (char *) message->key.v, sizeof message->key.v);

        // node
        mpack_write_utf8_cstr(writer, u8"node");
        mpack_start_map(writer, 2);
            // this node type
            mpack_write_utf8_cstr(writer, u8"type");
            mpack_write_utf8_cstr(writer, NODE_TYPE_KEYS[SST_NODE]);
            // this node key
            mpack_write_utf8_cstr(writer, u8"key");
            mpack_write_bin(writer, (char *) my_node_key->v, sizeof my_node_key->v);
        mpack_finish_map(writer);

        // send_x messages may add more content

    // closed by message_finish

    return message;
}


void message_finish_outgoing(mpack_writer_t *writer, struct Node *to, struct Message *message) {
    mpack_finish_map(writer);

    if (mpack_writer_destroy(writer) != mpack_ok) {
        printf("message_finish_outgoing: an error occurred writing the message data!\n");
        exit(EXIT_FAILURE);
    }

    message_send(to, message);

    free(message->data);
    free(message);
}


struct Message *message_init_incoming(
    mpack_tree_t *tree,
    struct sockaddr_storage remote_address,
    enum MessageProtocol protocol,
    uint8_t *buffer,
    size_t n_bytes) {
    // parse the message and verify matches SST schema
    mpack_node_t root, node;
    struct Message *message = malloc_or_exit(sizeof(struct Message));
    bool valid = true;

    mpack_tree_init(tree, (char *) buffer, n_bytes);
    root = mpack_tree_root(tree);

    node = mpack_node_map_cstr(root, u8"node");

    *message = (struct Message) {
        .key = extract_key(tree, mpack_node_map_cstr(root, u8"key")),
        .remote_key = extract_key(tree, mpack_node_map_cstr(node, u8"key")),
        .type = (enum MessageType) mpack_node_enum(
            mpack_node_map_cstr(root, u8"type"),
            MESSAGE_TYPE_KEYS,
            MESSAGE_TYPE_COUNT),
        .node_type = (enum NodeType) mpack_node_enum(
            mpack_node_map_cstr(node, u8"type"),
            NODE_TYPE_KEYS,
            NODE_TYPE_COUNT),
        .valid = valid,
        .direction = INBOUND,
        .remote_address = remote_address,
        .protocol = protocol,
        .sst_version = mpack_node_u16(mpack_node_map_cstr(root, u8"sstVersion")),
        .data_size = n_bytes,
        .data = malloc_or_exit(message->data_size),
    };

    memcpy(message->data, buffer, n_bytes);

    mpack_error_t err = mpack_tree_error(tree);

    if (err != mpack_ok) {
        printf("message_init_incoming: an error occurred parsing this message!\n");
        message->valid = false;
    }

    if (message->sst_version != SST_VERSION) {
        printf("incompatible SST protocol version\n");
        message->valid = false;
    }

    print_message(message);

    return message;
}


void message_finish_incoming(mpack_tree_t *tree, struct Message *message) {
    mpack_error_t err = mpack_tree_destroy(tree);
    if (err != mpack_ok) {
        printf("message_finish_incoming: an error occurred decoding the data!\n");
        exit(EXIT_FAILURE);
    }

    free(message->data);
    free(message);
}


void send_ping(struct Node *node) {
    assert(node);

    mpack_writer_t writer;
    struct Message *message = message_init_outgoing(&writer, node, PING, NULL, 0);
    message_finish_outgoing(&writer, node, message);
}


void send_ack(struct Node *node, struct Key *message_key, struct Key *entry_key) {
    assert(node);
    assert(message_key);

    mpack_writer_t writer;
    struct Message *message;

    if (entry_key) {
        message = message_init_outgoing(&writer, node, ACK, message_key, 1);

        mpack_write_utf8_cstr(&writer, u8"entryKey");
        mpack_write_bin(&writer, (char *) entry_key->v, sizeof my_node_key->v);

    } else {
        message = message_init_outgoing(&writer, node, ACK, message_key, 0);
    }
    message_finish_outgoing(&writer, node, message);
}


void send_key_search(struct Node *node, struct Key *search_key, struct Key *target_key) {
    // ask the to node to return the closest known nodes for this key
    assert(node);
    assert(search_key);
    assert(target_key);

    mpack_writer_t writer;
    struct Message *message = message_init_outgoing(&writer, node, PING, NULL, 2);

    mpack_write_utf8_cstr(&writer, u8"searchKey");
    mpack_write_bin(&writer, (char *) search_key->v, sizeof search_key->v);

    mpack_write_utf8_cstr(&writer, u8"targetKey");
    mpack_write_bin(&writer, (char *) target_key->v, sizeof target_key->v);

    message_finish_outgoing(&writer, node, message);
}


void send_node_search_response(struct Node *node, struct Key *search_key, struct Node *target_node) {
    // // return closest known nodes for this search key
    // // include the search key so that the recipient can connect the node_key to its existing
    // // search for the search_key
    assert(node);
    assert(search_key);
    assert(target_node);

    mpack_writer_t writer;
    struct Message *message = message_init_outgoing(&writer, node, NODE_SEARCH_RESPONSE, search_key, 1);

    mpack_write_utf8_cstr(&writer, u8"targetNode");
    write_node(&writer, node);

    message_finish_outgoing(&writer, node, message);
}


void send_value_search_response(struct Node *node, struct Key *search_key, struct Key *target_key, struct EntrySet *entry_set) {
    assert(node);
    assert(search_key);
    assert(target_key);
    assert(entry_set);

    print_node(node);
    print_key(search_key);
    print_entry_set(entry_set);

    printf("search key: ");
    print_key(search_key);

    mpack_writer_t writer;
    struct Message *message = message_init_outgoing(&writer, node, VALUE_SEARCH_RESPONSE, search_key, 1);

    mpack_write_utf8_cstr(&writer, u8"entries");
    write_entry_set(&writer, entry_set);

    message_finish_outgoing(&writer, node, message);
}


void send_entry_store(struct Node *node, struct EntrySet *entry_set, struct Tx *tx) {
    assert(node);
    assert(entry_set);
    assert(tx);

    /*
    ask this node to store this value set, optionally including a payment tx for storage

    Order:
    - bool (tx included?)
    - value set
    - tx
    */

    // assert(node);
    // assert(entry_set);

    // bool include_io = true, zero_signatures = false, includes_tx = (bool) tx;
    // size_t content_size = sizeof includes_tx
    //     + wire_size_entry_set(entry_set)
    //     + (includes_tx ? wire_size_tx(tx, include_io): 0);

    // uint8_t *p, content[content_size];
    // p = content;

    // printf("content size: %zu\n", content_size);

    // puts("value set before serialization:");
    // print_entry_set(entry_set);

    // to_buffer(&p, &includes_tx, sizeof includes_tx);
    // serialize_entry_set(&p, entry_set);
    // if (includes_tx) {
    //     assert(tx_valid(tx));
    //     puts("also serializing tx:");
    //     print_tx(tx);
    //     serialize_tx(&p, tx, include_io, zero_signatures);
    // }

    // struct Message *message = message_init(node, ENTRY_STORE, NULL, content, content_size);
    // message_send(node, message);

    // message_finish(message);
}


void send_block_request(struct Node *node, struct Hash *hash) {
    assert(node);
    print_hash(hash);

    // size_t content_size = wire_size_hash();
    // uint8_t *p, content[content_size];
    // p = content;

    // serialize_hash(&p, hash);

    // struct Message *message = message_init(node, BLOCK_REQUEST, NULL, content, content_size);
    // message_send(node, message);
    // message_finish(message);
}


void send_block(struct Node *node, struct Block *block) {
    assert(node);
    assert(block);

    // bool include_txs = true, zero_signatures = false, zero_block_hash = false;
    // size_t content_size = wire_size_block(block, include_txs);
    // uint8_t *p, content[content_size];
    // p = content;

    // serialize_block(&p, block, include_txs, zero_signatures, zero_block_hash);

    // struct Message *message = message_init(node, BLOCK, NULL, content, content_size);
    // message_send(node, message);
    // message_finish(message);
    mpack_writer_t writer;
    struct Message *message = message_init_outgoing(&writer, node, BLOCK, NULL, 1);

    mpack_write_utf8_cstr(&writer, u8"block");
    write_block(&writer, block, true);

    message_finish_outgoing(&writer, node, message);
}


void send_blockchain_request(struct Node *node) {
    assert(node);

    // struct Message *message = message_init(node, BLOCKCHAIN_REQUEST, NULL, NULL, 0);
    // message_send(node, message);
    // message_finish(message);
}


void send_blockchain(struct Node *node, struct Block *block) {
    assert(node);
    assert(block);

    // // create array of blocks then send in chronological order, so that
    // // the receiving node only receives a continuous blockchain and doesn't
    // // need to send out additional requests for missing blocks
    struct Block **blocks = malloc_or_exit(sizeof (void *) * n_all_blocks);
    printf("n all blocks: %"PRIu32"\n", n_all_blocks);
    uint32_t i = 0;

    while (block) {
        blocks[i] = block;
        block = block->previous;
        i++;
    }
    while (i > 0) {
        i--;
        send_block(node, blocks[i]);
    }

    free(blocks);
}


void on_message(
    struct sockaddr_storage remote_address,
    enum MessageProtocol message_protocol,
    uint8_t *raw_message,
    uint32_t raw_message_size) {
    // handle message depending on message type
    struct Node *node = NULL;
    struct sockaddr_storage datagram_address, stream_address;
    mpack_tree_t tree;
    mpack_node_t root;

    struct Message *message = message_init_incoming(
        &tree,
        remote_address,
        message_protocol,
        raw_message,
        raw_message_size);

    root = mpack_tree_root(&tree);

    print_message(message);

    if (!message->valid) {
        puts("message invalid: doing nothing!");
        message_finish_incoming(&tree, message);
        return;
    }

    if (message->node_type == SST_NODE) {
        // update our routing tree if message from another node
        node = node_find(message->remote_key);
    }

    if (!node) {
        // we need to guess at the missing address
        if (message->protocol == DATAGRAM) {
            datagram_address = message->remote_address;
        } else {
            stream_address = message->remote_address;
        }
        default_addresses(message->protocol, &datagram_address, &stream_address);
        // this node is not in the routing table
        node = node_init(message->remote_key,
            message->node_type,
            datagram_address,
            stream_address);
    }

    if (node->bucket) {
        // this node is in the routing table
        node = node_update(node, INBOUND);
    }

    switch (message->type) {
        case UNKNOWN:
        puts("On unknown message");
        break;


        case PING:
        puts("On ping message");
        printf("got a ping\n");
        send_ack(node, &message->key, NULL);
        break;


        case ACK:
        puts("On ack message");
        break;


        case KEY_SEARCH: {
            // If search is for value and store contains target key, return values
            // Always return closet known nodes to target key.
            puts("On key search message");
            send_ack(node, &message->key, NULL);

            // struct Key search_key;
            struct Key target_key;

            // search_key = extract_key(&tree, mpack_node_map_cstr(root, u8"searchKey"));
            target_key = extract_key(&tree, mpack_node_map_cstr(root, u8"targetKey"));

            mpack_error_t err = mpack_tree_error(&tree);
            if (err != mpack_ok) {
                printf("key_search: an error occurred parsing this message!\n");
                message->valid = false;
                break;
            }

            // try to send back the closest key to this request
            on_key_search(node, &message->key, &target_key);
            break;
        }


        case NODE_SEARCH_RESPONSE: {
            // the message should have space for:
            // - search_index
            // - node
            puts("On node response message");
            // struct Key search_key;
            // struct Node target_node;
            // if (message->content_size < wire_size_key() + wire_size_node()) {
            //     puts("invalid message: not enough room in message for key");
            //     message->valid = false;
            //     break;
            // }
            // deserialize_key(&search_key, &content);
            // deserialize_node(&target_node, &content);
            // // handle the target node: maybe add it to the routing table
            // on_node_search_response(&message->remote_key, &search_key, &target_node);
            break;
        }


        case VALUE_SEARCH_RESPONSE: {
            puts("On value search response message");
            // struct Key search_key;
            // struct EntrySet entry_set = {
            //     .entries = NULL,
            //     .n_entries = 0,
            // };
            // deserialize_key(&search_key, &content);
            // deserialize_entry_set(&entry_set, &content);

            // on_value_search_response(&message->remote_key, &search_key, &entry_set);
            // free_entry_set(&entry_set);

            break;
        }


        case ENTRY_STORE: {
            puts("On entry store message");

            mpack_node_t entries_node = mpack_node_map_cstr(root, u8"entries");
            struct EntrySet entry_set = extract_entry_set(&tree, entries_node);

            // TODO(cw): pay for storage of data using contracts

            mpack_error_t err = mpack_tree_error(&tree);
            if (err == mpack_ok) {
                for (size_t i = 0; i < entry_set.n_entries; i++) {
                    printf("adding entry %zu to the store\n", i);
                    store_add_entry(my_store, entry_set.entries[i]);
                    // The ack response includes the key of this entry so
                    // clients may verify entry was received and stored correctly
                    // TODO(cw): better response format, using acks is stupid
                    struct Key entry_key = atomic_load(&entry_set.entries[i]->key);

                    printf("saved this new entry at key ");
                    print_key(&entry_key);
                    printf("");
                    send_ack(node, &message->key, &entry_key);
                }
            } else {
                printf("entry store: an error occurred parsing this message!\n");
                message->valid = false;
            }

            free_entry_set(&entry_set);

            break;
        }


        case TRANSACTION: {
            puts("On transaction message");
            // struct Tx *tx = tx_init();
            // deserialize_tx(tx, &content);
            // on_receive_tx(node, tx);
            break;
        }


        case BLOCK_REQUEST: {
            puts("On block request");
            // if (message->content_size < wire_size_hash()) {
            //     puts("not enough room in message for hash");
            //     message->valid = false;
            //     break;
            // }

            // struct Hash hash;
            // deserialize_hash(&hash, &content);
            // struct Block *requested_block = block_find(&hash);
            // if (requested_block) {
            //     send_block(node, requested_block);
            // }
            break;
        }

        case BLOCK: {
            // uint8_t *p;
            puts("On block message");
            // check if we can fit the block struct itself
            // TODO(cw) better safe length checking
            // if (message->content_size < wire_size_block(NULL, false)) {
            //     puts("not enough room in message for block structure");
            //     message->valid = false;
            //     break;
            // }

            // struct Block *block = calloc_or_exit(1, sizeof(struct Block));
            // deserialize_block(block, &content);
            // on_receive_block(node, block);
            break;
        }


        case BLOCKCHAIN_REQUEST:
        puts("On blockchain request message");
        struct Block *highest_block = block_find_highest_valid();
        if (!highest_block) {
            puts("Could not send blockchain! We don't have any blocks!");
            break;
        }
        assert(blockchain_valid(highest_block));
        send_blockchain(node, highest_block);
        break;


        default:
        printf("On unknown message type %i: exiting!", message->type);
        exit(EXIT_FAILURE);
        break;
    }

    // TODO(cw): why here? better to do on regular timer?
    searches_update();

    if (!node->bucket) {
        // this node is not in the routing table
        free(node);
    }

    message_finish_incoming(&tree, message);
}


void message_send(struct Node *to, struct Message *message) {
    assert(to);
    assert(message);
    assert(message->data_size > 0);
    assert(node_type_handles_message(to->type, message->type));

    printf("sending %s message to ", MESSAGE_NAMES[message->type]);
    print_sockaddr_storage(&message->remote_address);
    puts("");
    print_message(message);

    // update this node (which may or may not be in the routing tree)
    node_update(to, OUTBOUND);

    if (compare_keys(to->key, *my_node_key) == 0) {
        puts("Solipsism is overrated. Not going to send message to self.");
        return;
    }

    puts("sending this message:");
    for (uint32_t i = 0; i < message->data_size; i++) {
        printf("%02x ", message->data[i]);
    }
    printf("\n");

    if (message->protocol == DATAGRAM) {
        send_datagram(&to->datagram_address, message->data, message->data_size);
    } else if (message->protocol == STREAM) {
        send_stream(&to->stream_address, &to->stream_socket, message->data, message->data_size);
    } else {
        puts("not sure how to send this message");
        exit(EXIT_FAILURE);
    }

    // TODO(cw): where is best place to start searches?
    searches_update();
}


void message_set_protocol(struct Message *message, struct Node *node) {
    // we use different protocols depending on message size, urgency, tolerance to failure
    switch (message->type) {
        case PING:
        case ACK:
        case KEY_SEARCH:
        case BLOCK_REQUEST:
        case BLOCKCHAIN_REQUEST:
        message->protocol = DATAGRAM;
        message->remote_address = node->datagram_address;
        break;

        case NODE_SEARCH_RESPONSE:
        case VALUE_SEARCH_RESPONSE:
        case ENTRY_STORE:
        case TRANSACTION:
        case BLOCK:
        message->protocol = STREAM;
        message->remote_address = node->stream_address;
        break;

        default:
        printf("not sure which protocol to use for this message type: %i\n", message->type);
        exit(EXIT_FAILURE);
    }
}


void print_message_type(enum MessageType message_type) {
    printf("%i ", message_type);
    printf("(%s)", message_type < MESSAGE_TYPE_COUNT ? MESSAGE_NAMES[message_type] : "invalid message type");
}


void print_message(struct Message *message) {
    printf(
        "\nMessage:\n"
        "%s\n"
        "- direction: %s\n"
        "- remote type: %s\n"
        "- remote key: ",
        message->valid ? "(valid)" : "(invalid!)",
        message->direction == INBOUND ? "inbound" : "outbound",
        message->node_type == SST_NODE ? "sst node" : "client");
    print_key(&message->remote_key);
    printf("\n- remote ");
    print_sockaddr_storage(&message->remote_address);
    printf("\n- protocol: %s\n- type: ", message->protocol == STREAM ? "tcp/ip stream" : "datagram");
    print_message_type(message->type);
    printf("\n- message key: ");
    print_key(&message->key);
    printf("\n- raw size: %"PRIu32" bytes\n", message->data_size);
}
