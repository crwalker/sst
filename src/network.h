// Copyright 2017 Chris Walker

#ifndef NETWORK_H_
#define NETWORK_H_

#include <stdbool.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ev.h>

#include "utils.h"
#include "message.h"

void network_init(struct ev_loop *loop, char *datagram_port, char *stream_port);

void print_sockaddr_storage(struct sockaddr_storage *);
struct sockaddr_storage get_remote_address(char *host, char *service, enum MessageProtocol protocol);

void default_addresses(
    enum MessageProtocol protocol,
    struct sockaddr_storage *default_datagram_address,
    struct sockaddr_storage *default_stream_address);

void send_stream(struct sockaddr_storage *, int *stream_socket, uint8_t *message, uint32_t message_size);
void send_datagram(struct sockaddr_storage *, uint8_t *message, uint32_t message_size);
void network_end();

#endif  // NETWORK_H_
