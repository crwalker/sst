// Copyright 2017 Chris Walker

#ifndef STORE_H_
#define STORE_H_

#include <stddef.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <dirent.h>

#include "config.h"
#include "types.h"
#include "utils.h"
#include "crypto.h"

/*
The store holds values in a lock-free hash table that uses open addressing and linear probing.
See: http://preshing.com/20130605/the-worlds-simplest-lock-free-hash-table/
Restrictions:
- table size must be a power of 2
- table is preallocated
- items cannot be deleted (the live flag can be set to false)

Each entry in the store tracks a single key-value pair
- the key of the entry is the hash of its content
- the entry refers to a value which must be stored elsewhere in memory
- an entry is open for storing a new value iff the key == 0 (as checked by entry_open()) 
*/


#define USING_POSTGRES true

struct Store {
    // pointer to first entry in array of entries
    struct Entry *entries;
    const struct Key key;
    // number of entries in store
    const uint64_t size;
    // number of entries used
    _Atomic uint64_t used;
} *my_store;

struct Store *store_init(struct Key *, uint64_t store_size);
// initialize entry containing value
struct Entry entry_value_init(struct Key *type_key, uint8_t *value, uint32_t value_size);
// initialize entry containing id
struct Entry entry_identity_init(
    struct Key *value,
    struct Hash *previous,
    struct PublicKey *public_key,
    struct SecretKey *secret_key);

void store_add_entry(struct Store *, struct Entry *);
void entry_set_add_entry(struct EntrySet *, struct Entry *);
struct EntrySet store_get_entry(
    struct Store *,
    struct Key *);
struct Entry *store_get_entry_matching_hash(
    struct Store *,
    struct Hash *,
    int min_matching_bits);
void store_free(struct Store *);
void free_entry_set(struct EntrySet *);

void print_entry(struct Entry *);
void print_entry_set(struct EntrySet *);
void print_store(struct Store *);

#endif  // STORE_H_
