// Copyright 2017 Chris Walker

#ifndef TYPES_H_
#define TYPES_H_

#include <stdbool.h>
#include <assert.h>
#include <sodium.h>
#include <sys/socket.h>

#include "config.h"

/*
Cryptographic primitives based on libsodium
*/

// Keys are KEY_BYTES byte pseudo-random values
// Any Key comparisons treat Keys as big-endian integers
struct Key {
    // must generate by cryptographically secure hash function
    uint8_t v[KEY_BYTES];
};

struct Hash {
    // must generate by cryptographically secure hash function
    uint8_t v[HASH_BYTES];
};

struct StorageProofHash {
    // must generate by cryptographically secure hash function
    uint8_t v[STORAGE_PROOF_HASH_BYTES];
};

// Public, Secret Key Pair - used to sign transactions on the blockchain
struct PublicKey {
    uint8_t v[crypto_sign_PUBLICKEYBYTES];
} my_public_key;

struct SecretKey {
    uint8_t v[crypto_sign_SECRETKEYBYTES];
} my_secret_key;

struct Signature {
    uint8_t v[crypto_sign_BYTES];
};

/*
Data stored in SST is handled as either immutable values or mutable identities
*/
enum ENTRY_TYPE {
    UNKNOWN_ENTRY_TYPE = 0,
    ENTRY_VALUE,
    ENTRY_ID,
    ENTRY_TYPE_COUNT,
};

// an Id is a type of value used to point to mutable values:
// unlike any other values, the key of an id remains constant when new id entries are added
struct Id {
    // current value pointed to by this Id
    struct Key value;
    // previous Id
    struct Hash previous;
    // public key and signature are used to authenticate the id and set the initial key
    struct PublicKey public_key;
    struct Signature signature;
};

struct Value {
    // the type of data stored in this value (key to another entry)
    struct Key type_key;
    // the actual data
    uint32_t size;
    void *v;
};

struct Entry {
    _Atomic struct Key key;
    _Atomic bool live;
    enum ENTRY_TYPE type;
    union {
        struct Id id;
        struct Value value;
    };
};

struct EntrySet {
    struct Entry **entries;
    uint32_t n_entries;
};

/*
Transactions are handled in SST using a blockchain
*/
/*
Block:
- block identity is determined by hash: two blocks are identical
  iff their hashes are equal
Blocks may be initialized / modified in an invalid state.
A block is considered valid iff it is a member of the blocks linked list
*/
struct Block {
    struct Hash hash;
    struct Block *previous;
    struct Hash previous_hash;
    uint32_t height;
    struct Tx **txs;
    uint16_t n_txs;
    // number of valid blocks built on top of this block
    uint32_t n_confirmations;
    // when did the miner begin mining this block?
    // TODO(cw): clarify consistant usage of time
    time_t time;
    struct Hash merkle_root;
    struct StorageProofHash storage_proof;
    uint32_t nonce;
    // start with a pseudo-bitcoin
    // currency input_value;
    // currency output_value;
    // currency reward;
    // currency fee;
};

// Array of all known blocks:
// blocks must only be added to all blocks when they are complete
// once added, blocks or their components may not be mutated.
struct Block **all_blocks;
uint32_t n_all_blocks;

// representation of blockchain currency
typedef int64_t currency;

// TODO(cw): use opcodes
// enum OpCode {
//     OP_FALSE = 0x00,  // evaluate to false
//     OP_TRUE = 0x01,   // evaluate to true
//     OP_PUSH = 0x02,   // push data onto stack: OP_PUSH BYTES DATA
//     OP_HASH = 0x03,   // take the hash of data on the stack
// };

// Script execution must not have any side effects! Simply used to return
// true for successful script execution and false otherwise.

// TODO(cw): add contracts for future storage commitments
enum ScriptType {
    // "pay to public key": corresponding signature must match public key
    PUBLIC_KEY = 0,
    // "pay to public key": this signature must match public key
    SIGNATURE,
    // hash to match
    DATA_HASH_CHALLENGE,
    // pay upon proof of data possession: commuted hashes must match challenge
    DATA_HASH_PROOF,
    // always pay: no output to validate
    FEE,
    // pay upon proof of data possession
    STORAGE_REWARD,
    // no output to validate
    WORK_REWARD,
    // cannot spend this output, ever
    NEVER,
    // Hash time lock contract: Pay to A if hash matches within expiry, otherwise B
    HTL,
    // A short value
    VALUE,
    // create a new asset class
    ASSET_CREATION,
};

struct StorageProof {
    struct StorageProofHash seed;
    struct StorageProofHash hash;
};

struct Htl {
    struct Hash lock;
    uint64_t time;
    struct Hash fallback;
};

struct Script {
    enum ScriptType type;
    struct Hash tx_hash;
    uint32_t tx_output_index;
    union {
        struct PublicKey public_key;
        struct Signature signature;
        struct StorageProof storage_proof;
        struct Htl htl;
        struct Hash value;
    };
};


/*
Every input references zero to one prior tx output.
- If the input references a prior tx output, it must prove permission to
  spend the output, and no prior tx input may have already spent the output.
- If the input does not reference a prior tx output, it must be a valid proof-of-work
  mining reward input a or fee input.
Mining and fees
- An input may only be a mining reward iff it is the first output of the first tx of the block.
- Multiple inputs may be fees, but the total fee value must be equal to the unallocated
  output value
*/
struct Input {
    struct Key asset;
    currency amount;
    // this script attempts to prove that this input is valid
    struct Script script;
};

struct Output {
    // which asset is being transferred? Either zeros for internal or
    // a key in the store or zeros for internal asset
    struct Key asset;
    currency amount;
    // the script defines conditions required to spend this output
    struct Script script;
};

// unspent transaction output (for easier bookkeeping)
struct UTXO {
  struct Hash tx_hash;
  uint16_t tx_output_index;
  struct Output output;
};

/*
Transaction:
- transfer currency from inputs to outputs
- a transaction may only be used once, i.e. bound to a single block, 
  according to a particular blockchain
  - note that if a node knows of multiple competing blockchains, the 
    node may know of multiple blocks containing the same transaction,
    but each blockchain may only contain the transaction once.
- transactions identity is determined by hash: thus two transactions
  are the same transaction iff their hashes are equal
- transactions contain an ordered set of inputs representing incoming
  currency from one of:
  - previous transaction outputs
  - mining fee
  - mining reward
- transactions contain an ordered set of outputs representing outgoing
  currency to one of:
  - a public key hash, such that proof of knowledge of the corresponding
    private key is required to use the output in a subsequent input
- optional transaction fee paid to block mining tx
  - fee = Σ(inputs) - Σ(outputs) ≥ 0
  - note that there is no corresponding output for the fee, as
    the transaction creator cannot be certain which node will
    successfully mine the transaction and earn the fee in advance,
    so the creator cannot create an output with the public key of
    the mining node.

Usage:

Building a new tx
 - initialize tx
 - add inputs and outputs
 - validate the tx
 - if mining, save it to newest block
 - send the tx

Receiving a tx
- validate the tx
- save it to an unmined block
- if appropriate, try to mine the block
  - when mined, send out the block
- when a new block arrives, discard this tx ... sometimes
*/
struct Tx {
    // tx inputs and outputs
    struct Input **inputs;
    uint16_t n_inputs;
    // tx outputs
    struct Output **outputs;
    uint16_t n_outputs;
    // time tx is created? Not sure what to do with this.
    // NOTE: not currently serialized!
    uint64_t time;
};

struct Balance {
    struct Key asset;
    currency value;
};

struct BalanceSet {
    struct Balance **balances;
    uint32_t n_balances;
};

struct AssetSet {
    struct Asset **assets;
    uint32_t n_assets;
};


/*
Nodes are computers implementing the SST protocol
*/
enum NodeType {
    /*
    Only SST nodes implement the full SST protocol: client nodes 
    simply send messages and receive responses from SST nodes
    */
    UNKNOWN_NODE_TYPE = 0,
    SST_NODE,
    CLIENT_NODE,
    NODE_TYPE_COUNT,
};


struct Node {
    enum NodeType type;
    struct Key key;
    // the IPv4 or IPv6 address of this node for TCP messages
    struct sockaddr_storage stream_address;
    // TODO(cw): reuse stream connections
    int stream_socket;
    // the IPv4 or IPv6 address of this node for UDP messages
    struct sockaddr_storage datagram_address;
    // time of the most recent outgoing message
    time_t message_time;
    // time of the most recent incoming reply
    time_t reply_time;
    // how many times have we tried to message this node without an answer
    int messages;
    // link to next node in this bucket
    struct Node *next;
    // is this node in a bucket in the routing table?
    struct Bucket *bucket;
    uint16_t sst_version;
};


struct NodeSet {
    struct Node **nodes;
    uint32_t n_nodes;
};

/* 
Messages are sent between nodes
*/
enum MessageType {
    UNKNOWN = 0,
    PING,
    ACK,
    // messages for Kademlia routing
    KEY_SEARCH,
    NODE_SEARCH_RESPONSE,
    VALUE_SEARCH_RESPONSE,
    ENTRY_STORE,
    // messages for blockchain
    TRANSACTION,
    BLOCK_REQUEST,
    BLOCK,
    BLOCKCHAIN_REQUEST,
    MESSAGE_TYPE_COUNT,
    // BLOCKCHAIN,
};

enum Direction {
    INBOUND = 0,
    OUTBOUND,
};

enum MessageProtocol {
    DATAGRAM = 0,
    STREAM = 1,
};

struct Message {
    // unique identifier for this message
    struct Key key;
    enum MessageType type;
    // either the message is from a node or a client
    enum NodeType node_type;
    enum MessageProtocol protocol;
    // either the message is coming in or going out
    enum Direction direction;
    bool valid;
    // TODO(cw): only set when direction is incoming?
    struct sockaddr_storage remote_address;
    struct Key remote_key;
    uint32_t data_size;
    uint16_t sst_version;
    // serialized message
    uint8_t *data;
};


#endif  // TYPES_H_

