// Copyright 2017 Chris Walker

#ifndef SERIALIZE_H_
#define SERIALIZE_H_

#include <stdbool.h>

#include "types.h"
#include "route.h"
#include "blockchain.h"
#include "message.h"
#include "mpack.h"

// tools for reading and writing msgpack
void write_node(mpack_writer_t *writer, struct Node *node);
void write_entry(mpack_writer_t *writer, struct Entry *entry, bool include_signature, bool include_live);
void write_entry_set(mpack_writer_t *writer, struct EntrySet *entry_set);
void write_tx(mpack_writer_t *writer, struct Tx *tx, bool include_signatures);
void write_block(mpack_writer_t *writer, struct Block *block, bool include_hash);

struct Key extract_key(mpack_tree_t *tree, mpack_node_t node);
struct EntrySet extract_entry_set(mpack_tree_t *tree, mpack_node_t entries);
#endif  // SERIALIZE_H_
