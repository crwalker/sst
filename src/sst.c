// Copyright 2017 Chris Walker

#define _POSIX_C_SOURCE 201112L // rand_r is a POSIX function, not in ANSI C
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <stdatomic.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <getopt.h>
#include <netdb.h>

// sst lib
#include <ev.h>

#include "config.h"
#include "types.h"
#include "utils.h"
#include "sst.h"
#include "store.h"
#include "network.h"
#include "route.h"
#include "crypto.h"
#include "serialize.h"
#include "blockchain.h"
#include "message.h"

#include "script.h"

// used by mu_run_test
int tests_run = 0;

// set by route_init
extern struct Key *my_node_key;

// new random value timer
extern uint32_t random_seed;
// static struct ev_timer *new_entry_watcher;
// static void new_entry_init(struct ev_loop *loop);

int main(int argc, char *argv[]) {
    // just testing a scheme repl
    char *scheme_commands[] = {
        "(eval 'sandbox-eval)",
        "(define bar +)",
    };

    run_scheme(sizeof scheme_commands, scheme_commands);

    exit(0);


    // parsing command-line input
    bool create_genesis_block = false;

    printf("size of time: %zu", sizeof(time_t));

    // set up networking with the bootstrap node
    // struct sockaddr_storage remote_address;

    char *datagram_port = DATAGRAM_PORT;
    char *stream_port = STREAM_PORT;
    char *node_key_string = NULL;
    char *remote_node_key_string = NULL;
    char *remote_ip = NULL;
    char *remote_datagram_port = DATAGRAM_PORT;
    char *remote_stream_port = STREAM_PORT;
    struct Node *bootstrap_node;

    int opt;

    const char *short_opt = "gd:s:i:j:k:e:t:";
    struct option long_opt[] = {
        {"genesis", no_argument, NULL, 'g'},
        {"datagram_port", required_argument, NULL, 'd'},
        {"stream_port", required_argument, NULL, 's'},
        {"node_key", required_argument, NULL, 'i'},
        {"remote_node_key", required_argument, NULL, 'j'},
        {"remote_ip", required_argument, NULL, 'k'},
        {"remote_datagram_port", required_argument, NULL, 'e'},
        {"remote_stream_port", required_argument, NULL, 't'},
    };

    while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
        switch (opt) {
            case 'g':
            create_genesis_block = true;
            break;

            case 'i':
            node_key_string = optarg;
            if (strlen(node_key_string) != 2 * KEY_BYTES) {
                printf("--key must be %i byte (%i char) hex string\n", KEY_BYTES, 2*KEY_BYTES);
                exit(EXIT_FAILURE);
            }
            break;

            case 'j':
            remote_node_key_string = optarg;
            if (strlen(remote_node_key_string) != 2 * KEY_BYTES) {
                printf("--remote_key must be %i byte (%i char) hex string\n", KEY_BYTES, 2*KEY_BYTES);
                exit(EXIT_FAILURE);
            }
            break;

            case 'k':
            remote_ip = optarg;
            break;

            case 'd':
            datagram_port = optarg;
            break;

            case 'e':
            remote_datagram_port = optarg;
            break;

            case 's':
            stream_port = optarg;
            break;

            case 't':
            remote_stream_port = optarg;
            break;

            default:
            break;
        }
    }

    if (remote_node_key_string == NULL || remote_ip == NULL) {
        fprintf(
            stderr,
            "Usage: %s\n"
            "--datagram_port (optional),\n"
            " --stream_port (optional),\n"
            " --key (optional),\n"
            " --remote_key,\n"
            " --remote_ip,\n"
            " --remote_datagram_port (optional)\n,"
            " --remote_stream_port (optional)\n",
            argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("\nhello sst\n\n");
    print_codex_version();

    // set up main event loop
    struct ev_loop *loop = ev_default_loop(0);
    if (!loop) {
        puts("could not create event loop. Exiting.");
        exit(EXIT_FAILURE);
    }

    // set up crypto suite
    crypto_init();

    // set up Kademlia routing tree
    // either accepts key string or creates a random key
    route_init(node_key_string);
    // set by route_init()
    assert(my_node_key);

    // set up networking:
    // * stream and datagram sockets
    // * non-blocking listen on sockets
    // * register callbacks with main event loop to parse messages when sockets readable
    network_init(loop, datagram_port, stream_port);

    // set up key-value store
    my_store = store_init(my_node_key, STORE_SIZE);

    // set up blockchain
    blockchain_init(loop, create_genesis_block);

    // bootstrap by trying to connect to another node.
    bootstrap_node = node_init(
        string_to_key(remote_node_key_string),
        SST_NODE,
        get_remote_address(
            remote_ip,
            remote_datagram_port,
            DATAGRAM),
        get_remote_address(
            remote_ip,
            remote_stream_port,
            STREAM));

    printf("set up bootstrap node:");
    print_node(bootstrap_node);

    // try talking with the bootstrap node
    send_ping(bootstrap_node);

    // fill in our routing table starting with our own node key
    search_start(*my_node_key);

    // get the up to date blockchain
    send_blockchain_request(bootstrap_node);

    // start sending out random dummy values
    // new_entry_init(loop);

    // now wait for events to arrive
    puts("running main event loop");
    ev_run(loop, 0);

    // clean up after sst is done
    network_end();
    route_end();
    blockchain_end();
    store_free(my_store);

    printf("\ngoodbye sst\n\n");
}

void on_new_entry() {
    // try sending a value
    struct UTXO **utxo;
    struct UTXO *selected_utxo = NULL;
    uint32_t n_utxo = 0;

    struct Block *block = block_find_highest_valid();

    if (!block) {
        puts("cannot send values before a block to pay for them exists");
        return;
    }

    // find out how many unspent tx outputs are available
    unspent_outputs(block, &utxo, &n_utxo);

    assert(n_utxo == 0 || *utxo);

    // print them
    printf("found %" PRIu32 "utxo\n", n_utxo);
    for (size_t i = 0; i < n_utxo; i++) {
        assert(utxo[i]);
        print_output(&utxo[i]->output);

        // find one that we can use to pay for the storage of this value
        if (utxo[i]->output.script.type == PUBLIC_KEY
            && public_keys_equal(&utxo[i]->output.script.public_key, &my_public_key)) {
            puts("found a utxo we can spend");
            selected_utxo = utxo[i];
            break;
        }
    }

    if (!selected_utxo) {
        puts("there are no outputs that we can spend");
        return;
    }

    currency payment = selected_utxo->output.amount > 1000 ? 1000 : selected_utxo->output.amount;
    currency change = selected_utxo->output.amount - payment;

    // we should compensate the node for storing this value
    struct Tx *tx = tx_init();
    struct Input *input;
    struct Output *payment_output, *change_output;

    input = input_init((struct Input) {
        .amount = selected_utxo->output.amount,
        .script = {
            .type = SIGNATURE,
            .tx_hash = selected_utxo->tx_hash,
            .tx_output_index = selected_utxo->tx_output_index,
        }
    });

    payment_output = output_init((struct Output) {
        .amount = payment,
        .script = {
            .type = PUBLIC_KEY,
            // TODO(cw): pay to public key of node
            .public_key = my_public_key,
        }
    });

    change_output = output_init((struct Output) {
        .amount = change,
        .script = {
            .type = PUBLIC_KEY,
            .public_key = my_public_key,
        }
    });

    tx_add_inputs(tx, input, 1);
    tx_add_outputs(tx, payment_output, 1);
    tx_add_outputs(tx, change_output, 1);


    tx_sign(tx);

    print_tx(tx);

    assert(tx_valid(tx));

    struct Entry entry;
    struct Key value, type_key;
    struct Hash previous = {
        .v = {0},
    };
    enum ENTRY_TYPE type;
    int size, n_entries = rand_r(&random_seed) % 5 + 1;

    struct EntrySet entry_set = {
        .entries = NULL,
        .n_entries = 0,
    };

    for (int i = 0; i < n_entries; i++) {
        type = rand_r(&random_seed) % 2 + 1;

        value = random_key();

        if (type == ENTRY_ID) {
            entry = entry_identity_init(
                &value,
                &previous,
                &my_public_key,
                &my_secret_key);

        } else if (type == ENTRY_VALUE) {
            type_key = random_key();
            size = rand_r(&random_seed) % 20 + 1;
            uint8_t value[size];
            for (int i = 0; i < size; i++) {
                value[i] = (uint8_t) rand_r(&random_seed) % 256;
            }
            entry = entry_value_init(&type_key, value, sizeof value);
        }

        print_entry(&entry);
        entry_set_add_entry(&entry_set, &entry);
    }

    // find the nodes closest to this key
    struct NodeSet node_set = closest_nodes(my_node_key, κ);
    for (size_t i = 0; i < node_set.n_nodes; i++) {
        // try to store the value on each of the closest nodes
        send_entry_store(node_set.nodes[i], &entry_set, tx);
    }

    // check if the values are stored
    for (size_t i = 0; i < entry_set.n_entries; i++) {
        search_start(entry_set.entries[i]->key);
    }
}

// void new_entry_init(struct ev_loop *loop) {
//     new_entry_watcher = malloc_or_exit(sizeof (ev_timer));
//     ev_init(new_entry_watcher, on_new_entry);
//     new_entry_watcher->repeat = 60;
//     ev_timer_again(loop, new_entry_watcher);
// }

static char *test_network() {
    puts("testing network");
    mu_assert(1 == 1, "1 == 1");
    // return 0 only reached if assertion passes
    return 0;
}


static char *run_tests() {
    mu_run_test(test_network);
    // return 0 only reached if all assertions pass
    return 0;
}


void test() {
    puts("Running tests");
    char *result = run_tests();
    if (result != 0) {
        printf("TEST FAILED: %s\n", result);
        exit(EXIT_FAILURE);
    } else {
        puts("All tests passed");
    }
}


void print_codex_version() {
    printf("version 0\n");
}
