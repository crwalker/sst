// Copyright 2017 Chris Walker

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <arpa/inet.h>

#include "types.h"
#include "utils.h"
#include "crypto.h"
#include "route.h"
#include "network.h"
#include "blockchain.h"
#include "store.h"

// create and send outgoing message
void send_ping(struct Node *);
// TODO(cw): add key to ack to track correspondence with incoming messages
void send_ack(struct Node *, struct Key *message_key, struct Key *entry_key);
void send_key_search(struct Node *, struct Key *search_key, struct Key *target_key);
void send_node_search_response(struct Node *, struct Key *search_key, struct Node *);
void send_value_search_response(struct Node *, struct Key *search_key, struct Key *target_key, struct EntrySet *);
void send_entry_store(struct Node *, struct EntrySet *, struct Tx *);
void send_block_request(struct Node *, struct Hash *);
void send_block(struct Node *, struct Block *);
void send_blockchain_request(struct Node *);
void send_blockchain(struct Node *, struct Block *);

// incoming message handler
void on_message(
    struct sockaddr_storage remote_address,
    enum MessageProtocol,
    uint8_t *raw_message,
    uint32_t message_size);

#endif  // MESSAGE_H_
