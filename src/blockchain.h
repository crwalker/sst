// Copyright 2017 Chris Walker

#ifndef BLOCKCHAIN_H_
#define BLOCKCHAIN_H_

#include <stdint.h>
#include <stdbool.h>
#include <ev.h>

#include "types.h"
#include "crypto.h"
#include "route.h"
#include "script.h"
#include "utils.h"

// maximum in-memory size in bytes of a single block
// #define MAX_BLOCK_SIZE 1000000;

// TODO(cw): check against this max
// Index into inputs is stored as a uint32_t
#define MAX_TX_INPUT UINT16_MAX
#define MAX_TX_OUTPUT UINT16_MAX
// max number of txs per block
#define MAX_BLOCK_TX UINT16_MAX

struct Input *input_init(struct Input);
struct Output *output_init(struct Output);

struct Tx *tx_init();
struct Hash tx_hash(struct Tx *);
void unbound_tx_add(struct Tx *);
void tx_add_inputs(struct Tx *, struct Input *, uint16_t);
void tx_add_outputs(struct Tx *, struct Output *, uint16_t);
void tx_sign(struct Tx *);
bool tx_valid(struct Tx *);

void block_add_txs(struct Block *, struct Tx *, uint16_t);

void unspent_outputs(struct Block *, struct UTXO ***, uint32_t *);
struct Output *output_find(
    struct Block *this_block,
    uint16_t this_tx_index,
    struct Hash *tx_h,
    uint16_t output_index);

struct Block *block_find(struct Hash *);
struct Block *block_find_highest();
struct Block *block_find_highest_valid();
uint32_t block_height(struct Block *);

void blockchain_init(struct ev_loop *loop, bool create_genesis_block);
void blockchain_end();
bool blockchain_valid(struct Block *block);

// event callbacks when other nodes provide or request information about the blockchain
void on_receive_tx(struct Node *, struct Tx *);
void on_receive_block(struct Node *, struct Block *);

void print_input(struct Input *);
void print_output(struct Output *);
void print_tx(struct Tx *);
void print_block(struct Block *, bool include_txs);
void print_blockchain(struct Block *, bool include_txs);
void print_blockchain_graph(int depth);

#endif  // BLOCKCHAIN_H_
