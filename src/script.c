// Copyright 2017 Chris Walker

// Use libracket3m garbage collector
#define MZ_PRECISE_GC

// racket's scheme header
#include <scheme.h>
#include <assert.h>
#include <time.h>

#include "utils.h"
#include "script.h"
#include "crypto.h"
#include "blockchain.h"

// racket sandbox as c header containing racket module
// generated via `raco ctool --c-mods racket_sandbox.h racket-sandbox.rkt`
#include "racket_sandbox.h"


static int run(Scheme_Env *e, int argc, char *argv[]) {
    // using libracket3m with 3M GC for installation simplicity and performance
    // see https://docs.racket-lang.org/inside/embedding.html

    Scheme_Object *curout = NULL, *v = NULL, *a[2] = {NULL, NULL};

    Scheme_Object *args[1] = {NULL};

    Scheme_Object *sandbox_eval_str = NULL;

    Scheme_Config *config = NULL;
    int i;
    Scheme_Thread *th = NULL;
    mz_jmp_buf * volatile save = NULL, fresh;

    MZ_GC_DECL_REG(13);
    MZ_GC_VAR_IN_REG(0, e);  // simple pointers require a single slot
    MZ_GC_VAR_IN_REG(1, curout);
    MZ_GC_VAR_IN_REG(2, save);
    MZ_GC_VAR_IN_REG(3, config);
    MZ_GC_VAR_IN_REG(4, v);
    MZ_GC_VAR_IN_REG(5, sandbox_eval_str);
    MZ_GC_VAR_IN_REG(6, th);
    MZ_GC_ARRAY_VAR_IN_REG(7, a, 2);  // arrays require 3 slots for registration
    MZ_GC_ARRAY_VAR_IN_REG(10, args, 1);

    MZ_GC_REG();

    // initial setup
    declare_modules(e);
    config = scheme_current_config();
    curout = scheme_get_param(config, MZCONFIG_OUTPUT_PORT);
    th = scheme_get_current_thread();

    v = scheme_intern_symbol("racket");
    scheme_namespace_require(v);

    // load the racket-sandbox module defined in racket-sandbox.rkt
    v = scheme_make_pair(scheme_intern_symbol("quote"),
                       scheme_make_pair(scheme_intern_symbol("racket-sandbox"),
                                        scheme_make_null()));
    scheme_namespace_require(v);

    // get the sandbox-eval-str procedure
    sandbox_eval_str = scheme_eval_string("sandbox-eval-str", e);

    args[0] = scheme_make_utf8_string("(+ 1 2 3 4)");

    v = scheme_apply(sandbox_eval_str, 1, args);
    scheme_display(v, curout);
    v = scheme_make_char('\n');
    scheme_display(v, curout);


    for (i = 0; i < argc; i++) {
      save = th->error_buf;
      th->error_buf = &fresh;
      if (scheme_setjmp(*th->error_buf)) {
        th->error_buf = save;
        printf("there was a scheme error\n");
        return -1;
      } else {
        printf("about to evaluate this string: %s\n", argv[i]);
        v = scheme_eval_string(argv[i], e);
        scheme_display(v, curout);
        v = scheme_make_char('\n');
        scheme_display(v, curout);
        // read-eval-print loop, uses initial Scheme_Env

        a[0] = scheme_intern_symbol("racket");
        a[1] = scheme_intern_symbol("read-eval-print-loop");
        v = scheme_dynamic_require(2, a);

        scheme_apply(v, 0, NULL);

        th->error_buf = save;
        printf("racket repl exited\n");
      }
    }

    MZ_GC_UNREG();

    return 0;
}


int run_scheme(int argc, char *argv[]) {
    return scheme_main_setup(1, run, argc, argv);
}


static const char *SCRIPT_TYPE_NAMES[] = {
    "Public Key",
    "Signature",
    "Data Hash Challenge",
    "Data Hash Proof",
    "Fee",
    "Storage Reward",
    "Work Reward",
    "Never",
    "Hash time lock",
    "Value",
    "Asset Creation",
};

bool script_position_valid(struct Script *script, bool is_input);


bool input_script_requires_prior_output(struct Script *script) {
    assert(script);
    assert(script_position_valid(script, true));

    switch (script->type) {
        case SIGNATURE:
        case STORAGE_REWARD:
        case DATA_HASH_PROOF:
        // puts("this script type requires a prior output");
        return true;

        case ASSET_CREATION:
        case FEE:
        case WORK_REWARD:
        // puts("this script type does not require prior output");
        return false;

        default:
        printf("input_script_requires_prior_output: unknown script type %i", script->type);
        exit(EXIT_FAILURE);
    }
}


bool script_position_valid(struct Script *script, bool is_input) {
    switch (script->type) {
        // output only
        case PUBLIC_KEY:
        case DATA_HASH_CHALLENGE:
        case NEVER:
        case HTL:
        if (is_input) {
            puts("script invalid: public key, data hash challenge, never, htl, may only be output");
            return false;
        }
        return true;

        // input-only
        case FEE:
        case WORK_REWARD:
        case SIGNATURE:
        case STORAGE_REWARD:
        case DATA_HASH_PROOF:
        case ASSET_CREATION:
        if (!is_input) {
            puts("script invalid: sig, fee, proof, rewards may only be input");
            return false;
        }
        return true;

        default:
        printf("script_valid: unknown script type %i\n", script->type);
        print_script(script);
        exit(EXIT_FAILURE);
    }
}


bool script_valid(struct Script *script, bool is_input) {
    // called on either an input or output script
    assert(script);

    if (!script_position_valid(script, is_input)) {
        puts("script invalid: incorrect position");
        return false;
    }

    // check that input scripts correctly reference prior outputs
    if (is_input && input_script_requires_prior_output(script) && zeros(script->tx_hash.v, sizeof script->tx_hash.v)) {
        puts("script invalid: missing prior output script reference");
        return false;
    } else if (is_input && !input_script_requires_prior_output(script)
        && (!zeros(script->tx_hash.v, sizeof script->tx_hash.v) || script->tx_output_index != 0)) {
        puts("script invalid: has prior output script reference when not required");
        return false;
    }

    switch (script->type) {
        case SIGNATURE:
        // input only
        // puts("sig script type requires a previous output tx hash and a signature");
        if (zeros(script->signature.v, sizeof script->signature.v)) {
            puts("script invalid: signature cannot be zeros");
            return false;
        }
        return true;

        case DATA_HASH_CHALLENGE:
        case DATA_HASH_PROOF:
        case STORAGE_REWARD:
        // puts("script type requires a previous output and a storage proof");
        if (zeros(script->storage_proof.seed.v, sizeof script->storage_proof.seed.v)
            || zeros(script->storage_proof.hash.v, sizeof script->storage_proof.hash.v)) {
            puts("script invalid: output tx hash, storage proof seed, storage proof hash cannot be zeros");
            return false;
        }
        return true;

        case NEVER:
        return true;

        case PUBLIC_KEY:
        case FEE:
        case WORK_REWARD:
        // puts("this script type must not reference a previous output");
        if (!zeros(script->tx_hash.v, sizeof script->tx_hash.v)
            || script->tx_output_index != 0) {
            puts("script invalid: output tx hash and output index must be zeros");
            return false;
        }
        return true;

        case HTL:
        if (
            zeros(script->htl.lock.v, sizeof script->htl.lock.v)
            || script->htl.time == 0
            || zeros(script->htl.fallback.v, sizeof script->htl.fallback.v)
        ) {
            puts("script invalid: lock, time, fallback cannot be zeros");
            return false;
        }
        return true;

        case ASSET_CREATION:
        // validated by checking tx and blockchain
        return true;

        default:
        puts("script invalid: unknown output script type");
        exit(EXIT_FAILURE);
    }

    puts("second switch should return");
    exit(EXIT_FAILURE);
}


bool input_script_unlocks_output(
    void *this_block,
    uint16_t this_tx_index,
    struct Key *input_asset,
    currency input_amount,
    struct Script *input_script,
    uint8_t *signed_value,
    uint32_t signed_size) {
    /*
    NOTE: value flows from a prior output to the input and then to new outputs!
    The prior output tx must be prior to this tx but may be in the same block!
    This function is only called on input scripts to determine whether they have the ability
    to access the value they claim from a previous output.
    The blockchain as a whole must be analyzed to determine if the transaction is valid
    */
    struct Output *prior_output;

    assert(input_script);
    assert(script_valid(input_script, true));

    if (!input_script_requires_prior_output(input_script)) {
        return true;
    }

    puts("finding required output");
    prior_output = output_find(
        (struct Block *) this_block,
        this_tx_index,
        &input_script->tx_hash,
        input_script->tx_output_index);
    assert(prior_output);
    assert(script_valid(&prior_output->script, false));

    if (!keys_equal(input_asset, &prior_output->asset)) {
        puts("prior output and input are different assets");
        return false;
    }

    if (input_amount != prior_output->amount) {
        puts("prior_output and input amounts differ");
        return false;
    }

    switch (prior_output->script.type) {
        case PUBLIC_KEY: {
            assert(signed_value);
            assert(signed_size > 0);

            if (input_script->type != SIGNATURE) {
                puts("input script failed: if prior_output script is public key, input script must be signature");
                return false;
            }

            if (!signature_valid(
                &input_script->signature,
                signed_value,
                signed_size,
                &prior_output->script.public_key)) {
                puts("input script failed: input signature does not match prior_output public key");
                return false;
            }
            return true;
        }

        case DATA_HASH_CHALLENGE: {
            // test if nodes that generated input script and prior_output script both had
            // access to value v by commutative hash fn H(value, seed):
            // If H(H(v, a), b) == H(H(v, b), a) then both input and prior_output nodes had access

            if (input_script->type != DATA_HASH_PROOF) {
                puts("input script failed: if prior_output script is data hash challenge, input script must be data hash proof");
                return false;
            }

            struct StorageProofHash h1 = storage_proof_hash(
                input_script->storage_proof.hash.v,
                sizeof input_script->storage_proof.hash.v,
                &prior_output->script.storage_proof.seed);

            struct StorageProofHash h2 = storage_proof_hash(
                prior_output->script.storage_proof.hash.v,
                sizeof prior_output->script.storage_proof.hash.v,
                &input_script->storage_proof.seed);

            if (!storage_proof_hashes_equal(h1, h2)) {
                puts("input script failed: storage proof hashes are not equal");
                return false;
            }
            return true;
        }

        case NEVER:
        // puts("prior_output may never be spent");
        return false;

        case HTL: {
            struct Hash h1, h2;
            time_t now = time(NULL);
            time_t lock_time = (time_t) prior_output->script.htl.time;
            if (input_script->type != VALUE) {
                puts("input script failed: HTL output requires value input");
                return false;
            }
            h1 = hash(input_script->value.v, sizeof input_script->value.v);
            h2 = now < lock_time ? prior_output->script.htl.lock : prior_output->script.htl.fallback;

            if (!hashes_equal(h1, h2)) {
                puts("input script failed: value does not match hash");
                return false;
            }
            return true;
        }

        default:
        puts("input script failed: unknown prior_output script type");
        print_script(&prior_output->script);
        exit(EXIT_FAILURE);
    }

    puts("all prior_output script cases must return");
    exit(EXIT_FAILURE);
}


void print_script(struct Script *script) {
    if (!script) {
        printf("Script\n(null)\n");
        return;
    }

    printf("Script\n- type: %s\n", SCRIPT_TYPE_NAMES[script->type]);

    // if (input_script_requires_prior_output(script)) {
    printf("- output tx hash: ");
    print_hash(&script->tx_hash);
    printf("\n- output index: %lu\n", (unsigned long) script->tx_output_index);
    // }

    switch (script->type) {
        case FEE:
        case WORK_REWARD:
        case NEVER:
        case ASSET_CREATION:
        break;

        case PUBLIC_KEY:
        printf("- public key: ");
        print_public_key(&script->public_key);
        printf("\n");
        break;

        case SIGNATURE:
        printf("- signature: ");
        print_signature(&script->signature);
        printf("\n");
        break;

        case STORAGE_REWARD:
        case DATA_HASH_CHALLENGE:
        case DATA_HASH_PROOF:
        printf("- storage proof seed");
        print_storage_proof_hash(&script->storage_proof.seed);
        printf("\n- storage proof hash");
        print_storage_proof_hash(&script->storage_proof.hash);
        printf("\n");
        break;

        case VALUE:
        printf("- value: ");
        print_hash(&script->value);
        printf("\n");
        break;

        case HTL:
        printf("- lock hash: ");
        print_hash(&script->htl.lock);
        printf("\n- lock time: ");
        print_time((time_t) script->htl.time);
        printf("\n- fallback hash: ");
        print_hash(&script->htl.fallback);
        printf("\n");
        break;

        default:
        puts("print_script: unknown script type");
        exit(EXIT_FAILURE);
    }
}

